import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CabinetMedicalService} from '../cabinet-medical.service';
import {Router} from '@angular/router';
import {sexeEnum} from '../dataInterfaces/sexe';
import {Adresse} from '../dataInterfaces/adresse';
import {PatientInterface} from '../dataInterfaces/patient';

@Component({
  selector: 'app-add-patient',
  templateUrl: './add-patient.component.html',
  styleUrls: ['./add-patient.component.scss']
})
export class AddPatientComponent implements OnInit {
  patForm: FormGroup;
  errorMessage : string ;
  constructor(private formBuilder: FormBuilder , private patServ: CabinetMedicalService, private rout : Router) { }

  ngOnInit() {
    this.initForm();
  }
  initForm() {
    this.patForm = this.formBuilder.group({
      prénom: ['',[Validators.required,Validators.pattern(/[a-zA-Zéèêëöô'à\s]/)]],
      nom:['',[Validators.required,Validators.pattern(/[a-zA-Zéèêëöô'à\s]/)]],
      sexe:'',
      numSecu : ['',[Validators.required,Validators.pattern(/(\d){15}/)]],
      anniversaire: ['',Validators.required],
      étage: '',
      numéro: '',
      rue: ['',[Validators.required,Validators.pattern(/[a-zA-Zé\s]/)]],
      ville: ['',[Validators.required,Validators.pattern(/[a-zA-Zé\s]/)]],
      codePostal: ['',[Validators.required,Validators.pattern(/(\d){5}/)]],
      dateVisite: ''
    });
  }
/*-------------------------addPatient : ajouter un patient au serveur ---------------------------------------------------*/

  /* dans cette fonction en cree un patient en recuperant les infos du formulaire et puis on
   appelle la fonction ajouter du service , celle-ci nous permet de faire un http.post
   si les champs obligatoires ne sont pas remplis , le boutton ajouter ne marchera pas*/
  addPatient (){

    const formValue = this.patForm.value;// recuperer les infos du formulaires(la valeur de chaque  champs remplis)
    let sexe = formValue['sexe'];// recuperer le sexe
    let sx;
    if(sexe =="M"){
      sx = sexeEnum.M;
    }else{
      sx = sexeEnum.F;
    }

    const adresse : Adresse ={ // creer une adresse a partir des infos recupéreés du formulaire
      ville: formValue['ville'],
      codePostal: formValue['codePostal'],
      rue: formValue['rue'],
      numéro: formValue['numéro'],
      étage: formValue['étage']
    };

    const patient : PatientInterface ={// creer un patient selon les champs remplis en formulaire
      prénom: formValue['prénom'],
      nom: formValue['nom'],
      sexe: sx,
      numéroSécuritéSociale: formValue['numSecu'],
      adresse: adresse,
      visiteDate:formValue['dateVisite']
    };
    const anniv = formValue['anniversaire'];
    if(this.patServ.dontExiste(patient)){// tester si le patient n'existe pas deja , si oui on l'ajoute en applant la methode ajouter du service
      this.patServ.ajouter(patient, anniv).then(
        ()=>{
          this.rout.navigate(['/patients']);
        },
        (error)=>{
          this.errorMessage=error;
        }
      );
    }
    else{// sinon on afficher ce message d'erreur
      this.errorMessage= this.patServ.patientExiste(patient);
    }


  }
}
