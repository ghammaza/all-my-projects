import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import { NgModule } from '@angular/core';
import {MatCardModule} from '@angular/material/card';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatMenuModule} from '@angular/material/menu';
import {ScrollDispatchModule} from '@angular/cdk/scrolling';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatSelectModule} from '@angular/material/select';

@NgModule({
  imports: [MatSelectModule,MatIconModule,MatInputModule,MatPaginatorModule,MatButtonModule, MatCheckboxModule,MatCardModule,MatGridListModule,MatMenuModule,ScrollDispatchModule,DragDropModule,ScrollingModule,CdkTableModule,CdkTreeModule],
  exports: [MatSelectModule,MatIconModule,MatInputModule,MatPaginatorModule,MatButtonModule, MatCheckboxModule,MatCardModule,MatGridListModule,MatMenuModule,ScrollDispatchModule,DragDropModule,ScrollingModule,CdkTableModule,CdkTreeModule],
})
export class MyOwnCustomMaterialModule { }
