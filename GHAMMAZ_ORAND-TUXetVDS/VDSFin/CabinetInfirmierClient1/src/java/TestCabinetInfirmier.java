
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author LENOVO
 */
public class TestCabinetInfirmier {
public static void launch(String fileName) {
        File htmlFile = new File(fileName);
        xml.BrowserUtil.launch(htmlFile.toURI());
    }
        
    public static void launch(URI fileURI) {
        // By default, load an interesting web page
        String default_url = "https://fr.wikipedia.org/wiki/La_grande_question_sur_la_vie,_l%27univers_et_le_reste";
        if (fileURI.toString().equals(""))
        try {
            fileURI= new URI(default_url);
        } catch (URISyntaxException ex) {
            Logger.getLogger(xml.BrowserUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        // Open a browser in Windows OS
        if (Desktop.isDesktopSupported()) {
            Desktop desktop = Desktop.getDesktop();
            try {
                desktop.browse(fileURI);
            } catch (IOException ex) {
                Logger.getLogger(xml.BrowserUtil.class.getName()).log(Level.SEVERE, null, ex);
            }
        // Or in Linux OSes
        } else {
            Runtime runtime = Runtime.getRuntime();
            try {
                runtime.exec("xdg-open " + fileURI);
            } catch (IOException ex) {
                Logger.getLogger(xml.BrowserUtil.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String s ;
        s = getNomCabinet();
        int i = getStrLength(s);
        s = hello(s);
        s += "t'as longueur est : ";
        System.out.println(s+" "+ i+" caractéres ");
          // Test : output in a browser of an HTML page
        try {
            // get the html content and record it into a file (a cache)
            FileUtils.stringToFile(getInfirmiere(), "myHtmlFile.html");
            // view this file in a browser
           TestCabinetInfirmier.launch("myHtmlFile.html");
        } catch (IOException ex) {
            Logger.getLogger(TestCabinetInfirmier.class.getName()).log(Level.SEVERE, null, ex);
        }    
         
    }

    private static String getNomCabinet() {
        fr.ujf_grenoble.l3miage.medical.CabinetInfirmier_Service service = new fr.ujf_grenoble.l3miage.medical.CabinetInfirmier_Service();
        fr.ujf_grenoble.l3miage.medical.CabinetInfirmier port = service.getCabinetInfirmierPort();
        return port.getNomCabinet();
    }

    private static int getStrLength(java.lang.String p) {
        fr.ujf_grenoble.l3miage.medical.CabinetInfirmier_Service service = new fr.ujf_grenoble.l3miage.medical.CabinetInfirmier_Service();
        fr.ujf_grenoble.l3miage.medical.CabinetInfirmier port = service.getCabinetInfirmierPort();
        return port.getStrLength(p);
    }

    private static String hello(java.lang.String name) {
        fr.ujf_grenoble.l3miage.medical.CabinetInfirmier_Service service = new fr.ujf_grenoble.l3miage.medical.CabinetInfirmier_Service();
        fr.ujf_grenoble.l3miage.medical.CabinetInfirmier port = service.getCabinetInfirmierPort();
        return port.hello(name);
    }

    private static String getHTML() {
        fr.ujf_grenoble.l3miage.medical.CabinetInfirmier_Service service = new fr.ujf_grenoble.l3miage.medical.CabinetInfirmier_Service();
        fr.ujf_grenoble.l3miage.medical.CabinetInfirmier port = service.getCabinetInfirmierPort();
        return port.getHTML();
    }

    private static String getInfirmiere() {
        fr.ujf_grenoble.l3miage.medical.CabinetInfirmier_Service service = new fr.ujf_grenoble.l3miage.medical.CabinetInfirmier_Service();
        fr.ujf_grenoble.l3miage.medical.CabinetInfirmier port = service.getCabinetInfirmierPort();
        return port.getInfirmiere();
    }
    
}
