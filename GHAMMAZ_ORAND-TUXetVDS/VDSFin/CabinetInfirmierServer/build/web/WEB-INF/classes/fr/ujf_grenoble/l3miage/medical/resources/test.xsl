<!--

    Document   : test.xsl
    Created on : 19 octobre 2017, 18:12
    Author     : Nicolas GLADE
    Description: Cette feuille de test montre comment transformer un centre médical.
                 et déclarer un paramètre pour recevoir une valeur fournie par le processeur XSLT
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.ujf-grenoble.fr/l3miage/medical" version="1.0">
<xsl:output method="html"/>
 <!--
 ICI, on déclare un paramètre, c'est à dire une valeur pouvant être reçue du processeur XSLT 
-->
<xsl:param name="idInfirmier" />
<xsl:template match="/">
<html>
<head>
                <title>kokoqjgd.xsl</title>
                <img src="src/java/cache/images/hfg.png"/>
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                 <link rel='stylesheet' type='text/css' href='src/java/cache/css/cabinet.css'/>
                  
               
                <script type="text/javascript" src="src/java/cache/js/facture.js"></script>
                <script type="text/javascript">
                    function openFacture(prenom, nom, actes) {
                    var width  = 500;
                    var height = 300;
                    if(window.innerWidth) {
                    var left = (window.innerWidth-width)/2;
                    var top = (window.innerHeight-height)/2;
                    }
                    else {
                    var left = (document.body.clientWidth-width)/2;
                    var top = (document.body.clientHeight-height)/2;
                    }
                    var factureWindow = window.open('','facture','menubar=yes, scrollbars=yes, top='+top+', left='+left+', width='+width+', height='+height+'');
                    factureText = afficherFacture(prenom, nom, actes);
                    factureWindow.document.write(factureText);
                    }     
                </script>

                  
            </head>
            
<body>
<br/>
 <table  class="table1"   style="border:0;" >
                    <tr style="border:0;">
                        <td rowspan = "2" width="100px" style="border:0;">
                            <img>
                                <xsl:attribute name="src">src/java/cache/images/<xsl:value-of select="//infirmier/photo"/></xsl:attribute>
                                <xsl:attribute name="width"> 150px</xsl:attribute>
                            </img>
                        </td>
                            
                        <td rowspan="2" colspan="4"  style="border:0;"> 
                            <h3> Bonjour  
                                <xsl:value-of select="//infirmier/prénom"/>
                                <xsl:text> </xsl:text>
                                <xsl:value-of select="//infirmier/nom"/>
                            </h3>
                            <h3>
                                Aujourd'hui, vous avez  <xsl:value-of select="count(//visite[@intervenant=001])"/>  patients
                            </h3>
                          </td>
                  
                       
                    </tr>
                    <tr>
                
                    </tr>
 </table>

                <table class="table1"  border="3">
                             
               
                    <tr>
                        <th rowspan="1" colspan="5" style="background-color: red;
                                color: white;
                                border-collapse: collapse;
                                 border: 2px solid black;">Toutes les visites</th>
                    </tr>
                    <tr>
                        <th style="background-color: #dddddd;"> Visite du jour </th>
                        <th style="background-color: #dddddd;"> date </th>
                        <th style="background-color: #dddddd;">nom du patient</th>
                        <th style="background-color: #dddddd;">adresse du patient</th>
                        <th style="background-color: #dddddd;">liste des soins</th>
                    </tr>
                    <xsl:apply-templates select="//visite[@intervenant=001]"/>
                </table>
<!--
 Ici, on utilise ce paramètre. Attention à bien mettre un $ devant le nom du paramètre 
-->
 </body>
        </html>
    </xsl:template>
    
    
    
    
    
    <xsl:template match="visite"> <!-- type d'element -->
   
        <xsl:param name="date" >
            <xsl:value-of select="@date"/>
        </xsl:param>
        <tr>
            <td>
                
                <xsl:if test="$date='2015-12-08'">
                    <center>oui</center> 
                </xsl:if>
                <xsl:if test="$date!='2015-12-08'">
                    <center>nn</center> 
                </xsl:if>
               
                           
            </td>
            <td> 
                <center>
                    <!-- type d'element -->
                    <xsl:value-of select="@date"/> 
                   
                    <br></br>
                </center>
                            
            </td>
            <td > 
                <center>
                    <xsl:template match="nom"> <!-- type d'element -->
                        <xsl:value-of select="../nom"/>
                    </xsl:template>
                    <xsl:text> </xsl:text>
                    <xsl:template match="prénom"  > <!-- type d'element -->
                        <xsl:value-of select="../prénom"/>
                        <br/>
                    </xsl:template>
                </center>
                            
            </td>
            <td text-align="center">
                <center>
                    <xsl:template match="adresse"  > <!-- type d'element -->
                    
                      
                        étage: <xsl:value-of select="../adresse/étage"/>
                        appartement: <xsl:value-of select="../adresse/numéro"/>
                        <br/>
                        rue: <xsl:value-of select="../adresse/rue"/>
                        <br/>
                        ville: <xsl:value-of select="../adresse/ville"/>
                        <br/>
                        codePostal: <xsl:value-of select="../adresse/codePostal"/>
                       
                    </xsl:template>
                </center>
                <br/>
            </td>
            <td text-align="center">
                <center>
                   
              
                    
                    <xsl:apply-templates select="./acte"/>
                    
        
                    <div id="divBouton">
                        <button value="Facture" class="button">
                            <xsl:attribute name="onclick">
                                openFacture('<xsl:template match="prénom"  > <!-- type d'element -->
                                    <xsl:value-of select="../prénom"/>
                                    <br/>
                                </xsl:template>', 
                                '<xsl:template match="nom"  > <!-- type d'element -->
                                    <xsl:value-of select="../nom"/>
                                    <br/>
                                </xsl:template>', 
                              
                                '  <xsl:template match="acte"  > <!-- type d'element -->
                                    <xsl:value-of select="./acte/@id"/>
                                    <br/>
                                </xsl:template>')
                            </xsl:attribute>
                            <span>Facture</span>
                        </button>
                    </div>
                     
                    <br/> 
                </center>
                <br/>
            </td>
        </tr>
             
                      
    </xsl:template>

    
 


</xsl:stylesheet>