import { Injectable } from '@angular/core';
import {MovieQuery, MovieResponse, MoviesResponse,CompileFilm} from './tmdb-data/Movie';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {PersonQuery, PersonResponse} from './tmdb-data/Person';
import {SearchMovieQuery, SearchMovieResponse} from './tmdb-data/searchMovie';
import {SearchPeopleQuery, SearchPeopleResponse} from './tmdb-data/SearchPeople';
import {TVQuery, TVResponse} from './tmdb-data/TV';
import {SearchTVQuery, SearchTVResponse} from './tmdb-data/SearchTV';
import 'rxjs/add/operator/map';

const tmdbApi = 'https://api.themoviedb.org/3';
type HTTP_METHOD = 'GET' | 'POST' | 'DELETE' | 'PUT';

function AlxToObjectString(data?: object): {[key: string]: string} {
  const res = {};
  for (const k of Object.keys(data || {}) ) {
    const v = data[k];
    res[k] = typeof v === 'string' ? v : JSON.stringify(v);
  }
  return res;
}

@Injectable({
  providedIn: 'root'
})
export class TmdbService {
  private apiKey: string;

  private async get<T>(url: string, data: object): Promise<HttpResponse<T>> {
    return this.http.get<T>( url, {
      observe: 'response',
      params: {...AlxToObjectString(data), api_key: this.apiKey}
    }).toPromise();
  }

  constructor(private http: HttpClient) {}

  init(key: string): this {
    this.apiKey = key;
    return this;
  }

  // _______________________________________________________________________________________________________________________________________
  // Movies ________________________________________________________________________________________________________________________________
  // _______________________________________________________________________________________________________________________________________
  async getMovie(id: number, options?: MovieQuery): Promise<MovieResponse> {
    const url = `${tmdbApi}/movie/${id}`;
    const res = await this.get<MovieResponse>(url, options);
    return res.body;
  }
  
  async getImage(id: number,option?: MovieQuery): Promise<string>{
      const res = await this.getMovie(id);
     const url  = `https://image.tmdb.org/t/p/w500/`+res.poster_path;
     return url;

  }

  async searchMovie(query: SearchMovieQuery): Promise<SearchMovieResponse> {
    const url = `${tmdbApi}/search/movie`;
    const res = await this.get<SearchMovieResponse>(url, query);
    return res.body;
  }

  async getChercherFilm(title: string, option?: SearchMovieQuery): Promise<SearchMovieResponse> {
    const url = `https://api.themoviedb.org/3/search/movie?api_key=b262a081cdabd053bb71fb789aa2f443&query=` + title ;
    const res = await this.get<SearchMovieResponse>(url, option);
    return res.body;
  }

  getFoundFilm(query:string){
      const url = `https://api.themoviedb.org/3/search/movie?api_key=b262a081cdabd053bb71fb789aa2f443&query=` + query ;
      return this.http.get(url);
  }

  // _______________________________________________________________________________________________________________________________________
  // Person / People _______________________________________________________________________________________________________________________
  // _______________________________________________________________________________________________________________________________________
  async getPerson(id: number, options?: PersonQuery): Promise<PersonResponse> {
    const url = `${tmdbApi}/person/${id}`;
    const res = await this.get<PersonResponse>(url, options);
    return res.body;
  }

  async searchPerson(query: SearchPeopleQuery): Promise<SearchPeopleResponse> {
    const url = `${tmdbApi}/search/person`;
    const res = await this.get<SearchPeopleResponse>(url, query);
    return res.body;
  }

  // _______________________________________________________________________________________________________________________________________
  // TV ____________________________________________________________________________________________________________________________________
  // _______________________________________________________________________________________________________________________________________
  async getTV(id: number, options?: TVQuery): Promise<TVResponse> {
    const url = `${tmdbApi}/tv/${id}`;
    const res = await this.get<TVResponse>(url, options);
    return res.body;
  }

  async searchTV(query: SearchTVQuery): Promise<SearchTVResponse> {
    const url = `${tmdbApi}/search/tv`;
    const res = await this.get<SearchTVResponse>(url, query);
    return res.body;
  }

  async getPopular(options?: MoviesResponse): Promise<MoviesResponse>{
      const url = `${tmdbApi}/movie/popular`;
      const res = await this.get<MoviesResponse>(url, options);
      return res.body;
  }
    async compilFilms(options?: MovieQuery):Promise<CompileFilm>{
        const url = `http://api.themoviedb.org/3/list/509ec17b19c2950a0600050d?api_key=b262a081cdabd053bb71fb789aa2f443`;
        const res = await this.get<CompileFilm>(url, options);
        return res.body;
    }

}
