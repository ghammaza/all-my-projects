import {typeArticle} from './typeArticle';

export interface articleInterface {
    id?: number;
    imgPath?: string;
    nom?: string;
    description?: string;
    type?: typeArticle;
    quantite?: number;
    prix?: number;
}
