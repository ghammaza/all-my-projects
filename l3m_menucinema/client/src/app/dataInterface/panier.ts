import {articleInterface} from './articleInterface';
export interface PanierInterface{
    id: number;
    articles:articleInterface[];
}