import {articleInterface} from './articleInterface';
export interface Facture{
    idFacture: number;
    articles: articleInterface[];
    total: number;
   
}