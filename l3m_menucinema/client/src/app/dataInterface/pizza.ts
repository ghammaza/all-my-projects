import {articleInterface} from './articleInterface';
import {typeArticle} from './typeArticle';

export interface PizzaInterface {
    infos:Information;
    ingredients: IngredientInterface[];
}


export interface IngredientInterface {
    ingredient: string;
}
export interface Information {
    id: number;
    prix: number;
    nom: string;
    type: typeArticle;
    imagePath: string;

}