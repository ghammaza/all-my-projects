import {PanierInterface} from './panier';
export interface personneInterface{
    id: number;
    nom: string;
    prenom: string;
    adresse: string;
    photo: string;
    mail: string;
    panier: PanierInterface[];
}
