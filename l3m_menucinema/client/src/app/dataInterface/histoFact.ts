import { Facture } from './facture';
export interface HistoFact{
    idPersonnr:string;
    factures:Facture[];
}