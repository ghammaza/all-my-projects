import { Component, OnInit } from '@angular/core';
import {ProfilService} from '../services/profil/profil.service';
import {AuthService} from '../services/auth/auth.service';
import {personneInterface} from  '../dataInterface/personne';
import {Router} from '@angular/router';

@Component({
    selector: 'app-profil-user',
    templateUrl: './profil-user.component.html',
    styleUrls: ['./profil-user.component.scss']
})
export class ProfilUserComponent implements OnInit {

    profileSerive: ProfilService;
    autService: AuthService;
    personne:personneInterface = new class implements personneInterface {
        adresse: "";
        id: null;
        mail: "";
        nom: "";
        panier: [];
        photo: "https://www.villascitemirabel.com/wp-content/uploads/2016/07/default-profile.png";
        prenom:"";
    };


    constructor(private profService: ProfilService, private authService: AuthService, private router: Router) {
        this.profileSerive = profService;
        this.autService = authService;
    }

    async ngOnInit() {

        this.personne = await this.profService.getData('client?action=getInfos&id='+this.authService.getIdUser());


        console.log(this.authService.getIdUser());
    }


    modifier(){

        let profilNom     = (<HTMLInputElement>document.getElementById("profilNom")).value;
        let profilPrenom  = (<HTMLInputElement>document.getElementById("profilPrenom")).value;
        let profilPhoto   = (<HTMLInputElement>document.getElementById("profilPhoto")).value;
        let profilTel     = (<HTMLInputElement>document.getElementById("profilTel")).value;
        let profilAdresse = (<HTMLInputElement>document.getElementById("profilAdresse")).value;



        let per : personneInterface = {
            id: this.authService.getIdUser(),
            nom: profilNom,
            prenom: profilPrenom,
            adresse: profilAdresse,
            photo: profilPhoto,
            mail: null,
            panier: null
        }

        let champs = "&id="+per.id+"&nom="+per.nom+"&prenom="+per.prenom+"&adresse="+per.adresse+"&photo="+per.photo+"&telephone="+profilTel;
        this.profService.postData('client?action=modifier'+champs);
        this.router.navigate(['/acceuil']);

    }

}
