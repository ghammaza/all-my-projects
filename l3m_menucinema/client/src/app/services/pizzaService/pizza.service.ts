import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {PizzaInterface} from '../../dataInterface/pizza';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class PizzaService {

    private _http: HttpClient;
    public get http(): HttpClient { return this._http; };

    constructor( http: HttpClient ) {
        this._http = http;
    }


     async getData<T>(url : string): Promise<T>{
         const response = await this.http.get("/api/"+url).toPromise();
         console.log(response)
        return <T>response;
    }


    async getOnePizza(id : number) : Promise<PizzaInterface>{
        const response = await this.http.get("/api/plats?action=retourner&id="+id).toPromise();
        console.log(response);
        return <PizzaInterface>response;
    }


}

