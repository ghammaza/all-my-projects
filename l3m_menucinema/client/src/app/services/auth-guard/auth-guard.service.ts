import {Injectable } from '@angular/core';
import {Router} from '@angular/router'
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase';

@Injectable()
export class AuthGuardService {

  constructor(private router :Router) { }

  canActivate():Promise<Boolean> | Boolean{
    return new Promise(
        (resolve,reject)=>{
          firebase.auth().onAuthStateChanged(
              (user) => {
                  if (user) {
                      resolve(true);
                  } else {
                      this.router.navigate(['/auth', 'signin']);
                      resolve(false);
                  }
              }
          );
        }
    );
  }



}
