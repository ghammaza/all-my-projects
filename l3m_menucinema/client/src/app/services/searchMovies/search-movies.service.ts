import {Injectable, OnInit} from '@angular/core';
import {TmdbService} from '../../tmdb.service';
import {SearchMovieResponse} from '../../tmdb-data/searchMovie';

@Injectable()
export class SearchMoviesService {


  constructor( private tmdb: TmdbService) { }
   public find = false;
  chercherFilm(query: string): Promise<SearchMovieResponse> {
      this.find = true;
      return this.tmdb.getChercherFilm(query);
  }
}
