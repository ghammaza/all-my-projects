import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {PanierInterface} from '../../dataInterface/panier';
import {Observable} from 'rxjs';
import {articleInterface} from '../../dataInterface/articleInterface';
import {forEach} from '@angular/router/src/utils/collection';
import {__await} from 'tslib';

@Injectable({
  providedIn: 'root'
})
export class SuggestionsService {

  private _http: HttpClient;

  public get http(): HttpClient {
    return this._http;
  }

  constructor(http: HttpClient) {
    this._http = http;
  }



    getSuggestions(forwhat:string, idArticle : string): Observable<articleInterface[]>{

      let arrayArticles : articleInterface[] = [];

      const t =  this.http.get("/api/client?action=getSuggestions&for="+forwhat+"&idArticle="+idArticle).subscribe(res => {

        const mapped = Object.keys(res).map(key => ({type: key, value: res[key]}));

        mapped.forEach(function(res) {

          const currentArticle: articleInterface = { id: null, imgPath:null, nom: null, description: null, type : null, quantite:null, prix:null};


          console.log(forwhat);
          console.log(res);
          currentArticle.id = res.value['id'];
          currentArticle.prix = res.value[forwhat]['prix'];
          currentArticle.imgPath = res.value[forwhat]['imagePath'];
          currentArticle.type = res.value[forwhat]['type'];
          currentArticle.nom = res.value[forwhat]['nom'];


          arrayArticles.push(currentArticle);

        });

    });

    return <any>arrayArticles;
  }
}
