import { TestBed } from '@angular/core/testing';

import { PanierServService } from './panier-serv.service';

describe('PanierServService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PanierServService = TestBed.get(PanierServService);
    expect(service).toBeTruthy();
  });
});
