import { PanierInterface } from '../../dataInterface/panier';
import { Injectable } from '@angular/core';
import {articleInterface} from '../../dataInterface/articleInterface';
import {typeArticle} from '../../dataInterface/typeArticle';
import {Observable ,of} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {AuthService} from '../auth/auth.service';


@Injectable({
  providedIn: 'root'
})
export class PanierService {

    panier :PanierInterface= new class implements PanierInterface{
        id:null;
        articles:[];
    }   ;
    public ajouter=true;

    private _http: HttpClient;


    constructor(private http: HttpClient, private authService: AuthService) {
        const dz: articleInterface = { id: null, imgPath:null, nom: null, description: null, type : null, quantite:null, prix:null};
        //console.log(this.panier);
        this._http = http;
        const dzdz:articleInterface[] = [];
        dzdz.push(dz);
        this.panier={id:null,articles: dzdz};
        this.ajouter=this.canAjouterFilm();
  }

    public gethttp(): HttpClient { return this._http; }

    suprimerArticle(index :number){

          //delete this.panier.articles[index];

        this.deleteArticle("client?action=supprimerArticle&id="+this.authService.getIdUser()+"&nomArticle="+this.panier.articles[index].nom);
        console.log("client?action=supprimerArticle&id="+this.authService.getIdUser()+"&nomArticle="+this.panier.articles[index].nom);
        this.panier.articles.splice(index, 1);


    }

    async deleteArticle(url: string): Promise<any> {
        const response = await this.http.get("/api/" + url).toPromise();

        return <any>response;
    }


    ajouterArticle(id: number,imgPath: string,nom: string, description: string, type: typeArticle, quantite: number,prixUnitaire: number){
        if(type ==="Film" ){
            this.ajouter=false;
        }
        console.log(this.ajouter);
        console.log(type);

        const article :articleInterface={
            id: id,
            imgPath: imgPath,
            nom: nom,
            description: description,
            type: type,
            quantite: quantite,
            prix: prixUnitaire
        };

      this.panier.articles.push(article);

      console.log("--> "+prixUnitaire);

        let commande = [
            {
                idPersonne : this.authService.getIdUser(),
                id: id,
                imgPath: imgPath,
                nom: nom,
                description: description,
                type: type,
                quantite: quantite,
                prix: prixUnitaire
            }
        ];

        this.ajouterArticleBDD("plats", commande);
    }

    async ajouterArticleBDD(url : string, obje : any) {
        const response = await this.http.post("/api/"+url, JSON.stringify(obje), ).toPromise();
    }

     async getPanier(idClient:string): Promise<PanierInterface>{
        this.panier= await this.getPanierBdd(idClient);
      return this.panier;
    }
    async getPanierBdd(idClient : string): Promise<PanierInterface>{
        const response = await this.http.get("/api/client?action=getPanier&id="+idClient).toPromise();
        return <PanierInterface>response;
    }
    canAjouterFilm():boolean{
        for(let art of this.panier.articles){
            console.log(art.type);
            if(art.type=="Film"){
                return false;
            }
        }
        return true;
    }



}
