import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {articleInterface} from '../../dataInterface/articleInterface'
import {personneInterface} from '../../dataInterface/personne';

@Injectable({
  providedIn: 'root'
})
export class ProfilService {


    private _http: HttpClient;
    public get http(): HttpClient {
        return this._http;
    }

    constructor(http: HttpClient) {
        this._http = http;
    }


    async getData(url: string): Promise<personneInterface> {
        const response = await this.http.get("/api/" + url).toPromise();

        return <personneInterface>response;
    }

    async postData(url: string) {

        const response = await this.http.get("/api/" + url).toPromise();

        console.log(response);
    }

}
