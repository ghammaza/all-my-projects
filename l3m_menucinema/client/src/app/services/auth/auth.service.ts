import {Injectable ,HostBinding} from '@angular/core';
import * as firebase from 'firebase';
//import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireAuth } from 'angularfire2/auth';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class AuthService {
    private user: Observable<firebase.User>;
    private userDetails: firebase.User = null;
    uidUser:string;
    error: any;

    private _http: HttpClient;
    public get http(): HttpClient { return this._http; }


    constructor(public af: AngularFireAuth,private router: Router,  http: HttpClient ) {
        this.user = af.authState;
        this._http = http;

        this.user.subscribe(
            (user) => {
                if (user) {
                    this.userDetails = user;
                    console.log(this.userDetails);
                    this.uidUser=user.uid;
                    console.log(this.uidUser);
                }
                else {
                    this.userDetails = null;
                }
            }
        );
    }



  createNewUser(email: string , mdp: string ){
    return new Promise(
      (resolve,reject)=>{

        firebase.auth().createUserAndRetrieveDataWithEmailAndPassword(email,mdp).then(
          ()=>{
            resolve();
          },
          (error)=>{
            reject(error);
          }
        );
      }
    );
  }

  getIdUser():any {
        return this.uidUser;
  }

  signInUser(email: string , mdp: string ){
    return new Promise(
      (resolve,reject)=>{
        firebase.auth().signInWithEmailAndPassword(email,mdp).then(
          ()=>{



              resolve();

          },
          (error)=>{
            reject(error);
          }
        );
      }
    );
  }
    signInWithGoogle() {
        return this.af.auth.signInWithPopup(
            new firebase.auth.GoogleAuthProvider()
        );
    }

    signInWithFacebook() {
        return this.af.auth.signInWithPopup(
            new firebase.auth.FacebookAuthProvider()
        );

    }

    public userExistInBDD(url: string): Observable<any> {
        return this.http.get<boolean>("/api/"+url);
    }

    async importUserInBDD(id: string, url: string):Promise<string> {

        let commande = [
            {
                action: 'create',
                idPersonne: id,
                nom: 'nA',
                prenom: 'nA',
                photo: 'https://www.villascitemirabel.com/wp-content/uploads/2016/07/default-profile.png',
                mail: 'nA',
                adresse: 'nA',
                telephone: 'nA'
            }
        ];

        const response = await this.http.post<string>("/api/"+url, JSON.stringify(commande), ).toPromise();
        return <string>response;
    }
  signOut(){
    firebase.auth().signOut();  
  }

}
