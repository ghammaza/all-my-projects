import { AuthService } from './../../services/auth/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
    signinForm: FormGroup;
    errorMessage: string;
    constructor(private formBuilder: FormBuilder,
                private authService: AuthService,
                private router: Router) {

    }

    ngOnInit() {
        this.initForm();

    }


    initForm() {
        this.signinForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]]
        });
    }

    signInWithFacebook() {
        this.authService.signInWithFacebook()
            .then((res) => {
                this.authService.userExistInBDD("client?action=exist&id="+this.authService.getIdUser()).subscribe(data => {
                    let res = data.toString();
                    if(res == "true"){
                        this.router.navigate(['/profil'])
                    }else{
                        const t = this.authService.importUserInBDD(this.authService.getIdUser(), "client?id="+this.authService.getIdUser());
                        this.router.navigate(['/profil']);
                    }
                });
            })
            .catch((err) => this.errorMessage=err);
    }
    signInWithGoogle() {
        this.authService.signInWithGoogle()
            .then((res) => {
                this.authService.userExistInBDD("client?action=exist&id="+this.authService.getIdUser()).subscribe(data => {
                    let res = data.toString();
                    if(res == "true"){
                        this.router.navigate(['/profil'])
                    }else{
                        const t = this.authService.importUserInBDD(this.authService.getIdUser(), "client?id="+this.authService.getIdUser());
                        this.router.navigate(['/profil']);
                    }
                });


            })
            .catch((err) => this.errorMessage=err);
    }


    onSubmit() {
        const email = this.signinForm.get('email').value;
        const password = this.signinForm.get('password').value;

        this.authService.signInUser(email, password).then(
            () => {
                    this.authService.userExistInBDD("client?action=exist&id="+this.authService.getIdUser()).subscribe(data => {
                        let res = data.toString();
                        if(res == "true"){
                            this.router.navigate(['/profil'])
                        }else{
                            const t = this.authService.importUserInBDD(this.authService.getIdUser(), "client?id="+this.authService.getIdUser());
                            this.router.navigate(['/profil']);
                        }
                    });
            },
            (error) => {
                this.errorMessage = error;
            }
        );
    }

}
