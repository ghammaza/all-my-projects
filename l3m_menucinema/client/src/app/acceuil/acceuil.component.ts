import { Component, OnInit } from '@angular/core';
import {TmdbService} from '../tmdb.service';
import {environment} from '../../environments/environment';
import {MoviesResponse, MovieResponse} from '../tmdb-data/Movie';
import {PizzaService} from '../services/pizzaService/pizza.service';
import {PizzaInterface} from '../dataInterface/pizza';
import {Information} from '../dataInterface/pizza';
import {typeArticle} from '../dataInterface/typeArticle';
import {PanierService} from '../services/panierService/panier-serv.service';
import {AuthService} from '../services/auth/auth.service';
import {SuggestionsService} from '../services/suggestions/suggestions.service';
import {articleInterface} from '../dataInterface/articleInterface';

@Component({
  selector: 'app-acceuil',
  templateUrl: './acceuil.component.html',
  styleUrls: ['./acceuil.component.scss']
})

export class AcceuilComponent  {
    indexPizza:number;
    pizzaOne:PizzaInterface = new class implements PizzaInterface {
        infos= new class implements Information{
            id:null;
            prix=null;
            nom="";
            type=typeArticle.P;
            imagePath="";
        };
        ingredients:[];
    };
    private suggestionsService = null;
    private articleInterface : articleInterface[] = [];

    pFilm: MoviesResponse= new class implements MoviesResponse {

        results: [];
        total_pages: null;
        total_results: null;
    };
    pizza:PizzaInterface[]=[];
    ajoutFilmPanier:boolean;

    constructor(private tmdb: TmdbService ,
                private pizzaServ : PizzaService ,
                private panierServ : PanierService,
                private authService: AuthService ,
                private suggesService : SuggestionsService
                ) {
        this.suggestionsService = suggesService;


        this.init();
  }

    async init() {

        this.pizza = await this.pizzaServ.getData<PizzaInterface[]>("plats?action=retourner&id=-1");
        this.tmdb.init( environment.tmdbKey );
        this.pFilm= await this.tmdb.getPopular();
        this.pFilm.results.splice(1,1);
        this.ajoutFilmPanier =this.panierServ.canAjouterFilm();
        this.pizzaOne.infos.nom="";
        this.pizzaOne.infos.id=0;
        this.pizzaOne.infos.imagePath="";
        this.pizzaOne.infos.prix=0;
        this.pizzaOne.infos.type= null;

    }

    async openModel(forwhat: string, idArticle :string , index : number){
        this.indexPizza=index;
        this.pizzaOne = this.pizza[index];
        console.log(this.pizzaOne);
        this.articleInterface = await this.suggestionsService.getSuggestions(forwhat, idArticle);
        console.log(this.articleInterface);
    }
    get film(): any {
        return this.pFilm.results;
    }
    ajouterPanier(id: number,imgPath: string,nom: string, description: string, type: number, quantite: number,prixUnitaire: number){

        if(type == 1){
            this.panierServ.ajouterArticle(id,imgPath,nom,description,typeArticle.F,1,10);
        }else{
            let descr: string;
            descr= this.Description(this.indexPizza);
            this.panierServ.ajouterArticle(id,imgPath,nom,descr,typeArticle.P,1,10);
        }
    }
    Description(index :number): string {
        let ingr : string;
        for(let ingredients of this.pizza[index].ingredients ){
            ingr +=ingredients.ingredient+" \n";
        }
        return ingr;
    }

}


