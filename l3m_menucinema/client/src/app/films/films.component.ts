import { Component, OnInit } from '@angular/core';
import {TmdbService} from './../tmdb.service';
import {environment} from './../../environments/environment';
import {CompileFilm} from '../tmdb-data/Movie';
import {SearchMoviesService} from '../services/searchMovies/search-movies.service';
import {SearchMovieResponse} from '../tmdb-data/searchMovie';
import {PanierService} from '../services/panierService/panier-serv.service';
import {typeArticle} from '../dataInterface/typeArticle';

@Component({
    selector: 'app-films',
    templateUrl: './films.component.html',
    styleUrls: ['./films.component.scss']
})
export class FilmsComponent {
    ajoutFilmPanier:boolean;
    compFilms:CompileFilm = new class implements CompileFilm {
        created_by: null;
        description: null;
        favorite_count: null;
        id: null;
        iso_639_1: null;
        item_count: null;
        items: [];
        name: null;
        poster_path: null;
    };
    image : any;
    actualPage:number=1;
    movieFound : boolean = false;
    find = false;
    films: any[];

    constructor(private tmdb: TmdbService, private searchService: SearchMoviesService , private panierServ :PanierService) {
        this.init();
    }

    async init() {
        this.tmdb.init( environment.tmdbKey );
        this.compFilms = await this.tmdb.compilFilms();
        this.compFilms.items.splice(1,1);
        this.ajoutFilmPanier =this.panierServ.canAjouterFilm();

    }

    get film(): any {
        return this.compFilms.items;
    }
    async  getimage(idMovie: number): Promise<string>{
        this.image = await this.tmdb.getImage(idMovie);
        return this.image;
    }
    /*search(title: string ): Promise<SearchMovieResponse> {
        return this.searchService.chercherFilm(title);
    }*/


    handleSuccess(data){
        this.movieFound = true;
        this.films = data.results;
        console.log(data.results);
    }

    handleError(error){
        console.log(error);
    }

    findMovie(query: string) {
        this.find = true;
        return this.tmdb.getFoundFilm(query).subscribe(
            data => this.handleSuccess(data),
            error => this.handleError(error),
            () => console.log('Request Completed')
        );
    }


    ajouterPanier(id: number,imgPath: string,nom: string, description: string, type: number, quantite: number,prixUnitaire: number){

        if(type == 1){
            this.panierServ.ajouterArticle(id,imgPath,nom,description,typeArticle.F,1,10);
        }else{
            this.panierServ.ajouterArticle(id,imgPath,nom,description,typeArticle.P,1,10);
        }

    }


}
