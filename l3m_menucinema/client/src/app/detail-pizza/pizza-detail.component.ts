import { Component, OnInit } from '@angular/core';
import {PizzaInterface} from '../dataInterface/pizza';
import {ActivatedRoute} from '@angular/router';
import {PizzaService} from '../services/pizzaService/pizza.service';
import {typeArticle} from '../dataInterface/typeArticle';
import {PanierService} from '../services/panierService/panier-serv.service';

@Component({
  selector: 'app-pizza-detail',
  templateUrl: './pizza-detail.component.html',
  styleUrls: ['./pizza-detail.component.scss']
})
export class PizzaDetailComponent implements OnInit {
    pizza:PizzaInterface;
  constructor( private route : ActivatedRoute, private pizzaServ : PizzaService ,private panierServ : PanierService) { }

  async ngOnInit() {
      const id= this.route.snapshot.params['id'];
      this.pizza= await this.pizzaServ.getOnePizza(id);
  }
  get nom(): string{
    return this.pizza.infos.nom;
  }
    Description(index :number): string {
        let ingr : string;
        for(let ingredients of this.pizza[index].ingredients ){
            ingr +=ingredients.nom;
        }
        return ingr;
    }
    ajouterPanier(id: number,imgPath: string,nom: string,index : number , type: typeArticle,prixUnitaire: number){
        let description: string;
        description= this.Description(index);
        this.panierServ.ajouterArticle(id,imgPath,nom,description,type,1,prixUnitaire);
    }
}
