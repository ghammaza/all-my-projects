import { Component, OnInit, Input } from '@angular/core';
import {TmdbService} from '../tmdb.service';
import {SearchMoviesService} from '../services/searchMovies/search-movies.service';
import {MovieResult, SearchMovieQuery, SearchMovieResponse} from '../tmdb-data/searchMovie';
import {typeArticle} from "../dataInterface/typeArticle";
import {PanierService} from "../services/panierService/panier-serv.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-search-movie',
    templateUrl: './search-movie.component.html',
    styleUrls: ['./search-movie.component.scss']
})
export class SearchMovieComponent implements OnInit {



    ajoutFilmPanier =this.panierServ.ajouter;
    films: any;
    public find:boolean = false;
    filmsTrouves: any;

    constructor(private tmdb: TmdbService, private searchServ: SearchMoviesService, private panierServ :PanierService, private router: Router) {  }

    ngOnInit() {
    }

    async searchFilm(){
        this.find = true;
        const title = (<HTMLInputElement>document.getElementById("cherche")).value;
        const data =  await this.searchServ.chercherFilm(title);
        this.films =  await data.results;
        //this.router.navigate(['cherche']);
        console.log(this.films);
        return this.films;
    }


    get Movies(){
        return this.films.results;
    }

    ajouterPanier(id: number,imgPath: string,nom: string, description: string, type: typeArticle, quantite: number,prixUnitaire: number){
        this.panierServ.ajouterArticle(id,imgPath,nom,description,type,1,10);
    }
}
