import { Component, OnInit } from '@angular/core';
import {PizzaService} from '../services/pizzaService/pizza.service';
import {PizzaInterface} from '../dataInterface/pizza';
import {PanierService} from '../services/panierService/panier-serv.service';
import {typeArticle} from '../dataInterface/typeArticle';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
    pizza:PizzaInterface[];

  constructor( private pizzaServ : PizzaService , private panierServ : PanierService) { }

  async ngOnInit() {
      this.pizza=await this.pizzaServ.getData<PizzaInterface[]>("plats?action=retourner&id=-1");

  }
    ajouterPanier(id: number,imgPath: string,nom: string,index : number , type: number,prixUnitaire: number){
        let description: string;
        description= this.Description(index);
        if(type == 1){
          this.panierServ.ajouterArticle(id,imgPath,nom,description,typeArticle.F,1,10);
      }else{
          this.panierServ.ajouterArticle(id,imgPath,nom,description,typeArticle.P,1,10);
      }
    }
    Description(index :number): string {
      let ingr : string;
    for(let ingredients of this.pizza[index].ingredients ){
        ingr +=ingredients.ingredient+" \n";
    }
      return ingr;
    }

}
