import {Component, OnInit} from '@angular/core';
import {PanierInterface} from '../dataInterface/panier';
import {articleInterface} from '../dataInterface/articleInterface';
import {ArticlesPanierComponent} from '../articles-panier/articles-panier.component';
import {typeArticle} from '../dataInterface/typeArticle';
import {PanierService} from '../services/panierService/panier-serv.service';
import {AuthService} from '../services/auth/auth.service';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.scss']
})
export class PanierComponent implements OnInit {


    panier :PanierInterface  ;


  constructor(private panierService :PanierService , private authServ: AuthService) {

  }

  async ngOnInit() {

    console.log(this.authServ.uidUser);
    this.panier= await this.panierService.getPanier(this.authServ.uidUser);
    console.log(this.panier);
  }

}