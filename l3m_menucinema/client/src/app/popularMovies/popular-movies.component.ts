import { Component, OnInit, Input } from '@angular/core';
import {MovieResponse} from '../tmdb-data/Movie';
import {TmdbService} from '../tmdb.service';
import * as firebase from 'firebase';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-popularMovies',
  templateUrl: './popular-movies.component.html',
  styleUrls: ['./popular-movies.component.scss']
})
export class PopularMoviesComponent implements OnInit {

    @Input() posterPath: string;
    @Input() idMovie: number;
    @Input() date: string;
    @Input() titre: string;
    @Input() popularity: number;
    @Input() moyenne: number;
    image: string;


    constructor(private tmdb: TmdbService) {

    }

    async ngOnInit() {
        this.image = await this.tmdb.getImage(this.idMovie);

   }
    getimage(): any {
        return this.tmdb.getImage(this.idMovie);
    }

    openModal(idMovie : number) {

    }
}
