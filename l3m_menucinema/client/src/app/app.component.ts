import { Component } from '@angular/core';
import {TmdbService} from './tmdb.service';
import {MovieResponse} from './tmdb-data/Movie';
import {environment} from '../environments/environment';
import * as firebase from 'firebase';
// Je suis passé par l'itération 0...
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  pFilm: MovieResponse;

  constructor(private tmdb: TmdbService) {
    this.init();
      var config = {
      apiKey: "AIzaSyBybLR4eioQzH7PKeDISB97zt81R-yUsfg",
      authDomain: "menucinema-ea073.firebaseapp.com",
      databaseURL: "https://menucinema-ea073.firebaseio.com",
      projectId: "menucinema-ea073",
      storageBucket: "menucinema-ea073.appspot.com",
      messagingSenderId: "681274373414"
    };
    firebase.initializeApp(config);
  }

  async init() {
    this.tmdb.init( environment.tmdbKey );
    this.pFilm= await this.tmdb.getMovie(18);
  }

  get film(): MovieResponse {
    return this.pFilm;
  }

}
