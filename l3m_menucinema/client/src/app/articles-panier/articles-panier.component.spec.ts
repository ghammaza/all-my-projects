import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticlesPanierComponent } from './articles-panier.component';

describe('ArticlesPanierComponent', () => {
  let component: ArticlesPanierComponent;
  let fixture: ComponentFixture<ArticlesPanierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticlesPanierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticlesPanierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
