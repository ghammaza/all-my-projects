import {Component, Input, OnInit} from '@angular/core';
import {articleInterface} from '../dataInterface/articleInterface';
import {PanierService} from '../services/panierService/panier-serv.service';
import {HttpClient} from '@angular/common/http';
import {AuthService} from '../services/auth/auth.service';
@Component({
  selector: 'app-articles-panier',
  templateUrl: './articles-panier.component.html',
  styleUrls: ['./articles-panier.component.scss']
})
export class ArticlesPanierComponent implements OnInit {
    @Input() article: articleInterface;
    @Input() idenx:number;
    @Input() tableauArticle:articleInterface[];

    private _http: HttpClient;

  constructor(private http: HttpClient, private panierService :PanierService,  private authService: AuthService) {
      this._http = http;
  }

    ngOnInit(): void {
      console.log("emmmmre");
    }

    supprimer(id:number){
        console.log("===> "+id);
        this.panierService.suprimerArticle(id);
    }

    changeTotalPrice(article: articleInterface){
        var prixHT =  (<HTMLInputElement>document.getElementById("prixU"+article.id+"")).textContent;

        var quant = (<HTMLInputElement>document.getElementById("quant"+article.id)).value;

        var total = parseInt(prixHT)*parseInt(quant);
        (<HTMLInputElement>document.getElementById("prixtotal"+article.id+"")).textContent = ''+total;
        this.setTotal();
    }

    getPrixTotal():number {
        let total = 0;

        this.tableauArticle.forEach(function (value) {
            console.log(total)
            let t = (<HTMLInputElement>document.getElementById("prixtotal"+value.id+"")).textContent;
            total = total + Number(t);
            console.log(total);
        });

        return total;
    }
    setTotal(){


        (<HTMLInputElement>document.getElementById("totalPanier")).value = ""+this.getPrixTotal();
    }
     totalAchat() : number{
      let tot :number=0;
      if(this.tableauArticle.length != null){
          for(let qt of this.tableauArticle){
              tot +=qt.quantite*qt.prix;
          }
      }

      return tot;
    }
    prixArticle(index:number) : number{
      return this.tableauArticle[index].quantite*this.tableauArticle[index].prix;
    }
    onOpen(event: any) {
        console.log(event);
    }

    commander() {
        let commande = [
            {
                action: 'commander',
                idPersonne: this.authService.getIdUser()
            }
        ];

        const response = this.http.post<string>("/api/client", commande).toPromise();

    }
}

