import { Component, OnInit } from '@angular/core';
import {TmdbService} from './../tmdb.service';
import {ActivatedRoute} from '@angular/router';
import {MovieResponse} from '../tmdb-data/Movie';
import {PanierService} from '../services/panierService/panier-serv.service';
import {typeArticle} from '../dataInterface/typeArticle';

@Component({
  selector: 'app-detail-film',
  templateUrl: './detail-film.component.html',
  styleUrls: ['./detail-film.component.scss']
})
export class DetailFilmComponent implements OnInit {
  film :MovieResponse = new class implements MovieResponse {
      adult: null;
      backdrop_path: "";
      belongs_to_collection: null;
      budget: 0;
      genres: [];
      homepage: "";
      id: null;
      imdb_id: "";
      original_language: "";
      original_title: "";
      overview: "";
      popularity: null;
      poster_path: "";
      production_companies: [];
      production_countries:[];
      release_date: "";
      revenue: null;
      runtime: null;
      spoken_languages: [];
      status: "";
      tagline: "";
      title: "";
      video: null;
      vote_average: null;
      vote_count: null;
  };
    ajoutFilmPanier:boolean;
  constructor(private tmdb: TmdbService , private route : ActivatedRoute , private panierServ :PanierService) { }

 async ngOnInit() {
    const id= this.route.snapshot.params['id'];
    this.film= await this.tmdb.getMovie(id);
    this.ajoutFilmPanier =this.panierServ.canAjouterFilm();

  }
    ajouterPanier(id: number,imgPath: string,nom: string, description: string, type: typeArticle, quantite: number,prixUnitaire: number){

        this.panierServ.ajouterArticle(id,imgPath,nom,description,type,1,10);
    }
}
