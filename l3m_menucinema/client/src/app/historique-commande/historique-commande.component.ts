import { articleInterface } from './../dataInterface/articleInterface';
import { Facture } from './../dataInterface/facture';
import { Component, OnInit } from '@angular/core';
import { HistoFact } from '../dataInterface/histoFact';
import { typeArticle } from '../dataInterface/typeArticle';
import {PizzaInterface} from '../dataInterface/pizza';
import {HttpClient} from '@angular/common/http';
import {AuthService} from '../services/auth/auth.service';

@Component({
  selector: 'app-historique-commande',
  templateUrl: './historique-commande.component.html',
  styleUrls: ['./historique-commande.component.scss']
})
export class HistoriqueCommandeComponent implements OnInit {
    private _http: HttpClient;
    public get http(): HttpClient { return this._http; };

    art :articleInterface = new class implements articleInterface{
      id:1;
      imgPath: "https://www.google.com/search?q=image+pizza&rlz=1C1CHBF_frFR833FR833&source=lnms&tbm=isch&sa=X&ved=0ahUKEwipgfn62JPiAhUM8hQKHXu_C2IQ_AUIDigB&biw=1366&bih=625#imgrc=M90on_DhCrDgEM:";
      nom: "pizza a kaka et pipi";
      description: "moueleux a l'exterieur , croustillant a l'interieur";
      type: typeArticle.P;
      quantite: 12;
      prix?: 10000;

    }
      histo : HistoFact = new class  HistoFact{
        idPersonnr:"absCn4rYKGSxXLYAD383cf3AVDz1";
        factures:[];
      };

      fact:Facture = new class  Facture {
        idFacture:1;
        articles: [];
        total :100;
      };
     
  constructor( private authserv : AuthService ,http: HttpClient) {
      this._http = http;
  }

  async ngOnInit() {
    /*this.fact.articles.push(this.art);
    this.fact.articles.push(this.art);
    this.fact.articles.push(this.art);
   */
    this.art.imgPath="https://www.google.com/search?q=image+pizza&rlz=1C1CHBF_frFR833FR833&source=lnms&tbm=isch&sa=X&ved=0ahUKEwipgfn62JPiAhUM8hQKHXu_C2IQ_AUIDigB&biw=1366&bih=625#imgrc=M90on_DhCrDgEM:";
    this.art.id=1;
    this.art.prix=10;
    this.art.quantite=12;
    this.art.nom="pizza a kaka et pipi";
    this.art.type=typeArticle.P;
    this.art.description="moueleux a l'exterieur , croustillant a l'interieur";

    this.fact.idFacture=1;
    this.fact.total=100;
    this.fact.articles=[];
    this.fact.articles.push(this.art);
    this.fact.articles.push(this.art);
    this.fact.articles.push(this.art);

    this.histo.idPersonnr="absCn4rYKGSxXLYAD383cf3AVDz1";
    this.histo.factures=[];
    this.histo.factures.push(this.fact);
    this.histo.factures.push(this.fact);
    this.histo.factures.push(this.fact);
    this.histo.factures.push(this.fact);
    this.histo.factures.push(this.fact);
    this.histo.factures.push(this.fact);
    
    this.histo= await this.getFactures();
  }
  ///api/client?action=getFactures&id=  +this.authserv.getIdUser()
    async getFactures() : Promise<HistoFact>{

        const response = await this.http.get("/api/client?action=getFactures&id="+this.authserv.getIdUser()).toPromise();
        console.log(response);
        return response as HistoFact;
    }

  

}
