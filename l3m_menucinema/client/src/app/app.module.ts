import {AuthService} from './services/auth/auth.service'
import { BrowserModule } from '@angular/platform-browser';
import { NgModule,NO_ERRORS_SCHEMA  } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {TmdbService} from './tmdb.service';
import {HttpClientModule} from '@angular/common/http';
import {MDBBootstrapModule} from 'angular-bootstrap-md';
import { AcceuilComponent } from './acceuil/acceuil.component';
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';
import { HeaderComponent } from './header/header.component';
import {PopularMoviesComponent} from './popularMovies/popular-movies.component'
import {  ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AuthGuardService } from './services/auth-guard/auth-guard.service';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import { environment } from '../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { FilmsComponent } from './films/films.component';
import { PanierComponent } from './panier/panier.component';
import { ArticlesPanierComponent } from './articles-panier/articles-panier.component';
import {SearchMoviesService} from './services/searchMovies/search-movies.service'
import {NgxPaginationModule} from 'ngx-pagination';
import {SearchMovieComponent} from './searchMovie/search-movie.component';
import { DetailFilmComponent } from './detail-film/detail-film.component';
import { MenuComponent } from './menu/menu.component';
import {PanierService} from './services/panierService/panier-serv.service';
import { PizzaDetailComponent } from './detail-pizza/pizza-detail.component';
import { ProfilUserComponent } from './profil-user/profil-user.component';
import { HistoriqueCommandeComponent } from './historique-commande/historique-commande.component';

const appRoutes : Routes =[
    {path: 'auth/signin' , component: SigninComponent},
    {path: 'auth/signup', component: SignupComponent},
    {path:'acceuil',canActivate:[AuthGuardService],component:AcceuilComponent},
    {path: 'panier', canActivate:[AuthGuardService],component: PanierComponent},
    {path: 'historique', canActivate:[AuthGuardService],component: HistoriqueCommandeComponent},
    {path:'pizza', canActivate:[AuthGuardService],component: MenuComponent},
    {path:'pizza/:id', canActivate:[AuthGuardService],component: PizzaDetailComponent},
    {path:'films',canActivate:[AuthGuardService],component:FilmsComponent},
    {path:'films/:id',canActivate:[AuthGuardService],component:DetailFilmComponent},
    //{path:'films',canActivate:[AuthGuardService],component:FilmsComponent},
    {path:'films/search',component:SearchMovieComponent},
    {path:'search',component:SearchMovieComponent},

    {path:'profil',canActivate:[AuthGuardService],component:ProfilUserComponent},
    {path:' ', redirectTo:'acceuil'},
    {path:'**',redirectTo:'acceuil'}
];
export const firebaseConfig = {
    apiKey: 'AIzaSyBybLR4eioQzH7PKeDISB97zt81R-yUsfg',
    authDomain: ' menucinema-ea073.firebaseapp.com '
};
@NgModule({
    declarations: [
        AppComponent,
        AcceuilComponent,
        SignupComponent,
        SigninComponent,
        PopularMoviesComponent,
        HeaderComponent,
        FilmsComponent,
        PanierComponent,
        ArticlesPanierComponent,
        DetailFilmComponent,
        MenuComponent,
        DetailFilmComponent,
        SearchMovieComponent,
        DetailFilmComponent,
        PizzaDetailComponent,
        ProfilUserComponent,
        HistoriqueCommandeComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        SlickCarouselModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        MDBBootstrapModule.forRoot(),
        AngularFireModule.initializeApp(firebaseConfig),
        AngularFireDatabaseModule,
        AngularFireAuthModule,
        RouterModule.forRoot(appRoutes)
    ],
    schemas : [NO_ERRORS_SCHEMA],
    providers: [TmdbService ,AuthService,AuthGuardService,SearchMoviesService,PanierService],
    exports: [ RouterModule ],
    bootstrap: [AppComponent]
})
export class AppModule { }
