<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : Plats.xsl
    Created on : 19 avril 2019, 15:08
    Author     : orandr
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>Plats.xsl</title>
            </head>
            <body>
                <h1>Menu des Pizzas</h1>
                
            </body>
        </html>
    </xsl:template>
    
     <section>
        <table>
            <tr>
                <td>
                    <xsl:apply-templates select="//ns1:id"/> 
                </td>
                <td> 
                    <xsl:apply-templates select="//ns1:type"/> 
                </td>
                <td> 
                    <xsl:apply-templates select="//ns1:prix"/> 
                </td>
                <td> 
                    <xsl:apply-templates select="//ns1:ingredients"/> 
                </td>
            </tr>
        </table>
    </section>    

    <xsl:template match="ns1:ingredients">     
        <xsl:value-of select="."/>
        <br></br>
    </xsl:template>  

</xsl:stylesheet>
