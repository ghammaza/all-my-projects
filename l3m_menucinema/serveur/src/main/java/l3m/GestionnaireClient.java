/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l3m;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import static l3m.SQLAble.st;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
/**
 *
 * @author madidea    
*/
public class GestionnaireClient extends SQLAble {
    private Client client;
    
    public GestionnaireClient() throws SQLException{
        connectToDatabase();        
    }
    
    public GestionnaireClient(String id,String nom,String prenom) throws SQLException, Exception{
        client = new Client();
    	connectToDatabase();
        if(!existClientIdNomPrenom(id,nom,prenom)){
            throw new Exception("L'identifiant ne correspond pas au nom et/ou prenom du client");
        }
        
        client.setId(id);
        client.setNom(nom);
        client.setPrenom(prenom);
        String photo;
        photo ="";
        String email;
        email ="";
        String tel;
        tel ="";
        String adresse;
        adresse ="";
        String rq = "Select photo,mail,tel,adresse from Personne where idPersonne ="+Integer.parseInt(id)+" and nom = '"+nom +"' and prenom = '"+prenom+"'";
        //System.out.println(rq);
        ResultSet rs = request(rq);
        while(rs.next()) {
            photo = rs.getString(1);
            email = rs.getString(2);
            tel = rs.getString(3);
            adresse = rs.getString(4);
            client.setPhoto(photo);
            client.setEmail(email);
            client.setTel(tel);
            client.setAdresse(adresse);
        }
        //disconnect();
    }
    /**
     * 
     * donne le nom du client courant 
     */
    public String getNom(){
        return client.getNom();
    }
    
    /**
     * 
     * donne le prenom du client courant
     */
    public String getPrenom(){
        
        return client.getPrenom();
    }
    
     /**
     * 
     * donne la photo du client courant
     */
    public String getPhoto(){
        return client.getPhoto();
    }
    
     /**
     * 
     * donne l'adresse du client courant
     */
    public String getAdresse(){
        return client.getAdresse();
    }
    
     /**
     * 
     * modifie l'adresse mail du client courant
     */
    public void editEmail(String email) throws SQLException {
       String rq="update Personne set mail = '"+ email +"' where idPersonne='"+client.getId()+"' ";   
       //System.out.println(rq);
       // select mail from Personne where id=
       requestUpdate(rq);
    }
    
    /**
     * 
     * modifie l'adresse du logement du client 
     */
    public void editAdresse(String adresse) throws SQLException{
        String rq="update Personne set adresse = '"+ adresse +"' where idPersonne='"+client.getId()+"' ";
        //System.out.println(rq);
        requestUpdate(rq);
    }
    
    /**
     * 
     * modifie le numero de telephone du client 
     */
    public void editTel(String tel) throws SQLException{
        String rq="update Personne set tel = '"+ tel +"' where idPersonne='"+client.getId()+"' ";
        //System.out.println(rq);
        requestUpdate(rq);
    }
    
    /**
     * 
     * modifie la photo du client 
     */
    public void editPhoto(String photo) throws SQLException{
        String rq="update Personne set photo = '"+ photo +"' where idPersonne='"+client.getId()+"' ";
        //System.out.println(rq);
        requestUpdate(rq);
    }
    
    // reliée angular
    /**
     * 
     *recuperation des informations du client grace à l'id et les renvoient dans le fichier JSON
     */
    public JSONObject getClientInfosById(String id) throws SQLException{

        
        ResultSet rs;
        String nom      = "";
        String prenom   = "";
        String photo    = "";
        String mail     = "";
        String tel      = "";
        String adresse = "";
        
        JSONObject personne = new JSONObject();
        
        rs = request("SELECT nom, prenom, photo, mail, tel, adresse FROM Personne WHERE idPersonne='"+id+"'");
        //System.out.println(rs);
        while(rs.next()) {
            nom     = rs.getString(1);
            prenom  = rs.getString(2);
            photo   = rs.getString(3);
            mail    = rs.getString(4);
            tel     = rs.getString(5);
            adresse = rs.getString(6);
        }
        
        personne.put("nom", nom);
        personne.put("prenom", prenom);
        personne.put("photo", photo);
        personne.put("mail", mail);
        personne.put("tel", tel);
        personne.put("adresse", adresse);
        
        
        return personne;
    }
    
    // reliée angular
    /**
     * 
     * verifie si le client avec les informations données en parametre existe déjà dans la base de données
     */
    public boolean enregistrerClientBD(String id, String nom, String prenom, String photo, String mail, String adresse, String telephone) throws SQLException{
           
        String n ="insert into Personne(idPersonne, nom, prenom, photo, mail, tel, adresse ) values ('"+id+ "','"+nom+ "', '"+prenom+ "', '"+photo+ "','"+mail+ "','"+telephone+ "', '"+adresse+"') ";
        //System.out.println(n);
        //connectToDatabase();
        int nb = 0;
    	nb = st.executeUpdate(n);
        //disconnect();
        return (nb!=0);
        
    }
    
    /**
     * 
     * modifie les informations données en parametre du client grâce à l'id
     */
    public void setClientBdByUserID(String id, String nom, String prenom, String photo, String adresse, String telephone) throws SQLException{
           
        String n ="update personne "
                + " set nom = '"+nom+ "',  prenom = '"+prenom+ "', photo = '"+photo+ "' "
                + ", tel = '"+telephone+ "', adresse = '"+adresse+"' where idPersonne = '"+id+"'  ";
        
        //System.out.println(n);
        request(n);
    }
    
    
    /**
     * 
     * verifie si le client existe déjà avec les informations données en parametre 
     */
    public boolean existClientIdNomPrenom(String id,String nom,String prenom) throws SQLException{
        boolean b;
        b=false;
        ResultSet rs;
        String n= "SELECT idPersonne,nom, prenom FROM Personne WHERE idPersonne='"+id+"' and nom='"+nom+"' and prenom='"+prenom+"' ";
        //System.out.println(n);
        rs = request("SELECT idPersonne FROM Personne WHERE nom='"+nom+"' and prenom='"+prenom+"' ");
        //System.out.println(rs);
        while(rs.next()) {
            b=true;
        }
        return b;
    }
    
    
    /**
     * 
     * verifie le client exist grâce à l'id
     */
    protected boolean existsClientDB(String id) throws SQLException{
        String n = "SELECT * FROM Personne WHERE idPersonne='"+id+"'"; 
        System.out.println(n);
        int nb = 0;
    	nb = st.executeUpdate(n);
        return (nb!=0);
    }
    
    
    
//    public void editClientDB() throws SQLException{
//        
//        System.out.println("Veuillez saisir votre nom");
//        String nom = LectureClavier.lireChaine();
//        client.setNom(nom);
//        
//        System.out.println("Veuillez saisir votre prenom");
//        String prenom = LectureClavier.lireChaine();
//        client.setPrenom(prenom);
//        
//        System.out.println("Veuillez saisir votre photo");
//        String photo = LectureClavier.lireChaine();
//        client.setPhoto(photo);
//        
//        System.out.println("Veuillez saisir votre mail");
//        String mail = LectureClavier.lireChaine();
//        client.setEmail(mail);
//        
//        System.out.println("Veuillez saisir votre adresse");
//        String adresse = LectureClavier.lireChaine();
//        client.setAdresse(adresse);
//        
//        System.out.println("Veuillez saisir votre telephone");
//        String telephone = LectureClavier.lireChaine();
//        client.setTel(telephone);
//        
//        
//        if(!existsClientDB()){
//            String rq="update Personne set nom = '"+nom+"', prenom = '"+prenom+"', photo = '"+photo+"', mail = '"+mail+"', adresse = '"+adresse+"', tel = '"+telephone+"' where idPersonne=" + client.getId();
//            //System.out.println(rq);
//            requestUpdate(rq);
//        }else{
//            System.out.println("Erreur, editClientDB() client existe deja");
//        }
//        
//    }
    
    /**
     * 
     * recupere l'id du client gràâce au nom et au prenom
     */
    public String getClientIdDB(String nom,String prenom) throws SQLException{
        String id;
        ResultSet rs;
        id = "";
        rs = request("SELECT idPersonne FROM Personne WHERE nom='"+nom+"' and prenom='"+prenom+"' ");
        //System.out.println(rs);
        while(rs.next()) {
            id = rs.getString(1);
        }
        return id;
    }
    
    
    /**
     * 
     * supprime le client courant
     */
    public boolean deleteClientDB() throws SQLException{
        String n = "DELETE FROM Personne WHERE idPersonne ='"+client.getId()+"' ";
        //System.out.println(n);
        //connectToDatabase();
        int nb = 0;
    	nb = st.executeUpdate(n);
        System.out.println("Le client a bien été supprimé");
        //disconnect();
        return (nb!=0);
    }
    
    
    /**
     * 
     * recupere la liste des id des commandes
     */
    public List<String> getListeCommandes() throws SQLException{
        String idCommande;
        idCommande ="";
        ResultSet rs;
        List<String> list = new ArrayList<>();
        String n ="SELECT idCommande FROM Commande WHERE idPersonne='"+Integer.parseInt(client.getId())+"' ";
        System.out.println(n);
        rs = request(n);
        
         while(rs.next()) {
             list.add(rs.getString(1));
         } 
        //System.out.println(list.size());
        return list;
    }
}
