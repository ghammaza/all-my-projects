/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l3m;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


/**
 *
 * @author orandr
 */
public class Plats {
    private ArrayList<Plat> plats = new ArrayList<Plat>();
    private static final String nomFichier = "./Plats.xml";
    
    /**
     * 
     * @param reference est la reference vers l'id de l'attribut d'un plat
     * @return le prix du plat ayant pour attribut la "reference" passé en paramètre
     * @throws SQLException 
     */
    public double getPrixPlat(String reference) throws SQLException{
        Document _doc;
        double prix;
        prix = 0.0;
        File f = new File(nomFichier);
        if(f.exists()){
            _doc = fromXML(nomFichier);
            NodeList pplat = _doc.getElementsByTagName("ns1:plat");
            String id="";
            int i=0;
            boolean b = false;
            Element e;
            while(i<pplat.getLength() && !b){
                e  = (Element)pplat.item(i);
                if(e.getAttribute("id").equals(reference)){
                   id=e.getAttribute("id");
                   b=true;
                }else{
                    i++;
                }
            }
            if(b){
                NodeList platReference = pplat.item(i).getChildNodes();
                for(i=0;i<platReference.getLength();i++){
                    if(platReference.item(i).getNodeName().equals("ns1:prix")){
                        prix = Double.parseDouble(platReference.item(i).getTextContent());
                    }
                }
            }else{
                System.out.println("Erreur, la reference n°"+reference+" n'existe pas !");
            }
        }else{
            System.out.println("Erreur, getPrixPlat("+reference+") le fichier "+nomFichier +" n'exist pas !");
        }
        return prix;
    }
    
    /**
     * 
     * @param id correspond à l'id dans le ArrayList<Plat> plats.
     */
    
    public void removePlatId(String id){
        int i=0;
        for (i = 0; i < plats.size(); i++) {
            if(plats.get(i).getId().equals(id)){
                plats.remove(i);
            }
        }
        if(i==plats.size()){
            System.out.println("L'id n°"+id+" n'etait pas dans le menu !");
        }
    }
    
    public void ajouterPlat(Plat p){
        this.plats.add(p);
    }
    
    /**
     * 
     * @param nomFichier correspond au nom de fichier en entrer permettant de generer le Document
     * @return le document generer par le fichier XML passer en parametre
     */
    //Correspond au getCarteDB
    // Cree un DOM à partir d'un fichier XML
    public Document fromXML(String nomFichier) {
        try {
            return XMLUtil.DocumentFactory.fromFile(nomFichier);
        } catch (Exception ex) {
            System.out.println("Erreur, fromXML("+nomFichier+")");
        }
        return null;
    }
    
   
    /**
     * Cette fonction permet de creer un fichier Menu.xml qui possède toute les caractéristiques de "ArrayList<Plat> plats"
     */
    public void toXML(){
        Element e = null;
        String nomFichier = "./Menu.xml";

        // instance of a DocumentBuilderFactory
            // create instance of DOM
            Document document = null;
            DocumentBuilderFactory fabrique = null;
         try {
           
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            document = docBuilder.newDocument();
            Element rootElement = document.createElement("ns1:plats");
            rootElement.setAttribute("xmlns:ns1", "http://ujf-grenoble.fr/plats");
            rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            rootElement.setAttribute("xsi:schemaLocation", "http://ujf-grenoble.fr/plats file:/home/o/orandr/Bureau/l3m_menucinema/serveur/src/main/java/l3m/Plats.xsd");
      
            document.appendChild(rootElement);          
            for (int i = 0; i < this.plats.size(); i++) {
                Plat platCourant = this.plats.get(i);
          
                Element rootPlat = document.createElement("ns1:plat");
                rootPlat.setAttribute("id", platCourant.getId());
                rootElement.appendChild(rootPlat);
                
                Element nom = document.createElement("ns1:nom");
                nom.appendChild(document.createTextNode(platCourant.getNom()));                
                rootPlat.appendChild(nom);
                
                Element type = document.createElement("ns1:type");
                type.appendChild(document.createTextNode(platCourant.getType()));
                rootPlat.appendChild(type);
                
                Element prix = document.createElement("ns1:prix");
                prix.appendChild(document.createTextNode(""+platCourant.getPrix()));
                rootPlat.appendChild(prix);
                
                Element imagePath = document.createElement("ns1:imagePath");
                imagePath.appendChild(document.createTextNode(platCourant.getImagePath()));
                rootPlat.appendChild(imagePath);         
                
                Element rootIngredients = document.createElement("ns1:ingredients");
                rootPlat.appendChild(rootIngredients);
                
                ArrayList<String> listIngredientCourant = platCourant.getIngredients();
              
                for (int j = 0; j < listIngredientCourant.size(); j++) {
                    //ns1:ingredients                   
                    Element ingredientCourant = document.createElement("ns1:ingredient");
                    ingredientCourant.appendChild(document.createTextNode(listIngredientCourant.get(j)));
                    rootIngredients.appendChild(ingredientCourant);
                    
                }
              
            }
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer = transformerFactory.newTransformer();
                DOMSource source = new DOMSource(document);
                StreamResult result = new StreamResult(new File(nomFichier));
                transformer.transform(source, result);
         } catch (Exception eo){
             System.out.println(eo);
         }
         
    }
    
    /**
     * 
     * @param idCommande est l'identifiant d'une commande et le prix lui correspondant
     * @param prix correspond au prix de la commande que l'on veut enregistrer
     * @throws SQLException 
     */
    public void ajoutFacture(Number idCommande,String prix) throws SQLException{
        
        XMLAble x = new XMLAble();
        x.connectToDatabase();
        x.requestUpdate("INSERT INTO Facture(idCommande,prix) VALUES("+idCommande+",'"+prix+"')");

    }
    
    /**
     * Permet de generer un fichier XML grace au facture recuperer depuis la Base De Donnee
     * @throws SQLException 
     */
    public void factureDBtoXML() throws SQLException{
        
        XMLAble x = new XMLAble();
        x.connectToDatabase();
        ResultSet rs=null;
        String idFacture="";
        String idCommande="";
        String prix = "";
        String rq="";
        ArrayList<String>  listIdFacture = new ArrayList<String>();
        ArrayList<String>  listIdCommande = new ArrayList<String>();
        ArrayList<String>  listPrix = new ArrayList<String>();
        
        
        rs = x.request("SELECT idFacture,idCommande, prix FROM facture");
        while(rs.next()){
            idFacture = rs.getString(1);
            idCommande = rs.getString(2);
            prix = rs.getString(3);
            
            listIdFacture.add(idFacture);
            listIdCommande.add(idCommande);
            listPrix.add(prix);
            
        }    
        factureToXML(listIdFacture, listIdCommande, listPrix);
        x.disconnect();
    }
    
    /**
     * 
     * @param idPersonne correspond à l'identifiant de la personne à qui on souhaite récuperer les factures
     * @return un objet JSON recapitulatif des informations de facturation de la personne possedant l' "idPersonne"
     * @throws SQLException 
     */
    public JSONObject factureIdToJSONArrayVersionAy(String idPersonne) throws SQLException{
        
        JSONArray listFact = new JSONArray();
        
        SQLAble x = new SQLAble();
        x.connectToDatabase();
        ResultSet rs=null;
        String idFacture="";
        String newIdFacture="";
        String nomArticle="";
        String newNomArticle="";
        String typeArticle = "";
        String newTypeArticle = "";
        String imagePathArticle="";
        String newImagePathArticle="";
        String prixArticle="";
        String newPrixArticle="";
        String prixTotal="";
        String newPrixTotal="";
        String quantite="";
        String newQuantite="";
        String idArticle="";
        String newIdArticle="";
        String rq="";
        String idA="";
        JSONObject  facturesList = new JSONObject(); 
        JSONArray factures = new JSONArray();
        JSONArray articles = new JSONArray();
        JSONObject  article = new JSONObject(); 
        
        //   ,to_char(C.dateCommande,'DD/MM/YYYY HH24:MI:SS') C.dateCommande, A.nom
        rq="SELECT F.idFacture,A.nom,A.type,A.imagePath,A.prix As prixA,F.prix,C.quantite,A.idArticle "
        + " FROM Personne P,Commande C,Facture F,Article A WHERE P.idPersonne=C.idPersonne AND"
                + " A.idArticle=C.idArticle AND F.idCommande=C.idCommande AND P.idPersonne='"+idPersonne+"' ORDER BY F.idFacture";
        System.out.println(rq);
        //System.out.println("");
        //System.out.println("F.idFacture,A.nom,A.type,A.imagePath,A.prix As prixA,F.prix,C.quantite,A.idArticle");
        boolean premier=true;
        int i=0;
        rs = x.request(rq);
        while(rs.next()){
            // System.out.println("Dans le while "+i);
            idFacture=newIdFacture;
            nomArticle=newNomArticle;
            typeArticle=newTypeArticle;
            imagePathArticle=newImagePathArticle;
            prixArticle=newPrixArticle;
            prixTotal=newPrixTotal;
            quantite=newQuantite;
            idArticle=newIdArticle;
            
            newIdFacture = rs.getString(1);
            newNomArticle = rs.getString(2);
            newTypeArticle = rs.getString(3);
            newImagePathArticle = rs.getString(4);
            newPrixArticle = rs.getString(5);
            newPrixTotal = rs.getString(6);
            newQuantite = rs.getString(7);
            newIdArticle = rs.getString(8);
            //idA = rs.getString(8);
            //System.out.println("old "+idFacture+" "+nomArticle+" "+typeArticle+"  "+imagePathArticle+" "+prixArticle+" "+quantite+" "+idArticle );
            
            //System.out.println("new "+newIdFacture+" "+newNomArticle+" "+newTypeArticle+"  "+newImagePathArticle+" "+newPrixArticle+" "+newQuantite+" "+newIdArticle );
            
            if(premier){
                idFacture=newIdFacture;
                nomArticle=newNomArticle;
                typeArticle=newTypeArticle;
                imagePathArticle=newImagePathArticle;
                prixArticle=newPrixArticle;
                prixTotal=newPrixTotal;
                quantite=newQuantite;
                idArticle=newIdArticle;
                premier=false;
            }
            //System.out.println("old2 "+idFacture+" "+nomArticle+" "+typeArticle+"  "+imagePathArticle+" "+prixArticle+" "+quantite+" "+idArticle );

                    
            article = new JSONObject();
            article.put("idArticle", newIdArticle);
            article.put("nomArticle", newNomArticle);
            article.put("typeArticle", newTypeArticle);
            article.put("imagePathArticle", newImagePathArticle);    
            article.put("quantite", newQuantite);
            article.put("prixArticle", newPrixArticle);
            if(!idFacture.equals(newIdFacture)){
                //System.out.println("if");
                facturesList = new JSONObject();
                facturesList.put("idFacture",  idFacture);
                facturesList.put("prixTotalFacture", prixTotal);
                facturesList.put("articles",articles);
                listFact.add(facturesList);
                articles = new JSONArray();
            }else{
                //System.out.println("Else");
                articles.add(article);
                article = new JSONObject(); 
            }
            i++;            
            System.out.println("");
        }
        
        JSONObject  newarticles = new JSONObject(); 
        newarticles.put("idArticle", idArticle);
        newarticles.put("nomArticle", nomArticle);
        newarticles.put("typeArticle", typeArticle);
        newarticles.put("imagePathArticle", imagePathArticle);    
        newarticles.put("quantite", quantite);
        newarticles.put("prixArticle", prixArticle);
        articles.add(newarticles);
        
        
        
        
        
        facturesList = new JSONObject();
        facturesList.put("idFacture",  idFacture);
        facturesList.put("prixTotalFacture", prixTotal);
        facturesList.put("articles",articles);
        listFact.add(facturesList);
//        JSONObject  newListFact = new JSONObject(); 
//        newListFact.put("factures",factures);
//        listFact.add(newListFact);
        JSONObject  factureFinal = new JSONObject(); 
        factureFinal.put("Facture",listFact);
        factureFinal.put("idClient",idPersonne);
        x.disconnect();
        
        return factureFinal;
    }
    
    /**
     * 
     * @param idPersonne correspond à l'identifiant de la personne à qui on souhaite récuperer les factures
     * @return un tableau JSON recapitulatif des informations de facturation de la personne possedant l' "idPersonne"
     * @throws SQLException 
     */
    public JSONArray factureIdToJSONArray(String idPersonne) throws SQLException{
        
        JSONArray listFact = new JSONArray();
        
        SQLAble x = new SQLAble();
        x.connectToDatabase();
        ResultSet rs=null;
        String idFacture="";
        String newIdFacture="";
        String nomArticle="";
        String newNomArticle="";
        String typeArticle = "";
        String newTypeArticle = "";
        String imagePathArticle="";
        String newImagePathArticle="";
        String prixArticle="";
        String newPrixArticle="";
        String prixTotal="";
        String newPrixTotal="";
        String quantite="";
        String newQuantite="";
        String dateComm="";
        String newDateComm="";
        String rq="";
        String idA="";
        JSONObject  obj = new JSONObject(); 
        JSONArray facts = new JSONArray();
        JSONObject  fact = new JSONObject(); 
        
        //   ,to_char(C.dateCommande,'DD/MM/YYYY HH24:MI:SS') C.dateCommande, A.nom
        rq="SELECT F.idFacture,A.nom,A.type,A.imagePath,A.prix As prixA,F.prix,C.quantite,A.idArticle "
        + " FROM Personne P,Commande C,Facture F,Article A WHERE P.idPersonne=C.idPersonne AND"
                + " A.idArticle=C.idArticle AND F.idCommande=C.idCommande AND P.idPersonne='"+idPersonne+"' ORDER BY F.idFacture";
        System.out.println(rq);
        //System.out.println("");
        //System.out.println("F.idFacture,A.nom,A.type,A.imagePath,A.prix As prixA,F.prix,C.quantite,A.idArticle");
        boolean premier=true;
        int i=0;
        rs = x.request(rq);
        while(rs.next()){
            System.out.println("Dans le while "+i);
            idFacture=newIdFacture;
            nomArticle=newNomArticle;
            typeArticle=newTypeArticle;
            imagePathArticle=newImagePathArticle;
            prixArticle=newPrixArticle;
            prixTotal=newPrixTotal;
            quantite=newQuantite;
            dateComm=newDateComm;
            
            newIdFacture = rs.getString(1);
            newNomArticle = rs.getString(2);
            newTypeArticle = rs.getString(3);
            newImagePathArticle = rs.getString(4);
            newPrixArticle = rs.getString(5);
            newPrixTotal = rs.getString(6);
            newQuantite = rs.getString(7);
            //newDateComm = rs.getString(8);
            idA = rs.getString(8);
            System.out.println(" "+newIdFacture+" "+newNomArticle+" "+newTypeArticle+"  "+newImagePathArticle+" "+newPrixArticle+" "+newQuantite+" "+idA );
            
            if(premier){
                idFacture=newIdFacture;
                nomArticle=newNomArticle;
                typeArticle=newTypeArticle;
                imagePathArticle=newImagePathArticle;
                prixArticle=newPrixArticle;
                prixTotal=newPrixTotal;
                quantite=newQuantite;
                dateComm=newDateComm;
                premier=false;
            }
            fact = new JSONObject();
            fact.put("nomArticle", newNomArticle);
            fact.put("typeArticle", newTypeArticle);
            fact.put("imagePathArticle", newImagePathArticle);    
            fact.put("quantite", newQuantite);
            fact.put("prixArticle", newPrixArticle);
            if(!idFacture.equals(newIdFacture)){
                obj = new JSONObject();
                obj.put("idFacture",  idFacture);
                obj.put("prixTotalFacture", prixTotal);
                //obj.put("dateCommande", idArticle);
                obj.put("Factures",facts);
                listFact.add(obj);
                facts = new JSONArray();
            }else{
                facts.add(fact);
            }
            i++;
        }
        JSONObject  newFact = new JSONObject(); 
        newFact.put("nomArticle", nomArticle);
        newFact.put("typeArticle", typeArticle);
        newFact.put("imagePathArticle", imagePathArticle);    
        newFact.put("quantite", quantite);
        newFact.put("prixArticle", prixArticle);
        facts.add(newFact);
        obj = new JSONObject();
        obj.put("idFacture",  idFacture);
        obj.put("prixTotalFacture", prixTotal);
        //obj.put("dateCommande", idArticle);
        obj.put("Factures",facts);
        listFact.add(obj);
        
        x.disconnect();
        
        return listFact;
    }
    
    /**
     * 
     * @param listIdFacture correspond à la liste des identifiants des factures que l'on souhaite ajouter dans un fichier "./Facture.xml"
     * @param listIdCommande correspond à la liste des identifiants des commandes que l'on souhaite commander
     * @param listPrix  correspond à la liste des prix respectivement par rapport aux identifiants Facture et identifiant Commande
     */
    public void factureToXML(ArrayList<String> listIdFacture,ArrayList<String>  listIdCommande,ArrayList<String>  listPrix) {
        Element e = null;
        String nomFichier = "./Facture.xml";
        
        Document document = null;
        DocumentBuilderFactory fabrique = null;
        
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            document = docBuilder.newDocument();
            
            Element rootElement = document.createElement("factures");
            
            document.appendChild(rootElement);
            for (int i = 0; i < listIdFacture.size(); i++) {
                Element facture = document.createElement("facture");


                Element idFacture = document.createElement("idFacture");
                idFacture.appendChild(document.createTextNode(""+listIdFacture.get(i)));
                facture.appendChild(idFacture);
                
                
                Element idCommandeF = document.createElement("idCommande");
                idCommandeF.appendChild(document.createTextNode(""+listIdCommande.get(i)));
                facture.appendChild(idCommandeF);

                Element prixF = document.createElement("prix");
                prixF.appendChild(document.createTextNode(""+listPrix.get(i)));
                facture.appendChild(prixF);
                
                rootElement.appendChild(facture);
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(new File(nomFichier));
            transformer.transform(source, result);
            

        }catch (Exception a){
            System.out.println(a);        
        }
    }
    
    
    /**
     * 
     * @return un tableau de type Plat, recupération de notre variable globale plats. 
     */
    public ArrayList<Plat> getPlats() {
        return plats;
    }
    
    /**
     * 
     * @param plats permet d'affecter un tableau à notre variable globale plats.
     */
    public void setPlats(ArrayList<Plat> plats) {
        this.plats = plats;
    } 
}


