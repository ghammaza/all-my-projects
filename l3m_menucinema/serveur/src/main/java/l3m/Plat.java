/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l3m;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author orandr
 */
public class Plat {
   private String id;
   private String nom;
   private String type;
   private double prix;
   private String imagePath;
   private ArrayList<String> ingredients;

   
   public Plat(){
       id="";
       nom="";
       type="";
       prix=0.0;
       imagePath="";
       this.ingredients = new ArrayList<String>();
   }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public ArrayList<String> getIngredients() {
        return ingredients;
    }

    
    
    public void setIngredients(ArrayList<String> ingredients) {
        this.ingredients = ingredients;
    }

   
}