package l3m;

import java.util.ArrayList;
import javax.servlet.http.HttpServlet;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.util.thread.QueuedThreadPool;

public class PizzaServer extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private Server server;

  
    void start() throws Exception {
        int maxThreads = 100;
        int minThreads = 10;
        int idleTimeout = 120;

        QueuedThreadPool threadPool = new QueuedThreadPool(maxThreads, minThreads, idleTimeout);

        server = new Server(threadPool);
        ServerConnector connector = new ServerConnector(server);
        connector.setPort(8090);
        
        server.setConnectors(new Connector[]{connector});

        ServletHandler servletHandler = new ServletHandler();
        server.setHandler(servletHandler);        
        
        servletHandler.addServletWithMapping(PizzaServlet.class, "/api/plats");
        servletHandler.addServletWithMapping(ClientServlet.class, "/api/client");
        servletHandler.addServletWithMapping(BlockingServlet.class, "/status");
        servletHandler.addServletWithMapping(ClientAuthentificationServlet.class, "/api/authentification");

        server.start();
    }

    void stop() throws Exception {
        System.out.println("Server stop");
        server.stop();
    }

    public static void main(String[] args) throws Exception {

            

        PizzaServer server = new PizzaServer();
        server.start();
        
       
        
        
        //server.stop();

    }

    
    /*
    

        Plats p = new Plats();

        p.toXML();
        Plat Savoyarde = new Plat();
        Savoyarde.setId("1");
        Savoyarde.setNom("Savoyarde");
        Savoyarde.setType("Plat");
        Savoyarde.setPrix(20.0);
        Savoyarde.setImagePath("https://static.cuisineaz.com/240x192/i92032-pizza-savoyarde.jpg");

        ArrayList<String> ingredientsSavoyarde = new ArrayList<String>();
        ingredientsSavoyarde.add("Lardon");
        ingredientsSavoyarde.add("CremeFraiche");
        ingredientsSavoyarde.add("PommeDeTerre");
        ingredientsSavoyarde.add("Oignon");
        ingredientsSavoyarde.add("Oeuf");

        Savoyarde.setIngredients(ingredientsSavoyarde);

        p.ajouterPlat(Savoyarde);

        Plat Mexicaine = new Plat();
        Mexicaine.setId("2");
        Mexicaine.setNom("Mexicaine");
        Mexicaine.setType("Plat");
        Mexicaine.setPrix(16.0);
        Mexicaine.setImagePath("https://assets.afcdn.com/recipe/20151003/6002_w600.jpg");

        ArrayList<String> ingredientsMexicaine = new ArrayList<String>();
        ingredientsMexicaine.add("Poivron");
        ingredientsMexicaine.add("Merguez");
        ingredientsMexicaine.add("ViandeHachee");
        ingredientsMexicaine.add("Jambon");
        ingredientsMexicaine.add("Oignon");
        ingredientsMexicaine.add("Olives");

        Mexicaine.setIngredients(ingredientsMexicaine);

        p.ajouterPlat(Mexicaine);

    */
}
