/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l3m;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author orandr
 */
public class GestionnaireCommande extends SQLAble {

    private Commande commande;

    public GestionnaireCommande() throws SQLException {
        connectToDatabase();
        commande = new Commande();
        //disconnect();
    }

    public GestionnaireCommande(String idClient, List<String> idPlats, List<String> idFilms, String adresseLivraison) throws SQLException {
        connectToDatabase();
        commande = new Commande();
        commande.setIdClient(idClient);
        commande.setIdPlats(idPlats);
        commande.setIdFilms(idFilms);
        commande.setAdresseLivraison(adresseLivraison);
        //commande.setDate(-----);
        //commande.setId(-----);
        //commande.setPrix(-----);
        double prix;
        String date;
        date = "";
        prix = 0.0;
        prix += getPrixListArticle(idPlats);
        prix += getPrixListArticle(idFilms);

        //System.out.println("prix :" + prix);
        commande.setPrix(prix);

        date = systemeDatejjmmannee();
        commande.setDate(date);

        //disconnect();
    }

    public GestionnaireCommande(String id) throws SQLException {
        connectToDatabase();
        commande = new Commande();
        commande.setId(id);

        Commande newCommande = new Commande();
        newCommande = getCommande(id);
        commande.setAdresseLivraison(newCommande.getAdresseLivraison());
        commande.setDate(newCommande.getDate());
        commande.setIdClient(newCommande.getIdClient());
        commande.setIdFilms(newCommande.getIdFilms());
        commande.setIdPlats(newCommande.getIdPlats());
        commande.setPrix(newCommande.getPrix());
        //afficherCommande(commande);
        disconnect();
    }

     // reliée angular
    public JSONArray suggestionPizzaToJSON(String idFilm) throws SQLException{
        
        GestionnaireMenu gm = new GestionnaireMenu();
        JSONArray list = new JSONArray();
        
        
        ArrayList<String> listIdPizza = new ArrayList<String>();
        listIdPizza =  suggestionPizza(idFilm);
        String nom="";
        String type="";
        String prix="";
        String imagePath="";
        SQLAble x = new SQLAble();
        JSONObject article = new JSONObject();
        JSONObject  obj = new JSONObject(); 
        x.connectToDatabase();
        for (int i = 0; i < listIdPizza.size(); i++) {
            idFilm = listIdPizza.get(i);
            Plat currentPlat = gm.getMenuDB(idFilm);
            ResultSet rs;
            String rq="SELECT nom,type,prix,imagePath FROM Article where idArticle="+listIdPizza.get(i);
            //System.out.println(rq);
            rs = x.request(rq);
            while (rs.next()) {
                nom = rs.getString(1);
                type = rs.getString(2);
                prix = rs.getString(3);
                imagePath =rs.getString(4);
                article = new JSONObject();
                obj = new JSONObject();  
                obj.put("nom", nom);
                obj.put("type", type);
                obj.put("prix", prix);    
                obj.put("imagePath", imagePath);   
                article.put("plats", obj);
                article.put("id",  listIdPizza.get(i));
                list.add(article);
            }
        }    
        x.disconnect();
        return list;
    }
    
    
    public void supprimerArticleByIdPersonne(String nomArticle, String idPersonne) throws SQLException{
        SQLAble x = new SQLAble();
            x.connectToDatabase();
            
            int idArticle = getArticleIdByName(nomArticle);
            ResultSet rs;
            String rq="DELETE FROM COMMANDE WHERE idArticle = "+idArticle+" and idpersonne = '"+idPersonne+"'";
            System.out.println(rq);
 
            x.requestUpdate(rq);
        
    }
    // reliée angular
    public JSONArray suggestionFilmToJSON(String idPizza) throws SQLException{
        
        GestionnaireMenu gm = new GestionnaireMenu();
        JSONArray list = new JSONArray();
        
        
        ArrayList<String> listIdFilm = new ArrayList<String>();
        listIdFilm = suggestionFilm(idPizza);
        String nom="";
        String type="";
        String prix="";
        String imagePath="";
        SQLAble x = new SQLAble();
        JSONObject article = new JSONObject();
        JSONObject  obj = new JSONObject(); 
        ResultSet rs;
        x.connectToDatabase();
        for (int i = 0; i < listIdFilm.size(); i++) {
            idPizza = listIdFilm.get(i);
            Plat currentPlat = gm.getMenuDB(idPizza);
            String rq="SELECT nom,type,prix,imagePath FROM Article where idArticle ="+listIdFilm.get(i);
            //System.out.println(rq);
            rs = x.request(rq);
            while (rs.next()) {
                nom = rs.getString(1);
                type = rs.getString(2);
                prix = rs.getString(3);
                imagePath = rs.getString(4);
                article = new JSONObject();
                obj = new JSONObject();  
                obj.put("nom", nom);
                obj.put("type", type);
                obj.put("prix", prix);   
                obj.put("imagePath", imagePath);    
                article.put("films", obj);
                article.put("id",  listIdFilm.get(i));
                list.add(article);
            }
        }    
        x.disconnect();
        return list;
    }
    
    public boolean existArticle(String nom) throws SQLException{
            SQLAble x = new SQLAble();
            x.connectToDatabase();
            ResultSet rs;
            String rq="SELECT * FROM article where nom = '"+nom+"'";
            //System.out.println(rq);
            int compteur = 0;
            rs = x.request(rq);
            while (rs.next()) {
                compteur++;
            }
            x.disconnect();
            return compteur != 0;
    }
    
    public boolean panierVide(String idPersonne) throws SQLException{

        SQLAble x = new SQLAble();
        x.connectToDatabase();
        ResultSet rs;         
        String rq="SELECT * FROM commande where idPersonne = '"+idPersonne+"' and pannier = 'oui'";
        System.out.println(rq);
        int compteur = 0;
        rs = x.request(rq);
        while (rs.next()) {
           compteur++;
        }
        x.disconnect();
        
        return compteur == 0;
    }
    
    public boolean articleExistInHisCommande(String idPersonne, String articleName) throws SQLException{
            SQLAble x = new SQLAble();
            x.connectToDatabase();
            ResultSet rs;
            int idArticle = getArticleIdByName(articleName);            
            String rq="SELECT * FROM commande where idPersonne = '"+idPersonne+"' and idArticle = "+idArticle;
            System.out.println(rq);
            int compteur = 0;
            rs = x.request(rq);
            while (rs.next()) {
                compteur++;
            }
            x.disconnect();
            return compteur != 0;
    }
    /*
    idArticle int NOT NULL,
	nom varchar2(50),
	description varchar2(500),
	type varchar2(20),
	prix float(20),	
	imagePath varchar2(150),
	PRIMARY KEY (idArticle)
    */
    
    public int getArticleIdByName(String name) throws SQLException{
        int r = 0;
        SQLAble x = new SQLAble();
        x.connectToDatabase();
        ResultSet rs;        
        rs = x.request("SELECT idArticle FROM Article WHERE nom='" +name+"'");
        System.out.println(rs); 
        while (rs.next()) {            
            r = Integer.parseInt(rs.getString(1));
        }
        
        
        return r;
    }
    public void ajouterArticle(int idArticle, String nom, String description, String type, String prix, String imagePath) throws SQLException{
       
        if(!existArticle(nom)){
        SQLAble x = new SQLAble();
          x.connectToDatabase();

            String rq="INSERT INTO Article(nom, description, type, prix, imagePath) VALUES ('"+nom+"', 'blabla','Film',10,'/bk8LyaMqUtaQ9hUShuvFznQYQKR.jpg')";                      
        
        
            x.requestUpdate(rq);
            
            System.out.println("l'article ajouté dans la base de données");
        }else{
            System.out.println("l'article existe dans la base de données");
        }

    }
    
    public ArrayList<String> suggestionFilm(String idPizza) throws SQLException {
        
        ArrayList<String> listIdFilm = new ArrayList<String>();
        ArrayList<String> listIdFilmDate = new ArrayList<String>();
        HashMap<String, Integer> hmap = new HashMap<String, Integer>();
        String idFilmDate = "";
        SQLAble x = new SQLAble();
        x.connectToDatabase();
        ResultSet rs;
        String dateCommande;
        rs = x.request("SELECT to_char(dateCommande,'DD/MM/YYYY HH24:MI:SS')  AS dateCommande FROM Commande WHERE idArticle=" + idPizza);
        while (rs.next()) {
       
            dateCommande = rs.getString(1);
            //System.out.println("date ->"+dateCommande);
            idFilmDate = recupFilmViaDate(dateCommande);
            if(hmap.containsKey(idFilmDate)){
                hmap.replace(idFilmDate, hmap.get(idFilmDate), hmap.get(idFilmDate)+1);
            }else{
                hmap.put(idFilmDate,1);
            }
        }
        x.disconnect();
        
        x.connectToDatabase();
        
        
        // Ajout des entrées de la map à une liste
        final List<Map.Entry<String, Integer>> entries = new ArrayList<Map.Entry<String, Integer>>(hmap.entrySet());

        // Tri de la liste sur la valeur de l'entrée
        Collections.sort(entries, new Comparator<Map.Entry<String, Integer>>() {
          public int compare(final Map.Entry<String, Integer> e1, final Map.Entry<String, Integer> e2) {
            return e1.getValue().compareTo(e2.getValue());
          }
        });
        
        Map.Entry<String, Integer> e;
        for (int  i = 0; i  < entries.size(); i  ++) {
            e=entries.get(i);
            //System.out.println("-> "+e.getKey()+" _ "+e.getValue());
            if(!(i<entries.size()-5)){
                //System.out.println("cle :"+e.getKey()+" valeur :"+e.getValue());
                listIdFilm.add(e.getKey());
            }
        }
        
        
        int nbMaxArticle=0;
        rs = x.request("SELECT MAX(idArticle) FROM Article");
        while (rs.next()) {
            nbMaxArticle = Integer.parseInt(rs.getString(1));
        }
        
        int nbMaxArticlePizza=0;
        rs = x.request("SELECT MAX(idArticle) FROM Article where type='PIZZA'");
        while (rs.next()) {
            nbMaxArticlePizza = Integer.parseInt(rs.getString(1));
        }
        int idF=0;
        for (int i = 0; i < listIdFilm.size(); i++) {
            if(listIdFilm.get(i).equals("")){
                listIdFilm.remove(i);
            }
        }
        
        while(listIdFilm.size()<5){
            idF = nombreAleatoire(nbMaxArticlePizza,nbMaxArticle );
            if(!listIdFilm.contains(idF)){
                listIdFilm.add(""+idF);
            }
        }
        x.disconnect();
        
        return listIdFilm;
    }
    
    
    public ArrayList<String> suggestionPizza(String idFilm) throws SQLException {
        
        ArrayList<String> listIdPizza = new ArrayList<String>();
        HashMap<String, Integer> hmap = new HashMap<String, Integer>();
        String idPizzaDate = "";
        SQLAble x = new SQLAble();
        x.connectToDatabase();
        ResultSet rs;
        String dateCommande;
        rs = x.request("SELECT to_char(dateCommande,'DD/MM/YYYY HH24:MI:SS')  AS dateCommande FROM Commande WHERE idArticle=" + idFilm);
        while (rs.next()) {
            dateCommande = rs.getString(1);
            //System.out.println("date ->"+dateCommande);
            listIdPizza = recupPizzaViaDate(dateCommande);
            for (int i = 0; i < listIdPizza.size(); i++) {
                if(hmap.containsKey(listIdPizza.get(i))){
                    hmap.replace(idPizzaDate, hmap.get(listIdPizza.get(i)), hmap.get(listIdPizza.get(i))+1);
                }else{
                    hmap.put(listIdPizza.get(i),1);
                }
            }
        }
        listIdPizza = new ArrayList<String>();
        x.disconnect();
        
        x.connectToDatabase();
        
        
        // Ajout des entrées de la map à une liste
        final List<Map.Entry<String, Integer>> entries = new ArrayList<Map.Entry<String, Integer>>(hmap.entrySet());

        // Tri de la liste sur la valeur de l'entrée
        Collections.sort(entries, new Comparator<Map.Entry<String, Integer>>() {
          public int compare(final Map.Entry<String, Integer> e1, final Map.Entry<String, Integer> e2) {
            return e1.getValue().compareTo(e2.getValue());
          }
        });
        
        Map.Entry<String, Integer> e;
        for (int  i = 0; i  < entries.size(); i  ++) {
            e=entries.get(i);
            //System.out.println("-> "+e.getKey()+" _ "+e.getValue());
            if(!(i<entries.size()-5)){
                //System.out.println("cle :"+e.getKey()+" valeur :"+e.getValue());
                listIdPizza.add(e.getKey());
            }
        }
        
        int nbMaxArticlePizza=0;
        rs = x.request("SELECT MAX(idArticle) FROM Article where type='PIZZA'");
        while (rs.next()) {
            nbMaxArticlePizza = Integer.parseInt(rs.getString(1));
        }
        int idF=0;
        while(listIdPizza.size()<5){
            idF = nombreAleatoire(1,nbMaxArticlePizza );
            if(!listIdPizza.contains(idF)){
                listIdPizza.add(""+idF);
            }
        }
        x.disconnect();
        return listIdPizza;
    }
    

    public String recupFilmViaDate(String dateCommande) throws SQLException {
        String idFilm="";
//        Select * from Commande NATURAL JOIN ARticle Where type='FILM';
//        SELECT idArticle FROM Commande NATURAL JOIN Article WHERE type='FILM' 
//        and dateCommande=TO_DATE('09/05/2019 10:55:2', 'DD/MM/YYYY HH24:MI:SS');
        
        SQLAble x = new SQLAble();
        x.connectToDatabase();
        ResultSet rs;
        String rq="SELECT idArticle FROM Commande NATURAL JOIN Article WHERE type='FILM' and dateCommande=TO_DATE('"+dateCommande+"', 'DD/MM/YYYY HH24:MI:SS') ";
        //System.out.println(rq);
        rs = x.request(rq);
        while (rs.next()) {
            idFilm = rs.getString(1);
        }
        x.disconnect();
        return idFilm;
    }
    
    public ArrayList<String>  recupPizzaViaDate(String dateCommande) throws SQLException {
        ArrayList<String> listIdPizza= new ArrayList<String>();
//        Select * from Commande NATURAL JOIN ARticle Where type='FILM';
//        SELECT idArticle FROM Commande NATURAL JOIN Article WHERE type='FILM' 
//        and dateCommande=TO_DATE('09/05/2019 10:55:2', 'DD/MM/YYYY HH24:MI:SS');
        
        SQLAble x = new SQLAble();
        x.connectToDatabase();
        ResultSet rs;
        String rq="SELECT idArticle FROM Commande NATURAL JOIN Article WHERE type='PIZZA' and dateCommande=TO_DATE('"+dateCommande+"', 'DD/MM/YYYY HH24:MI:SS') ";
        System.out.println(rq);
        rs = x.request(rq);
        while (rs.next()) {
            listIdPizza.add(rs.getString(1));
        }
        x.disconnect();
        return listIdPizza;
    }

    public void genererInsertCommande(int nbInsert,int nbMaxQuantite) throws SQLException {

        SQLAble x = new SQLAble();
        x.connectToDatabase();
        String rq;
        rq="";
        String idPersonne;
        idPersonne = "";
        String dateCommande;
        dateCommande="";
        String idArticle;
        idArticle = "";
        String quantite;
        quantite = "";
        String pannier;
        pannier = "";
        int idArticleFilm;
        idArticleFilm = 0;
        
        
        ResultSet rs;
        int nbMaxPersonne=0;
        rs = x.request("SELECT MAX(idPersonne) FROM Personne");
        while (rs.next()) {
            nbMaxPersonne = Integer.parseInt(rs.getString(1));
        }
        int nbMaxArticlePizza=0;
        rs = x.request("SELECT MAX(idArticle) FROM Article where type='PIZZA'");
        while (rs.next()) {
            nbMaxArticlePizza = Integer.parseInt(rs.getString(1));
        }
        
        int nbMaxArticle=0;
        rs = x.request("SELECT MAX(idArticle) FROM Article");
        while (rs.next()) {
            nbMaxArticle = Integer.parseInt(rs.getString(1));
        }
        
        
//  INSERT INTO Commande(idPersonne,dateCommande,idArticle,quantite,pannier) VALUES(4,TO_DATE('09/04/2019 09:40:16', 'DD/MM/YYYY HH24:MI:SS'),10,1,'non');
        for (int i = 0; i < nbInsert; i++) {
            String id;
            id = "";
            idPersonne   = ""+nombreAleatoire(1,nbMaxPersonne);
            dateCommande = ""+genererDateAleatoire();
            int nbArtic  = nombreAleatoire(1, 7);
            if(nombreAleatoire(0,100)<50){
                pannier="oui";
            }else{
                pannier="non";
            }
            idArticleFilm = nombreAleatoire(nbMaxArticlePizza+1,nbMaxArticle);
            rq="INSERT INTO Commande(idPersonne,dateCommande,idArticle,quantite,pannier)"
            + " VALUES('"+idPersonne+"',TO_DATE('"+dateCommande+"', 'DD/MM/YYYY HH24:MI:SS'),"+idArticleFilm+",1,'"+pannier+"')";          
            x.requestUpdate(rq);
            for (int j = 0; j < nbArtic; j++) {
                idArticle    = ""+nombreAleatoire(1,nbMaxArticlePizza);
                quantite     = ""+nombreAleatoire(1,nbMaxQuantite);
                rq="INSERT INTO Commande(idPersonne,dateCommande,idArticle,quantite,pannier)"
                 + " VALUES('"+idPersonne+"',TO_DATE('"+dateCommande+"', 'DD/MM/YYYY HH24:MI:SS'),"+idArticle+","+quantite+",'"+pannier+"')";
                x.requestUpdate(rq);
            }
            System.out.println("--------");
            System.out.println("");
        }

        x.disconnect();
    }
    
    
    
//  INSERT INTO Facture(idCommande,prix) VALUES(4,'13.0');
    public String genereFactureAleatoire(int nbFact) throws SQLException{
        String fact;
        fact="";
        int idCommande;
        int pprix;
        String rq;
        SQLAble x = new SQLAble();
        x.connectToDatabase();
        ResultSet rs;
        String nbMaxCommande="";
        rs = x.request("SELECT MAX(idCommande) FROM Commande");
        while (rs.next()) {
            nbMaxCommande = rs.getString(1);
        }
        
        
        for (int i = 0; i < nbFact; i++) {
            idCommande = nombreAleatoire(1,Integer.parseInt(nbMaxCommande));
            pprix      = nombreAleatoire(1,100);
            rq  ="INSERT INTO Facture(idCommande,prix) VALUES("+idCommande+",'"+pprix+".0')";
            //System.out.println("genereFacture "+i+" :"+rq);
            x.requestUpdate(rq);
        }
        x.disconnect();
        return fact;
    }
    
    public String genererDateAleatoire(){
        //'09/04/2019 09:40:16'
        String date;
        date="";
        int nbJours=nombreAleatoire(0, 30);
        if(nbJours<10){
            date+="0"+nombreAleatoire(1,9 );
        }else{
            date+=nbJours;
        }
        date+="/";
        int nbMois=nombreAleatoire(1, 12);
        if(nbMois<10){
            date+="0"+nbMois;
        }else{
            date+=nbMois;
        }
        date+="/";
        date+="201"+nombreAleatoire(7, 9);;
        date+=" ";
        int nbh=nombreAleatoire(0, 23);
        if(nbh<10){
            date+="0"+nbh;
        }else{
            date+=nbh;
        }
        date+=":";
        int nbm=nombreAleatoire(1, 59);
        if(nbm<10){
            date+="0"+nbm;
        }else{
            date+=nbm;
        }
        date+=":";
        int nbs=nombreAleatoire(1, 59);
        if(nbs<10){
            date+="0"+nbs;
        }else{
            date+=nbs;
        }
        return date;
    }

    public String retournerPhrase(int taille) {
        String phrase;
        phrase = "";
        int nbAleat;
        for (int i = 0; i < taille; i++) {
            nbAleat = nombreAleatoire(0, 100);
            if (nbAleat < 50) {
                int n = nombreAleatoire(0, 26);
                phrase += (char) ('a' + n);
            } else {
                int n = nombreAleatoire(0, 26);
                phrase += (char) ('A' + n);
            }
        }
        return phrase;
    }

    public int nombreAleatoire(int min, int max) {
        return min + (int) Math.floor((max - min + 1) * Math.random());
    }

    public void afficherCommandeReference(Commande c) {
        String phraseCommande;
        phraseCommande = "";
        phraseCommande += "Date :" + c.getDate() + " \n";
        phraseCommande += "Adresse :" + c.getAdresseLivraison() + " \n";
        phraseCommande += "Id Client :" + c.getIdClient() + " \n";
        phraseCommande += "Prix :" + c.getPrix() + " \n";
        phraseCommande += "idFilm :" + c.getAfficherListFilm() + " \n";
        phraseCommande += "idPizza :" + c.getAfficherListPlats() + " \n";
        System.out.println("Commande :\n" + phraseCommande);
    }

    public void afficherCommande() {
        String phraseCommande;
        phraseCommande = "";
        phraseCommande += "Date :" + commande.getDate() + " \n";
        phraseCommande += "Adresse :" + commande.getAdresseLivraison() + " \n";
        phraseCommande += "Id Client :" + commande.getIdClient() + " \n";
        phraseCommande += "Prix :" + commande.getPrix() + " \n";
        phraseCommande += "idFilm :" + commande.getAfficherListFilm() + " \n";
        phraseCommande += "idPizza :" + commande.getAfficherListPlats() + " \n";
        System.out.println("Commande :\n" + phraseCommande);
    }

    
    public void updateQuantite(String idPersonne,String idArticle,String Quantite) throws SQLException{
    
        SQLAble x = new SQLAble();
        x.connectToDatabase();
        ResultSet rs;
        
        String idCommande="";
        String rqId="SELECT idCommande FROM Commande WHERE idPersonne= '" +idPersonne+"' and idArticle="+idArticle;
        System.out.println(rqId);
        rs = x.request(rqId);
        while (rs.next()) {
            idCommande = rs.getString(1);
        }
        
        String rq="UPDATE Commande SET Quantite="+Quantite+" WHERE idCommande="+idCommande;
        System.out.println(rq);
        x.requestUpdate(rq);
        System.out.println("Mise a jour de la quantite bien enregistre...");
        x.disconnect();
    }
    
    public void enregistrerCommandeDBListArticle(String idPersonne,ArrayList<String> listIdArticle) throws SQLException{
    
        SQLAble x = new SQLAble();
        x.connectToDatabase();
        ResultSet rs;
         String date = systemeDatejjmmannee();
         String rq="";
        for (int i = 0; i < listIdArticle.size(); i++) {
            rq="INSERT INTO Commande(idPersonne,dateCommande,idArticle,quantite,pannier)"
             + " VALUES('"+idPersonne+"',TO_DATE('"+date+"', 'DD/MM/YYYY HH24:MI:SS'),"+listIdArticle.get(i)+",1,'oui')";

            System.out.println(rq);
            x.requestUpdate(rq);
        }
        System.out.println("Commande bien enregistre...");
        x.disconnect();
    }

    public int getIdCommandeByIdPersonne(String idPersonne) throws SQLException{        
        
        SQLAble x = new SQLAble();
        x.connectToDatabase();
        ResultSet rs;
        
        String idCommande="";
        String rqId="SELECT idCommande FROM Commande WHERE idPersonne= '" +idPersonne+"' and pannier = 'oui'";

        rs = x.request(rqId);
        while (rs.next()) {
            idCommande = rs.getString(1);
        }
        
        return Integer.parseInt(idCommande);
    }
    
    public boolean existIdCommandeByIdPersonne(String idPersonne) throws SQLException{
        
        
        SQLAble x = new SQLAble();
        x.connectToDatabase();
        ResultSet rs;
        
        int cont = 0;
        
        String rqId="SELECT idCommande FROM Commande WHERE idPersonne= '" +idPersonne+"' and pannier = 'oui' ";

        rs = x.request(rqId);
        while (rs.next()) {
            cont++;
        }
        
        return cont != 0;
    }
    
    public int getNewIdCommandeByIdUser(String idPersonne) throws SQLException{
        String nbMaxCommande = "";

        SQLAble x = new SQLAble();
        x.connectToDatabase();
        ResultSet rs;
       
        rs = x.request("SELECT MAX(idCommande) as max FROM Commande");
        while (rs.next()) {
            nbMaxCommande = rs.getString(1);
        }
        return Integer.parseInt(nbMaxCommande);   
    }
    public void enregistrerCommandeDB(String idPersonne,String idArticle) throws SQLException {
        
        
        SQLAble x = new SQLAble();
        x.connectToDatabase();
        ResultSet rs;
        int idCommande = 0;
        if(existIdCommandeByIdPersonne(idPersonne)){
            idCommande = getIdCommandeByIdPersonne(idPersonne);
        } else {            
             idCommande = getNewIdCommandeByIdUser(idPersonne);
        }

        String date = systemeDatejjmmannee();
        String rq="INSERT INTO Commande(idCommande, idPersonne,dateCommande,idArticle,quantite,pannier)"
         + " VALUES("+idCommande+", '"+idPersonne+"',TO_DATE('"+date+"', 'DD/MM/YYYY HH24:MI:SS'),"+idArticle+",1,'oui')";
        System.out.println(rq);
        x.requestUpdate(rq);
        System.out.println("Commande bien enregistre...");
        x.disconnect();

    }
    
      
    public int getQuantiteByIdCommande(int idCommande) throws SQLException{
        int quantite = 0;
        
        XMLAble x = new XMLAble();
        x.connectToDatabase();
        ResultSet rs;

        rs = x.request("SELECT QUANTITE FROM commande WHERE IDCOMMANDE=" +idCommande);
        while (rs.next()) {               
            quantite = Integer.parseInt(rs.getString(1));            
        }
        return quantite;
    }
    
    public ArrayList<String> getArticleInfos(String id) throws SQLException{
        XMLAble x = new XMLAble();
        x.connectToDatabase();
        ResultSet rs;
        ArrayList<String> article = new ArrayList<String>();

        rs = x.request("SELECT nom, description, type, prix, imagePath FROM article WHERE idArticle=" +Integer.parseInt(id));
        
        while (rs.next()) { 
            article.add(rs.getString(1));
            article.add(rs.getString(2));
            article.add(rs.getString(3));
            article.add(rs.getString(4));
            article.add(rs.getString(5));
        }
        
        return article;
    }
    public JSONObject getPanierByUserID(String id) throws SQLException{
        XMLAble x = new XMLAble();
        x.connectToDatabase();
        ResultSet rs;
        
        JSONArray panier = new JSONArray();
        String rq = "SELECT idCommande, idArticle FROM Commande WHERE pannier = 'oui' and idPersonne= '" +id+"'";
        
        System.out.println(rq);
        rs = x.request(rq);
         JSONObject grupArticle = new JSONObject();
        while (rs.next()) {
            String quantite = rs.getString(1);
 
            ArrayList<String> articleCurrent = getArticleInfos(rs.getString(2));
            
            if(articleCurrent.size() > 0 ){             
                JSONObject article = new JSONObject();
                article.put("id", rs.getString(2));
                article.put("nom", articleCurrent.get(0));
                article.put("description", articleCurrent.get(1));
                article.put("type", articleCurrent.get(2));
                article.put("prix", articleCurrent.get(3));
                article.put("imagePath", articleCurrent.get(4));
                article.put("quantite", quantite);
                
                panier.add(article);
           }
            
        }
        
       grupArticle.put("articles", panier);
        return grupArticle;
    }
    
    public Commande getCommande(String idCommande) throws SQLException {
        SQLAble x = new SQLAble();
        x.connectToDatabase();
        String idPersonne;
        String idArticle;
        String genre;
        String adresse;
        String date;
        double prix;
        Commande newCommande;
        List<String> idPlats;
        List<String> idFilms;
        newCommande = new Commande();
        prix = 0.0;
        genre = "";
        idPersonne = "";
        idArticle = "";
        date = "";

        idPlats = new ArrayList<String>();
        idFilms = new ArrayList<String>();

        ResultSet rs;
        rs = x.request("SELECT idPersonne,idArticle FROM Commande WHERE idCommande=" + Integer.parseInt(idCommande));
        while (rs.next()) {
            idPersonne = rs.getString(1);
            idArticle = rs.getString(2);
            genre = getGenreArticle(idArticle);
            prix += Double.parseDouble(getPrixArticle(idArticle));
            if (genre.equals("FILM")) {
                idFilms.add(idArticle);
            } else {//C'est donc un plat
                idPlats.add(idArticle);
            }
        }

        date = systemeDatejjmmannee();
        adresse = getAdressePersonne(idPersonne);

        newCommande.setDate(date);
        newCommande.setAdresseLivraison(adresse);
        newCommande.setIdClient(idPersonne);
        newCommande.setIdFilms(idFilms);
        newCommande.setIdPlats(idPlats);
        newCommande.setPrix(prix);
//        System.out.println("date :" + date);
//        System.out.println("adresse :" + adresse);
//        System.out.println("idPersonne :" + idPersonne);
//        System.out.println("prix :" + prix);
//        System.out.println("film size :" + idFilms.size());
//        System.out.println("plats size :" + idPlats.size());
        //afficherCommande(newCommande);
        x.disconnect();
        return newCommande;
    }

    //type ='PIZZA'  ou 'FILM'
    public ArrayList<String> getList(String type) throws SQLException {
        SQLAble x = new SQLAble();
        x.connectToDatabase();
        ArrayList<String> films = new ArrayList<>();
        ArrayList<String> pizzas = new ArrayList<>();
        ArrayList<String> list = new ArrayList<>();

        ResultSet rs;
        if (type.equals("FILM")) {
            rs = x.request("SELECT nom FROM Article WHERE type='FILM' ");
            while (rs.next()) {
                films.add(rs.getString(1));
                list.add(rs.getString(1));
            }
            //list = films; 
        } else {
            rs = x.request("SELECT nom FROM Article WHERE type='PIZZA' ");
            while (rs.next()) {
                pizzas.add(rs.getString(1));
                list.add(rs.getString(1));
            }
            //list = pizzas;
        }
        //System.out.println("list size :" + list.size());
        x.disconnect();
        return list;
    }

    public void afficherList(ArrayList<String> list) {
        for (int i = 0; i < list.size(); i++) {
            System.out.println(i + ": " + list.get(i));
        }
    }
    

    public String getIdPersonne(String nom, String prenom, String numeroTel) throws SQLException {
        SQLAble x = new SQLAble();
        x.connectToDatabase();
        String id;
        ResultSet rs;
        id = "";
        rs = x.request("SELECT idPersonne FROM Personne WHERE nom='" + nom + "' and prenom='" + prenom + "' and tel='" + numeroTel + "' ");
        while (rs.next()) {
            id = rs.getString(1);
        }
        x.disconnect();
        return id;
    }

    public double getPrixListArticle(List<String> idArticle) throws SQLException {
        SQLAble x = new SQLAble();
        x.connectToDatabase();
        double prix;
        ResultSet rs;
        prix = 0.0;
        for (int i = 0; i < idArticle.size(); i++) {
            rs = x.request("SELECT prix FROM Article WHERE idArticle=" + Integer.parseInt(idArticle.get(i)));
            while (rs.next()) {
                prix += Double.parseDouble(rs.getString(1));
            }
        }
        x.disconnect();
        return prix;
    }

    public String systemeDatejjmmannee() {

        Date d = new Date();//  09/04/2019 09:40:1
        int jour = d.getDate();
        String j = "" + jour;
        int mois = d.getMonth()+1;
        String m = "" + mois;
        int annee = d.getYear() + 1900;
        if (jour < 10) {
            j = "0" + jour;
        }
        if (mois < 10) {
            m = "0" + mois;
        }

        int heures = d.getHours();
        int minutes = d.getMinutes();
        int secondes = d.getSeconds();
        String h=""+heures;
        String min=""+minutes;
        String s=""+secondes;
        if(heures<10){
            h="0"+heures;
        }
        if(minutes<10){
            min="0"+minutes;
        }
        if(secondes<10){
            s="0"+secondes;
        }
        
        String ddate = j + "/" + m + "/" + annee + " " + h + ":" + min + ":" + s;
        return ddate;
    }

    public String getAdressePersonne(String idPersonne) throws SQLException {
        SQLAble x = new SQLAble();
        x.connectToDatabase();
        ResultSet rs;
        String adresse;
        adresse = "";
        rs = x.request("SELECT adresse FROM Personne WHERE idPersonne=" + Integer.parseInt(idPersonne));
        while (rs.next()) {
            adresse = rs.getString(1);
        }
        x.disconnect();
        return adresse;
    }

    public String getGenreArticle(String idArticle) throws SQLException {
        SQLAble x = new SQLAble();
        x.connectToDatabase();
        ResultSet rs;
        String type;
        type = "";
        rs = x.request("SELECT type FROM Article WHERE idArticle=" + Integer.parseInt(idArticle));
        while (rs.next()) {
            type = rs.getString(1);
        }
        x.disconnect();
        return type;
    }
    
    public String getPrixArticle(String idArticle) throws SQLException {
        SQLAble x = new SQLAble();
        x.connectToDatabase();
        ResultSet rs;
        String prix;
        prix = "";
        rs = x.request("SELECT prix FROM Article WHERE idArticle=" + Integer.parseInt(idArticle));
        while (rs.next()) {
            prix = rs.getString(1);
        }
        x.disconnect();
        return prix;
    }

    public String getNomArticle(String idArticle) throws SQLException {
        SQLAble x = new SQLAble();
        x.connectToDatabase();
        ResultSet rs;
        String nom;
        nom = "";
        rs = x.request("SELECT nom FROM Article WHERE idArticle=" + Integer.parseInt(idArticle));
        while (rs.next()) {
            nom = rs.getString(1);
        }
        x.disconnect();
        return nom;
    }

}

