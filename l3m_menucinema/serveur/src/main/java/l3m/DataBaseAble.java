/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l3m;

import java.sql.SQLException;

/**
 *
 * @author orandr
 */
public interface DataBaseAble {
    public void connectToDatabase()  throws SQLException ;
    public void disconnect()  throws SQLException ;
    public java.sql.ResultSet request(String request)  throws SQLException ;
}
