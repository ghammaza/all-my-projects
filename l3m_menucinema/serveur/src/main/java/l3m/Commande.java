/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l3m;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author orandr
 */
public class Commande {
    private String date;
    private String id;
    private String idClient;
    private List<String> idPlats;
    private List<String> idFilms;
    private double prix;
    private String adresseLivraison;

    public Commande(){
        date="";
        id="";
        idClient="";
        idPlats= new ArrayList<String>();
        idFilms= new ArrayList<String>();
        prix=0.0;
        adresseLivraison="";
    }
    
    
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    public List<String> getIdPlats() {
        return idPlats;
    }

    public void setIdPlats(List<String> idPlats) {
        for (int i = 0; i < idPlats.size(); i++) {
            if(Integer.parseInt(idPlats.get(i))>20){
                System.out.println("Erreur setIdPlats, l'id n°"+idPlats.get(i)+" n'existe pas !");
            }else{
                this.idPlats.add(idPlats.get(i));
            }
        }
    }

    public List<String> getIdFilms() {
        return idFilms;
    }

    public void setIdFilms(List<String> idFilms) {
        int nbMaxFilm=2000000;
        for (int i = 0; i < idFilms.size(); i++) {
            if(Integer.parseInt(idFilms.get(i))>nbMaxFilm){
                System.out.println("Erreur setIdPlats, l'id n°"+idFilms.get(i)+" n'existe pas !");
            }else{
                this.idFilms.add(idFilms.get(i));
            }
        }
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public String getAdresseLivraison() {
        return adresseLivraison;
    }

    public void setAdresseLivraison(String adresseLivraison) {
        this.adresseLivraison = adresseLivraison;
    }
    
    
    public String getAfficherListFilm(){
        String listF;
        listF = "";
        int i;
        for(i=0;i<idFilms.size()-1;i++){
            listF += idFilms.get(i)+" ,";
        }
        System.out.println("nbFilm :"+i);
        listF += idFilms.get(i);
        return listF;
    }
    
    public String getAfficherListPlats(){
        String listP;
        listP = "";
        int i;
        for(i=0;i<idPlats.size()-1;i++){
            listP += idPlats.get(i)+" ,";
        }
        System.out.println("nbPizza :"+i);
        listP += idPlats.get(i);
        return listP;
    }
    
}
