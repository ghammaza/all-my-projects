/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l3m;

/**
 *
 * @author orandr
 */
public enum Ingredient {
    Ananas("Ananas"),
    Anchois("Anchois"),
    Aubergine("Aubergine"),
    Asperge("Asperge"),
    Avocat("Avocat"),
    Bacon("Bacon"),
    BoudinNoir("BoudinNoir"),
    Basilic("Basilic"),
    Cheddar("Cheddar"),
    Champignon("Champignon"),
    CremeFraiche("CremeFraiche"),
    Crevette("Crevette"),
    Chevre("Chevre"),
    Chorizo("Chorizo"),
    Curry("Curry"),
    Compte("Compte"),
    Citron("Citron"),
    Cornichon("Cornichon"),
    EscalopePoulet("EscalopePoulet"),
    Emmental("Emmental"),
    Fromage("Fromage"),
    FruitDeMer("FruitDeMer"),
    Guacamole("Guacamole"),
    HuileDolive("HuileDolive"),
    Jambon("Jambon"),
    Lardon("Lardon"),
    Mais("Mais"),
    Merguez("Merguez"),
    Miel("Miel"),
    Mozzarella("Mozzarella"),
    Oeuf("Oeuf"),
    Oignon("Oignon"),
    OignonRouge("Oignon Rouge"),
    Olives("Olives"),
    Piments("Piments"),
    PommeDeTerre("Pomme De Terre"),
    Poivron("Poivron"),
    Poivre("Poivre"),
    Poire("Poire"),
    Parmesan("Parmesan"),
    Paprika("Paprika"),
    Roquefort("Roquefort"),
    Raclette("Raclette"),
    SaumonFume("Saumon Fume"),
    Sucre("Sucre"),
    Tomate("Tomate"),
    Thym("Thym"),
    ViandeHachee("Viande hachee");
    
    
     private String nom;
 
    Ingredient(String nom) {
        this.nom = nom;
    }
 
    public String getNom() {
        return nom;
    }
}
