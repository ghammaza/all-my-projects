/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l3m;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author orandr
 */
public class ClientServlet extends HttpServlet  {
    
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    
        GestionnaireClient client;
            try {
                client = new GestionnaireClient();
                StringBuilder sb = new StringBuilder();
                BufferedReader br = request.getReader();
                String str = null;
                while ((str = br.readLine()) != null) {
                    sb.append(str);
                }               

                str = sb.toString();     
                str = str.substring(0, str.length()-1);
                str = str.substring(1, str.length());    
                
                JsonObject jsonObject = new JsonParser().parse(str).getAsJsonObject();
                String action = jsonObject.get("action").toString();
                action = action.replace("\"", "");
                
                if(action.equals("create")){
                    System.out.println("Creation d'une nouvelle personne dans la BDD");
                    String id = jsonObject.get("idPersonne").toString();
                    String nom = jsonObject.get("nom").toString();
                    String prenom = jsonObject.get("prenom").toString();
                    String photo = jsonObject.get("photo").toString();
                    String mail = jsonObject.get("mail").toString();
                    String adresse = jsonObject.get("adresse").toString();
                    String telephone = jsonObject.get("telephone").toString();

                    id = id.replace("\"", "");
                    nom = nom.replace("\"", "");
                    prenom = prenom.replace("\"", "");
                    photo = photo.replace("\"", "");
                    mail = mail.replace("\"", "");
                    adresse = adresse.replace("\"", "");
                    telephone = telephone.replace("\"", "");

                    if(!client.existsClientDB(id)){
                        client.enregistrerClientBD(id,nom, prenom, photo, mail,adresse,telephone);
                        System.out.println("Nouveau client : enregistré ! ");
                        response.getWriter().println("Nouveau client : enregistré ! ");
                    }else {
                        System.out.println("Ancien client : déjà enregistré !");
                        response.getWriter().println("Ancien client : déjà enregistré !");
                    }
                    System.out.println("id de la personne ; "+id);
                    response.getWriter().println("id de la personne ; "+id);
                } else if(action.equals("commander")){
                    GestionnaireCommande commande = new GestionnaireCommande();
                    
                    JsonObject json = new JsonParser().parse(str).getAsJsonObject();
                    str = str.substring(0, str.length()-1);
                    str = str.substring(1, str.length());    
                    
                    System.out.println(json.get("id"));                   
                    
                    System.out.println("commander");
//                    ArrayList<String> list = new ArrayList<String>(); 
//                    JsonObject json = new JsonParser().parse(str).getAsJsonObject();
//                    HashMap<String, Object> yourHashMap = new Gson().fromJson(json.toString(), HashMap.class);
//                    
//                    for (Map.Entry<String,Object> e : yourHashMap.entrySet()){
//                        System.out.println(e.getKey() + " : " + e.getValue());
//}
                    }
                
                
            } catch (SQLException ex) {
                response.getWriter().println("erreur");
                System.out.println("erreur");
            }
    }	
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        String action = request.getParameter("action");
        
        String id = request.getParameter("id");       
        
        GestionnaireClient client = null;
        GestionnaireCommande commande = null;
        
        try {
            client = new GestionnaireClient();
            commande = new GestionnaireCommande();
            
            if(action.equals("getPanier")){         
    
                response.getWriter().print(commande.getPanierByUserID(id)); 
                
            }else if(action.equals("getInfos")){        
                
                response.getWriter().print(client.getClientInfosById(id)); 
                
            }else if(action.equals("modifier")){       
                
                String nom = request.getParameter("nom");
                String prenom = request.getParameter("prenom");
                String photo  = request.getParameter("photo");
                String adresse = request.getParameter("adresse");
                String telephone = request.getParameter("telephone");
                client.setClientBdByUserID(id, nom, prenom, photo, adresse, telephone);
                
            }else if(action.equals("exist")){   
                
                response.getWriter().println(client.existsClientDB(id));
                
            }else if(action.equals("supprimerArticle")){  
                
                String nomArticle = request.getParameter("nomArticle");          
                commande.supprimerArticleByIdPersonne(nomArticle, id);
                System.out.println("Article supprimé !");
                
            } else if(action.equals("getFactures")){
                
                Plats pl = new Plats();
                System.out.println("facture");
                response.getWriter().println(pl.factureIdToJSONArrayVersionAy(id));
                
            }else if(action.equals("getSuggestions")){
                
                String forwhat = request.getParameter("for");   
                String idArticle = request.getParameter("idArticle");    
                
                if(forwhat.equals("plats")){
                    
                    response.getWriter().println(commande.suggestionPizzaToJSON(idArticle));
                    
                }else if(forwhat.equals("films")){
                    
                    response.getWriter().println(commande.suggestionFilmToJSON(idArticle));
                    
                }else{
                    
                    System.out.println("problème dans la requetes des suggestions");
                }
            }
            
        } catch (SQLException ex) {
            System.out.println("Erreur : "+ex);
        }
        
    }	
    
}
