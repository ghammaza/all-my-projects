/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l3m;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**
 *
 * @author orandr
 */
public class XMLAble implements DataBaseAble {

    static final String CONN_IP = "jdbc:oracle:thin:@195.220.82.190:1521:im2ag";
    static final String USER = "aydine";
    static final String PASSWD = "Emre2839";


    public static Connection conn;
    public static Statement st;
    /*
        ghammaza
        j3ib6Sm3VY
    */


   public void connectToDatabase() throws SQLException {
        // Enregistrement du driver Oracle
        DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());  	    

        // Etablissement de la connection
        conn = DriverManager.getConnection(CONN_IP,USER,PASSWD);
        st = conn.createStatement();
        conn.setAutoCommit(true);
//        System.out.println("----------------------------------------------------------------------");
//        System.out.println("Connexion reussi sur XMLAble...");
//        System.out.println("Bonjour, vous etes actuellement connecte sur le compte de "+USER);
//        System.out.println("----------------------------------------------------------------------");
    }
    
    @Override
    public void disconnect() throws SQLException {
    	conn.close();
    }
    
    @Override
    public java.sql.ResultSet request(String request) throws SQLException{
    	ResultSet rs;
    	rs = st.executeQuery(request);
    	return rs;
    }
    
    public void requestUpdate(String request) throws SQLException{
        int nb = 0;
    	nb = st.executeUpdate(request);
        if(nb==0){
            System.out.println("Erreur, aucune ligne modifier avec la requete :"+request);
        }
    }

}
