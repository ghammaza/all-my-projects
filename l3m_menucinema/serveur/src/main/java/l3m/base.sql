DROP TABLE Facture;
DROP TABLE Commande;
DROP TABLE Article;
DROP TABLE Personne;

set pagesize 200;
set linesize 150;

CREATE TABLE Personne (
	idPersonne varchar(255) NOT NULL,
	nom varchar(15),
	prenom varchar(15),
	photo varchar(150),
	mail varchar(30),
	tel varchar(12),
	adresse varchar(150)
);

ALTER TABLE Personne
  ADD (
    CONSTRAINT Personne PRIMARY KEY (idPersonne)
  );

CREATE TABLE Article (
	idArticle int NOT NULL,
	nom varchar2(50),
	description varchar2(500),
	type varchar2(20),
	prix float(20),	
	imagePath varchar2(150),
	PRIMARY KEY (idArticle)
);

CREATE OR REPLACE TRIGGER ArticleTrigger
BEFORE INSERT ON Article 
FOR EACH ROW

BEGIN
  SELECT coalesce(MAX(idArticle) + 1, 1) INTO :new.idArticle
  FROM   Article;
END;
/


CREATE TABLE Commande (
	idCommande INT NOT NULL,
        dateCommande DATE,
	idArticle INT NOT NULL,
        quantite INT NOT NULL,
        pannier varchar(7) CHECK (pannier IN ('oui','non')),
	idPersonne varchar(255) NOT NULL,
	PRIMARY KEY (idCommande,idArticle)
);


CREATE TABLE Facture (
	idFacture INT NOT NULL,
        idCommande INT NOT NULL,
	idArticle INT NOT NULL,
        prix varchar2(20),
	PRIMARY KEY (idFacture),
	FOREIGN KEY (idCommande,idArticle) REFERENCES Commande(idCommande,idArticle)
);

CREATE OR REPLACE TRIGGER FactureTrigger
BEFORE INSERT ON Facture 
FOR EACH ROW

BEGIN
  SELECT coalesce(MAX(idFacture) + 1, 1) INTO :new.idFacture
  FROM   Facture;
END;
/
INSERT INTO Personne (idPersonne, nom, prenom, photo, mail, tel, adresse) VALUES ('N3vhjDQhuhQlr0eFyyhtWsq4eOd2','Aydin', 'Emre', 'www.emre.jpg','Emre.aydin@gmail.com'           , '1111111111', 'rue de aydin'); 
INSERT INTO Personne (idPersonne, nom, prenom, photo, mail, tel, adresse) VALUES ('2','Madide', 'Adam', 'www.adam.jpg','Adam.madide@gmail.com'         , '2222222222', 'rue de madide'); 
INSERT INTO Personne (idPersonne, nom, prenom, photo, mail, tel, adresse) VALUES ('3','Ghammaz', 'Ayoub', 'www.ayoub.jpg','Ayoub.ghammaz@gmail.com'     , '3333333333', 'rue de ghammaz'); 
INSERT INTO Personne (idPersonne, nom, prenom, photo, mail, tel, adresse) VALUES ('4','Phan', 'Thanh Thang', 'www.thang.jpg','ThangThang.phan@gmail.com', '4444444444', 'rue de phan'); 
INSERT INTO Personne (idPersonne, nom, prenom, photo, mail, tel, adresse) VALUES ('5','Orand', 'Regis', 'www.regis.jpg','regis.orand@gmail.com'         , '5555555555', 'rue de mayencin'); 


--Insert film dans article    A faire 


INSERT INTO Article(nom,description, type,prix, imagePath) VALUES('Savoyarde','description','PIZZA','20.0','https://static.cuisineaz.com/240x192/i92032-pizza-savoyarde.jpg');
INSERT INTO Article(nom,description, type,prix, imagePath) VALUES('Mexicaine','description','PIZZA','16.0','https://assets.afcdn.com/recipe/20151003/6002_w600.jpg');
INSERT INTO Article(nom,description, type,prix, imagePath) VALUES('Presto','description','PIZZA','10.0','https://assets.afcdn.com/recipe/20170105/24149_w600.jpg');
INSERT INTO Article(nom,description, type,prix, imagePath) VALUES('Allegro','description','PIZZA','24.0','https://assets.afcdn.com/recipe/20161130/2342_w420h344c1cx2699cy1799.jpg');
INSERT INTO Article(nom,description, type,prix, imagePath) VALUES('Sarrabande','description','PIZZA','50.0','http://img.over-blog-kiwi.com/2/66/65/46/20180306/ob_8a2fe1_wrap-pizza-saumon.jpg');
INSERT INTO Article(nom,description, type,prix, imagePath) VALUES('Reine','description','PIZZA','12.0','https://static.cuisineaz.com/680x357/i96018-pizza-reine.jpg');
INSERT INTO Article(nom,description, type,prix, imagePath) VALUES('ChevreMiel','description','PIZZA','25.0','http://www.soignon.fr/uploads/recette/.thumbs/Pizza%20ch%C3%A8vre%20miel%20et%20b%C3%BBche_f431e5.jpg');
INSERT INTO Article(nom,description, type,prix, imagePath) VALUES('Hawaienne','description','PIZZA','55.0','https://cache.marieclaire.fr/data/photo/w1000_c17/cuisine/47/pizza.jpg');
INSERT INTO Article(nom,description, type,prix, imagePath) VALUES('LasagnePizza','description','PIZZA','16.0','https://cache.marieclaire.fr/data/photo/w1000_c17/cuisine/47/pizza.jpg');
INSERT INTO Article(nom,description, type,prix, imagePath) VALUES('CarbonaraPizza','description','PIZZA','46.0','https://www.atelierdeschefs.com/media/recette-e22965-pizza-carbonara.jpg');
INSERT INTO Article(nom,description, type,prix, imagePath) VALUES('Avocat','description','PIZZA','17.0','https://static.cuisineaz.com/680x357/i54182-la-roquette-pas-si-bete.jpg');
INSERT INTO Article(nom,description, type,prix, imagePath) VALUES('Blanchette','description','PIZZA','8.0','https://media-cdn.tripadvisor.com/media/photo-s/11/75/fa/cd/pizza-de-roquefort-en.jpg');
INSERT INTO Article(nom,description, type,prix, imagePath) VALUES('PoirePizza','description','PIZZA','12.0','https://media-cdn.tripadvisor.com/media/photo-s/11/75/fa/cd/pizza-de-roquefort-en.jpg');
INSERT INTO Article(nom,description, type,prix, imagePath) VALUES('Calzone','description','PIZZA','11.0','https://foodsogoodmall.com/wp-content/uploads/2013/09/Seafood-Puff-Pastry-Pizza-1024x500.jpg');
INSERT INTO Article(nom,description, type,prix, imagePath) VALUES('4fromage','description','PIZZA','18.0','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRm02ftoMZlMmua_rnK_HFdEg_v9kFqG55m-DTGTRpUuETc3Y0v_w');
INSERT INTO Article(nom,description, type,prix, imagePath) VALUES('Anchois','description','PIZZA','18.0','https://static.cuisineaz.com/400x320/i23841-pizza-anchois.jpg');
INSERT INTO Article(nom,description, type,prix, imagePath) VALUES('JambonCreme','description','PIZZA','12.0','http://www.pizza-du-pharo.fr/wp-content/uploads/2013/08/Pizza-Cr%C3%A9mi%C3%A8re-Cr%C3%A8me-Fra%C3%AEche-Jambon-Mozzarella-Fromage-R%C3%A2p%C3%A9.jpg');
INSERT INTO Article(nom,description, type,prix, imagePath) VALUES('Poulet','description','PIZZA','16.0','https://static.cuisineaz.com/610x610/i36644-pizza-poulet-au-curry-et-ananas.jpg');
INSERT INTO Article(nom,description, type,prix, imagePath) VALUES('Royale','description','PIZZA','22.0','https://static.cuisineaz.com/680x357/i95731-pizza-royale.jpg');
INSERT INTO Article(nom,description, type,prix, imagePath) VALUES('BalAuBacon','description','PIZZA','13.0','https://static.cuisineaz.com/680x357/i52543-le-bal-du-bacon.jpg');
INSERT INTO Article(nom,description, type,prix, imagePath) VALUES('Avenger','description','FILM','5.0','urlFilm');
INSERT INTO Article(nom,description, type,prix, imagePath) VALUES('FastAndFurious','description','FILM','55.0','urlFilm');
INSERT INTO Article(nom,description, type,prix, imagePath) VALUES('TrueBlonde','description','FILM','15.0','urlFilm');
INSERT INTO Article(nom,description, type,prix, imagePath) VALUES('Taken','description','FILM','12.0','urlFilm');
INSERT INTO Article(nom,description, type,prix, imagePath) VALUES('IpMan','description','FILM','16.0','urlFilm');



INSERT INTO Commande(idCommande,idPersonne,dateCommande,idArticle,quantite,pannier) VALUES(1,'N3vhjDQhuhQlr0eFyyhtWsq4eOd2',TO_DATE('01/05/2019 11:50:32', 'DD/MM/YYYY HH24:MI:SS'),1,1,'non');
INSERT INTO Commande(idCommande,idPersonne,dateCommande,idArticle,quantite,pannier) VALUES(1,'N3vhjDQhuhQlr0eFyyhtWsq4eOd2',TO_DATE('01/05/2019 11:50:32', 'DD/MM/YYYY HH24:MI:SS'),2,1,'non');
INSERT INTO Commande(idCommande,idPersonne,dateCommande,idArticle,quantite,pannier) VALUES(1,'N3vhjDQhuhQlr0eFyyhtWsq4eOd2',TO_DATE('01/05/2019 11:50:32', 'DD/MM/YYYY HH24:MI:SS'),3,1,'non');
INSERT INTO Commande(idCommande,idPersonne,dateCommande,idArticle,quantite,pannier) VALUES(1,'N3vhjDQhuhQlr0eFyyhtWsq4eOd2',TO_DATE('01/05/2019 11:50:32', 'DD/MM/YYYY HH24:MI:SS'),22,1,'non');


INSERT INTO Commande(idCommande,idPersonne,dateCommande,idArticle,quantite,pannier) VALUES(2,'2',TO_DATE('12/04/2019 13:10:11', 'DD/MM/YYYY HH24:MI:SS'),20,1,'oui');
INSERT INTO Commande(idCommande,idPersonne,dateCommande,idArticle,quantite,pannier) VALUES(2,'2',TO_DATE('12/04/2019 13:10:11', 'DD/MM/YYYY HH24:MI:SS'),4,6,'oui');
INSERT INTO Commande(idCommande,idPersonne,dateCommande,idArticle,quantite,pannier) VALUES(2,'2',TO_DATE('12/04/2019 13:10:11', 'DD/MM/YYYY HH24:MI:SS'),21,1,'oui');


INSERT INTO Commande(idCommande,idPersonne,dateCommande,idArticle,quantite,pannier) VALUES(3,'4',TO_DATE('09/04/2019 09:40:16', 'DD/MM/YYYY HH24:MI:SS'),1,1,'non');
INSERT INTO Commande(idCommande,idPersonne,dateCommande,idArticle,quantite,pannier) VALUES(3,'4',TO_DATE('09/04/2019 09:40:16', 'DD/MM/YYYY HH24:MI:SS'),2,1,'non');
INSERT INTO Commande(idCommande,idPersonne,dateCommande,idArticle,quantite,pannier) VALUES(3,'4',TO_DATE('09/04/2019 09:40:16', 'DD/MM/YYYY HH24:MI:SS'),3,1,'non');
INSERT INTO Commande(idCommande,idPersonne,dateCommande,idArticle,quantite,pannier) VALUES(3,'4',TO_DATE('12/04/2019 13:10:11', 'DD/MM/YYYY HH24:MI:SS'),4,1,'non');
INSERT INTO Commande(idCommande,idPersonne,dateCommande,idArticle,quantite,pannier) VALUES(3,'4',TO_DATE('09/04/2019 09:40:16', 'DD/MM/YYYY HH24:MI:SS'),9,1,'non');
INSERT INTO Commande(idCommande,idPersonne,dateCommande,idArticle,quantite,pannier) VALUES(3,'4',TO_DATE('09/04/2019 09:40:16', 'DD/MM/YYYY HH24:MI:SS'),10,1,'non');
INSERT INTO Commande(idCommande,idPersonne,dateCommande,idArticle,quantite,pannier) VALUES(3,'4',TO_DATE('09/04/2019 09:40:16', 'DD/MM/YYYY HH24:MI:SS'),24,1,'non');



INSERT INTO Commande(idCommande,idPersonne,dateCommande,idArticle,quantite,pannier) VALUES(4,'4',TO_DATE('10/05/2019 10:46:16', 'DD/MM/YYYY HH24:MI:SS'),6,3,'non');
INSERT INTO Commande(idCommande,idPersonne,dateCommande,idArticle,quantite,pannier) VALUES(4,'4',TO_DATE('10/05/2019 10:46:16', 'DD/MM/YYYY HH24:MI:SS'),21,1,'non');



INSERT INTO Commande(idCommande,idPersonne,dateCommande,idArticle,quantite,pannier) VALUES(5,'5',TO_DATE('12/05/2019 09:46:16', 'DD/MM/YYYY HH24:MI:SS'),1,3,'non');
INSERT INTO Commande(idCommande,idPersonne,dateCommande,idArticle,quantite,pannier) VALUES(5,'5',TO_DATE('12/05/2019 09:46:16', 'DD/MM/YYYY HH24:MI:SS'),2,3,'non');
INSERT INTO Commande(idCommande,idPersonne,dateCommande,idArticle,quantite,pannier) VALUES(5,'5',TO_DATE('12/05/2019 09:46:16', 'DD/MM/YYYY HH24:MI:SS'),25,1,'non');




-- select to_char(dateCommande,'DD/MM/YYYY HH24:MI:SS')  as dateCommande from Commande where idArticle=4;
-- to_char(dateCommande,'DD/MM/YYYY HH24:MI:SS')  as dateCommande

INSERT INTO Facture(idCommande,idArticle,prix) VALUES(1,1,'101.0');
INSERT INTO Facture(idCommande,idArticle,prix) VALUES(3,1,'144.0');
INSERT INTO Facture(idCommande,idArticle,prix) VALUES(4,6,'41.0');
INSERT INTO Facture(idCommande,idArticle,prix) VALUES(5,1,'52.0');

select * from Personne;  
select * from Facture;
select * from Article;
select idCommande,to_char(dateCommande,'DD/MM/YYYY HH24:MI:SS')  as dateCommande,idArticle,quantite,pannier,idPersonne from Commande;
SELECT * FROM Personne P,Commande C,Facture F,Article A WHERE P.idPersonne=C.idPersonne AND A.idArticle=C.idArticle AND F.idCommande=C.idCommande AND P.idPersonne='4';

-- 195.220.82.190

commit;


