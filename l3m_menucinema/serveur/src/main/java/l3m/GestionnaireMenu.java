/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l3m;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author orandr
 */
public class GestionnaireMenu extends XMLAble {

    private Plats menu;
    private static final String nomFichier = "./Plats.xml";
    
    /**
     * Constructeur de GestionnaireMenu
     */
    public GestionnaireMenu() {
        menu = new Plats();
        ajouterAuMenu("1");
    }

    public GestionnaireMenu(List<String> idPlats) {
        menu = new Plats();
        for (int i = 0; i < idPlats.size(); i++) {
            ajouterAuMenu(idPlats.get(i));
        }

    }
    // Cree un DOM à partir d'un fichier XML
    public Document fromXML(String nomFichier) {
        try {
            return XMLUtil.DocumentFactory.fromFile(nomFichier);
        } catch (Exception ex) {
            System.out.println("Erreur, fromXML(" + nomFichier + ")");
        }
        return null;
    }

    protected boolean existPlatDB(String id) {
        Document _doc;
        boolean b;
        b = false;
        File f = new File(nomFichier);
        if (f.exists()) {
            _doc = fromXML(nomFichier);
            NodeList pplat = _doc.getElementsByTagName("ns1:plat");
            int i = 0;
            Element e;
            while (i < pplat.getLength() && !b) {
                e = (Element) pplat.item(i);
                if (e.getAttribute("id").equals(id)) {
                    b = true;
                } else {
                    i++;
                }
            }
        } else {
            System.out.println("Erreur, existPlatDB(" + id + "), le fichier "+nomFichier+" n'exist pas !");
        }
        if(!b){
            System.out.println("le plat avec l'id n°"+id+" n'est pas dans la Base De Donnee");
        }
        return b;
    }
    
    public void afficherIdPlat(){
        List<Plat> a =  new ArrayList<Plat>();
        a = menu.getPlats();
        for (int i = 0; i < a.size(); i++) {
            System.out.println("Pizza n°"+a.get(i).getId()+" :"+a.get(i).getNom());
        }
    }

    public boolean dejaDansMenu(String id){
        for (int i = 0; i < menu.getPlats().size(); i++) {
            if(menu.getPlats().get(i).getId().equals(id)){
                return true;
            }
        }
        return false;
    }
    
    public void ajouterAuMenu(String id) {
        Plat p = new Plat();
        Document _doc;
        File f = new File(nomFichier);
        ArrayList<String> ArrayIngredients = new ArrayList<String>();
        String nom = "";
        String type = "";
        double prix = 0.00;
        String imagePath = " ";

        if (f.exists()) {
            if(dejaDansMenu(id)){
                System.out.println("Plat deja dans le menu !");
            }else{
                if(existPlatDB(id)){ 
                    _doc = fromXML(nomFichier);

                    NodeList pplat = _doc.getElementsByTagName("ns1:plat");

                    for (int i = 0; i < pplat.getLength(); i++) {

                        if (pplat.item(i).getAttributes().getNamedItem("id").getTextContent().equals(id)) {
                            Node nNode = pplat.item(i);

                            if (nNode.getNodeName() != null) {

                                NodeList pLatCourant = (NodeList) nNode;
                                Plat platCourant = new Plat();
                                for (int j = 0; j < nNode.getChildNodes().getLength(); j++) {

                                    if (nNode.getChildNodes().item(j).getNodeValue() == null) {

                                        String currentName = nNode.getChildNodes().item(j).getNodeName();
                                        String currentValue = nNode.getChildNodes().item(j).getTextContent();

                                        if (currentName.equals("ns1:nom")) {
                                            nom = currentValue;
                                        } else if (currentName.equals("ns1:type")) {
                                            type = currentValue;
                                        } else if (currentName.equals("ns1:prix")) {
                                            prix = Double.parseDouble(currentValue);
                                        } else if (currentName.equals("ns1:imagePath")) {
                                            imagePath = currentValue;
                                        } else if (currentName.equals("ns1:ingredients")) {
                                            Node ingredients = nNode.getChildNodes().item(j);
                                            NodeList listIngredients = ingredients.getChildNodes();

                                            for (int k = 0; k < listIngredients.getLength(); k++) {
                                                currentName = listIngredients.item(k).getNodeName();
                                                if (currentName.equals("ns1:ingredient")) {

                                                    Node currentIngredient = listIngredients.item(k);
                                                    ArrayIngredients.add(currentIngredient.getTextContent());
                                                }
                                            }

                                        } else {

                                        }
                                    }

                                }
                                /*
                                for (int j = 0; j < pLatCourant.getLength(); j++) {
                                    Node nNodeCourant = pLatCourant.item(j);
                                    System.out.println(nNodeCourant.getNodeValue());
                                }*/

                            }
                        }

                        NodeList platReference = pplat.item(i).getChildNodes();

                    }

        //            System.out.println("nom : " + nom);
        //            System.out.println("type : " + type);
        //            System.out.println("prix : " + prix);
        //            System.out.println("imagePath : " + imagePath);

                    p.setIngredients(ArrayIngredients);
                    p.setId(id);
                    p.setImagePath(imagePath);
                    p.setNom(nom);
                    p.setPrix(prix);
                    p.setType(type);
                    this.menu.ajouterPlat(p);
                }
            }   
        } else {
            System.out.println("Erreur, ajouterAuMenu()!");
        }
    }

    public Plat getMenuDB(String id) {
        Document _doc;
        File f = new File(nomFichier);
        Plat p = new Plat();
        ArrayList<String> ArrayIngredients = new ArrayList<String>();
        String nom = "";
        String type = "";
        double prix = 0.00;
        String imagePath = " ";

        if (f.exists()) {
            _doc = fromXML(nomFichier);

            NodeList pplat = _doc.getElementsByTagName("ns1:plat");

            for (int i = 0; i < pplat.getLength(); i++) {

                if (pplat.item(i).getAttributes().getNamedItem("id").getTextContent().equals(id)) {
                    Node nNode = pplat.item(i);

                    if (nNode.getNodeName() != null) {

                        NodeList pLatCourant = (NodeList) nNode;
                        Plat platCourant = new Plat();
                        for (int j = 0; j < nNode.getChildNodes().getLength(); j++) {

                            if (nNode.getChildNodes().item(j).getNodeValue() == null) {

                                String currentName = nNode.getChildNodes().item(j).getNodeName();
                                String currentValue = nNode.getChildNodes().item(j).getTextContent();

                                if (currentName.equals("ns1:nom")) {
                                    nom = currentValue;
                                } else if (currentName.equals("ns1:type")) {
                                    type = currentValue;
                                } else if (currentName.equals("ns1:prix")) {
                                    prix = Double.parseDouble(currentValue);
                                } else if (currentName.equals("ns1:imagePath")) {
                                    imagePath = currentValue;
                                } else if (currentName.equals("ns1:ingredients")) {
                                    Node ingredients = nNode.getChildNodes().item(j);
                                    NodeList listIngredients = ingredients.getChildNodes();

                                    for (int k = 0; k < listIngredients.getLength(); k++) {
                                        currentName = listIngredients.item(k).getNodeName();
                                        if (currentName.equals("ns1:ingredient")) {

                                            Node currentIngredient = listIngredients.item(k);
                                            ArrayIngredients.add(currentIngredient.getTextContent());
                                        }
                                    }

                                } else {

                                }
                            }

                        }

                    }
                }

            }

            p.setIngredients(ArrayIngredients);
            p.setId(id);
            p.setImagePath(imagePath);
            p.setNom(nom);
            p.setPrix(prix);
            p.setType(type);

        } else {
            System.out.println("Erreur, ajouterAuMenu()!");
        }
        this.menu.ajouterPlat(p);

        return p;
    }

    public void enleverDuMenu(String id) {
        menu.removePlatId(id);
    }

    public Plats getMenu() {
        return this.menu;
    }
    
    public JSONArray getCarteDBToJsonByID(int id){
        
        JSONArray list = new JSONArray();
        int taille = getCarteDB().getPlats().size();
        
        if(id > taille){
            JSONArray arrayError = new JSONArray();   
            JSONObject objError = new JSONObject();  
            objError.put("status", "erreur");
            arrayError.add(objError);
            return arrayError;
        }
        Plat currentPlat = getMenuDB(""+id);
        
        
        JSONObject article = new JSONObject();
        JSONObject obj = new JSONObject();       
            obj.put("id", currentPlat.getId());
            obj.put("nom", currentPlat.getNom());
            obj.put("type", currentPlat.getType());
            obj.put("prix", currentPlat.getPrix());
            obj.put("imagePath", currentPlat.getImagePath());            
            article.put("infos", obj);
            
            ArrayList<String> arrayIngredients = currentPlat.getIngredients();
            JSONArray listIngredients = new JSONArray();
            
            for (int j = 0; j < arrayIngredients.size(); j++) {
                JSONObject objIngr = new JSONObject();
                objIngr.put("ingredient", arrayIngredients.get(j));
                listIngredients.add(objIngr);
            }
            
            article.put("ingredients", listIngredients);
            list.add(article);
            
        return list;
    }
    
    
    public void getRandomCarteFromDB(){
        Plats carteDB = getCarteDB();
        
    }
    
    
    
    public JSONArray getCarteDBToJson(){
        JSONArray list = new JSONArray();
        
        Plats carteDB = getCarteDB();
        ArrayList<Plat> arrayPlat = carteDB.getPlats();        

        for (int i = 0; i < arrayPlat.size(); i++) {
            JSONObject article = new JSONObject();
            
            JSONObject obj = new JSONObject();
            obj.put("id", arrayPlat.get(i).getId());
            obj.put("nom", arrayPlat.get(i).getNom());
            obj.put("type", arrayPlat.get(i).getType());
            obj.put("prix", arrayPlat.get(i).getPrix());
            obj.put("imagePath", arrayPlat.get(i).getImagePath());
           
            article.put("infos", obj);
            
            ArrayList<String> arrayIngredients = arrayPlat.get(i).getIngredients();
            JSONArray listIngredients = new JSONArray();
            listIngredients.add(obj);
            for (int j = 0; j < arrayIngredients.size(); j++) {
                JSONObject objIngr = new JSONObject();
                objIngr.put("nom", arrayIngredients.get(j));
                listIngredients.add(objIngr);
            }
            article.put("ingredients", listIngredients);
  
            list.add(article);
        
            
        }
        
        
        return list;
    }
    public Plats getCarteDB() {
        
        Plats p = new Plats();
        Document _doc;
        File f = new File(nomFichier);
        ArrayList<Plat> ArrayPlat = new ArrayList<Plat>();

        String nom = "";
        String type = "";
        double prix = 0.00;
        String imagePath = " ";

        if (f.exists()) {
            _doc = fromXML(nomFichier);

            NodeList pplat = _doc.getElementsByTagName("ns1:plat");

            for (int i = 0; i < pplat.getLength(); i++) {
                String idPlat = pplat.item(i).getAttributes().getNamedItem("id").getTextContent();
                Plat platCourant = getMenuDB(idPlat);

                ArrayPlat.add(platCourant);

            }

        }else{
            System.out.println("Erreur, getCarteDB() fichier : "+nomFichier+" inexistant");
        }
        p.setPlats(ArrayPlat);
        return p;
    }

}

