/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l3m;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import jdk.nashorn.internal.parser.JSONParser;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author orandr
 */
public class PizzaServlet extends HttpServlet  {   
    
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
     
            GestionnaireCommande commande;
            try {
                commande = new GestionnaireCommande();
                StringBuilder sb = new StringBuilder();
                BufferedReader br = request.getReader()
                        ;
                String str = null;

               
                   
                while ((str = br.readLine()) != null) {
                    sb.append(str);
                }               

                str = sb.toString();     
                str = str.substring(0, str.length()-1);
                str = str.substring(1, str.length());     
                
      

                JsonObject jsonObject = new JsonParser().parse(str).getAsJsonObject();
                
                String idPersonne = jsonObject.get("idPersonne").toString();
                String idArticle = jsonObject.get("id").toString();
                idPersonne = deleteQuotes(idPersonne);
 

                String imagePath = jsonObject.get("imgPath").toString();
                imagePath = deleteQuotes(imagePath);
               
                String nom = jsonObject.get("nom").toString();
                nom = deleteQuotes(nom);
               
                String description = jsonObject.get("description").toString();
                description =  deleteQuotes(description);
               
                String type = jsonObject.get("type").toString();
                type = deleteQuotes(type);
                
                String prixUnitaire = jsonObject.get("prix").toString();
                              
                
                if(type.equals("Film")){
                    commande.ajouterArticle(Integer.parseInt(idArticle), nom, description, type, prixUnitaire, imagePath);
                }

                if(!commande.articleExistInHisCommande(idPersonne, nom)){            
                    commande.enregistrerCommandeDB(idPersonne, ""+commande.getArticleIdByName(nom));                 
                    System.out.println("Ajouté dans le panier + "+idPersonne); 
                }else {
                    System.out.println("l'article existe dans le panier ! ");
                }      
            } catch (SQLException ex) {
                    Logger.getLogger(PizzaServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
            
           
        }
        
        public String deleteQuotes(String res){
          
            res = res.substring(0, res.length()-1);
            res = res.substring(1, res.length());     
            return res;
        }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            GestionnaireMenu gm = new GestionnaireMenu();         
 
        response.setStatus(HttpServletResponse.SC_OK);
        
        
        int id = Integer.parseInt(request.getParameter("id"));
        String action = request.getParameter("action");
        
        if(action.equals("retourner")){
            if(id == -1){
                response.getWriter().println(gm.getCarteDBToJson());
            } else {
                //getCarteDBToJsonByID
               response.getWriter().println(gm.getCarteDBToJsonByID(id));
            }
        }
        
    }
    
}
