/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l3m;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author orandr
 */
public class TestBase{
    public static void main(String[] args) throws SQLException {
        SQLAble x = new SQLAble();
        x.connectToDatabase();
        
        //GestionnaireCommande gc = new GestionnaireCommande("1");
        
        ResultSet rs;
        String prix,id;
        prix="";
        id="";
        rs = x.request("SELECT idArticle,prix FROM Article ");
        while(rs.next()) {
            id = rs.getString(1);
            prix = rs.getString(2);
            System.out.println("Article id : "+id+" prix : "+prix);
        }
        
        System.out.println("");
        System.out.println("");
        
        ResultSet rs2;
        //Personne (nom, prenom, photo, mail, tel, adresse)
        String nom,prenom,mail,tel,adresse;
        nom="";
        prenom="";
        mail="";
        tel="";
        adresse="";
        rs2 = x.request("SELECT nom,prenom,mail,tel,adresse FROM Personne ");
        while(rs2.next()) {
            nom = rs2.getString(1);
            prenom = rs2.getString(2);
            mail = rs2.getString(3);
            tel = rs2.getString(4);
            adresse = rs2.getString(5);
            System.out.println("Personne nom : "+nom+" ,prenom : "+prenom+" ,mail : "+mail+" ,tel : "+tel+" ,adresse : "+adresse);
        }
        System.out.println("");
        System.out.println("");
        
        ResultSet rs3;
        String idPersonne,idArticle,idCommande,dateCommande,quantite,pannier;
        idCommande="";
        idPersonne="";
        idArticle="";
        dateCommande ="";
        quantite="";
        pannier="";
        //,to_char(dateCommande,'DD/MM/YYYY HH24:MI:SS') 
        String rq="SELECT idCommande,idPersonne,to_char(dateCommande,'DD/MM/YYYY HH24:MI:SS')  as dateCommande,  idArticle,quantite,pannier FROM Commande ";
        System.out.println(rq);
        rs = x.request(rq);
        while(rs.next()) {
            idCommande = rs.getString(1);
            idPersonne = rs.getString(2);
            dateCommande = rs.getString(3);
            idArticle = rs.getString(4);
            quantite = rs.getString(5);
            pannier = rs.getString(6);
            
            //idArticle = rs.getString(3);
            System.out.println("Commande id : "+idCommande+" idPersonne : "+idPersonne+" dateCommande : "+dateCommande+" idArticle : "+idArticle+" quantite : "+quantite+" pannier : "+pannier);
        }
        
        System.out.println("");
        System.out.println("");
        
        ResultSet rs4;
        String idFacture,idCommandeFact,prixFact;
        idFacture="";
        idCommandeFact="";
        prixFact="";
        rs4 = x.request("SELECT idFacture,idCommande,prix FROM Facture ");
        while(rs4.next()) {
            idFacture = rs4.getString(1);
            idPersonne = rs4.getString(2);
            prixFact = rs4.getString(3);
            System.out.println("Commande idFacture : "+idFacture+" idCommandeFact : "+idCommandeFact+" prixFact : "+prixFact);
        }
        
        x.disconnect();
        
    }

}
