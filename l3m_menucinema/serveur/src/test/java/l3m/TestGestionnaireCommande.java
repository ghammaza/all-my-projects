/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l3m;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.simple.JSONArray;

/**
 *
 * @author orandr
 */
public class TestGestionnaireCommande {
    
    public static void main(String[] args) throws SQLException {
        SQLAble x = new SQLAble();
        x.connectToDatabase();
        int test;
        test = 12;
        if(test==1){
            GestionnaireCommande gc = new GestionnaireCommande();
            Commande c = new Commande();
            c.setAdresseLivraison("Miam");
            c.setDate("11/02/1995");
            c.setId("1");
            c.setIdClient("5");

            List<String> listFilm = new ArrayList<String>();
            listFilm.add("1");
            listFilm.add("2");
            listFilm.add("4");

            List<String> listPizza = new ArrayList<String>();
            listPizza.add("8");
            listPizza.add("9");
            listPizza.add("10");


            c.setIdFilms(listFilm);
            c.setIdPlats(listPizza);
            c.setPrix(56.8);
            gc.afficherCommandeReference(c);
        }else if(test==2){
            
            GestionnaireCommande gc = new GestionnaireCommande();
            Commande c = new Commande();
            c.setAdresseLivraison("Miam");
            c.setDate("11/02/1995");
            c.setId("1");
            c.setIdClient("5");

            List<String> listFilm = new ArrayList<String>();
            listFilm.add("1");
            listFilm.add("9009");
            listFilm.add("4");

            List<String> listPizza = new ArrayList<String>();
            listPizza.add("8");
            listPizza.add("86");
            listPizza.add("10");


            c.setIdFilms(listFilm);
            c.setIdPlats(listPizza);
            c.setPrix(56.8);
            gc.afficherCommandeReference(c);
        }else if(test==3){
            
            Commande c = new Commande();
            c.setAdresseLivraison("Miam");
            c.setDate("11/02/1995");
            c.setId("1");
            c.setIdClient("5");

            List<String> listFilm = new ArrayList<String>();
            listFilm.add("1");
            listFilm.add("9009");
            listFilm.add("4");

            List<String> listPizza = new ArrayList<String>();
            listPizza.add("8");
            listPizza.add("10");


            c.setIdFilms(listFilm);
            c.setIdPlats(listPizza);
            
            
            c.setPrix(56.8);
            
            String idClient="1";
            String adresseLivraison="";
            
            
            GestionnaireCommande gc = new GestionnaireCommande(idClient,listPizza,listFilm,adresseLivraison);
            gc.afficherCommande();
        }else if(test==4){
        
            GestionnaireCommande gc = new GestionnaireCommande();
            gc.getList("PIZZA");
        }else if (test==5){
            GestionnaireCommande gc = new GestionnaireCommande();
            gc.genererInsertCommande(50, 10);
            //select idCommande,idPersonne,to_char(dateCommande,'DD/MM/YYYY HH24:MI:SS')  as dateCommande,idArticle,quantite,pannier from Commande;
        }else if (test==6){
            GestionnaireCommande gc = new GestionnaireCommande();
            gc.enregistrerCommandeDB("1","19");
        }else if (test==7){
            GestionnaireCommande gc = new GestionnaireCommande();
            //gc.updateQuantite("2", "20", "10");
            ArrayList<String> listIdArticle = new ArrayList<>();
            
            listIdArticle.add("1");
            listIdArticle.add("15");
            listIdArticle.add("10");
            listIdArticle.add("24");
            
            gc.enregistrerCommandeDBListArticle("5", listIdArticle);
        } else if (test==8){
            GestionnaireCommande gc = new GestionnaireCommande();
            ArrayList<String> listIdArticle = new ArrayList<>();
            listIdArticle = gc.suggestionFilm("3");
            for (int i = 0; i < listIdArticle.size(); i++) {
                System.out.println("idFilm Suggestion :"+listIdArticle.get(i));
            }
            
            JSONArray j =  gc.suggestionFilmToJSON("3");
            System.out.println("JSON :\n"+j);
        } else if(test==9){
            GestionnaireCommande gc = new GestionnaireCommande();
            //gc.enregistrerCommandeDB("1","19");
            for (int i = 0; i < gc.getPanierByUserID("2").size(); i++) {
                System.out.println(gc.getPanierByUserID("2").get(i));
            }
	} else if (test==10){
            GestionnaireCommande gc = new GestionnaireCommande();
            ArrayList<String> listIdArticle = new ArrayList<>();
            listIdArticle = gc.suggestionPizza("1");
            for (int i = 0; i < listIdArticle.size(); i++) {
                System.out.println("idFilm Suggestion :"+listIdArticle.get(i));
            }
            
            JSONArray j =  gc.suggestionPizzaToJSON("1");
            System.out.println("JSON :\n"+j);
        }else if(test == 11){ // test de l'existence d'un article dans le panier d'une 
            GestionnaireCommande gc = new GestionnaireCommande();
            System.out.println(gc.articleExistInHisCommande("IOt7AWg9J0TU0yaaJdLRG4xNa4D2", "Hellboy"));
        } else if (test == 12){
            GestionnaireCommande gc = new GestionnaireCommande();
            
            System.out.println(gc.panierVide("CEMofFOeyRUcavlOLSMYV9FwJMz2"));
            
            //supprimerArticleByIdPersonne
        }
                
        x.disconnect();
    }
    
    /*
    [{"Pizza":{"prix":"20","imagePath":"https:\/\/static.cuisineaz.com\/240x192\/i92032-pizza-savoyarde.jpg","type":"PIZZA","nom":"Savoyarde"},"id":"1"},{"Pizza":{"prix":"16","imagePath":"https:\/\/assets.afcdn.com\/recipe\/20151003\/6002_w600.jpg","type":"PIZZA","nom":"Mexicaine"},"id":"2"},{"Pizza":{"prix":"10","imagePath":"https:\/\/assets.afcdn.com\/recipe\/20170105\/24149_w600.jpg","type":"PIZZA","nom":"Presto"},"id":"3"},{"Pizza":{"prix":"16","imagePath":"https:\/\/cache.marieclaire.fr\/data\/photo\/w1000_c17\/cuisine\/47\/pizza.jpg","type":"PIZZA","nom":"LasagnePizza"},"id":"9"},{"Pizza":{"prix":"46","imagePath":"https:\/\/www.atelierdeschefs.com\/media\/recette-e22965-pizza-carbonara.jpg","type":"PIZZA","nom":"CarbonaraPizza"},"id":"10"}]
    
    */
}
