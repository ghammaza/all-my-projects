/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l3m;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author madidea
 */
public class TestPlat {
    
    public static void main(String [] args) throws SQLException{
        
        Plats p= new Plats();
        
        
        Plat Savoyarde=new Plat();
        Savoyarde.setId("1");
        Savoyarde.setNom("Savoyarde");
        Savoyarde.setType("Plat");
        Savoyarde.setPrix(20.0);
        Savoyarde.setImagePath("https://static.cuisineaz.com/240x192/i92032-pizza-savoyarde.jpg");
        
        ArrayList<String> ingredientsSavoyarde = new ArrayList<String>();
        ingredientsSavoyarde.add("Lardon");
        ingredientsSavoyarde.add("CremeFraiche");
        ingredientsSavoyarde.add("PommeDeTerre");
        ingredientsSavoyarde.add("Oignon");
        ingredientsSavoyarde.add("Oeuf");
        
        Savoyarde.setIngredients(ingredientsSavoyarde);
        
        p.ajouterPlat(Savoyarde);
        
        Plat Mexicaine=new Plat();
        Mexicaine.setId("2");
        Mexicaine.setNom("Mexicaine");
        Mexicaine.setType("Plat");
        Mexicaine.setPrix(16.0);
        Mexicaine.setImagePath("https://assets.afcdn.com/recipe/20151003/6002_w600.jpg");
        
        ArrayList<String> ingredientsMexicaine = new ArrayList<String>();
        ingredientsMexicaine.add("Poivron");
        ingredientsMexicaine.add("Merguez");
        ingredientsMexicaine.add("ViandeHachee");
        ingredientsMexicaine.add("Jambon");
        ingredientsMexicaine.add("Oignon");
        ingredientsMexicaine.add("Olives");
        
        Mexicaine.setIngredients(ingredientsMexicaine);
        
        p.ajouterPlat(Mexicaine);
        
        p.toXML();
//        p.ajoutFacture(6,"50");
//        
//        p.ajoutFacture(10, "12");
        
        //p.factureDBtoXML();
        Plats ar= new Plats();
        
        System.out.println("JSON :\n"+ar.factureIdToJSONArrayVersionAy("N3vhjDQhuhQlr0eFyyhtWsq4eOd2"));
        
        
//        ArrayList<String> listIdFacture = new ArrayList<String>();
//        ArrayList<String>  listIdCommande = new ArrayList<String>();
//        ArrayList<String>  listPrix = new ArrayList<String>();
//        
//        listIdFacture.add("1");
//        listIdFacture.add("2");
//        listIdFacture.add("3");
//        
//        listIdCommande.add("5");
//        listIdCommande.add("6");
//        listIdCommande.add("7");
//        
//        listPrix.add("13.0");
//        listPrix.add("26.0");
//        listPrix.add("8.0");
        
        
//        p.factureToXML(listIdFacture,listIdCommande,listPrix);
        
        //System.out.println(p.getPrixPlat("22"));
        
    }
    
    
    
}
