DROP TABLE Facturation;
DROP TABLE Reservations;
DROP TABLE Location;
DROP TABLE Bornette;
DROP TABLE Station;
DROP TABLE PlageHoraire;
DROP TABLE Velo;
DROP TABLE ModeleVelo;
DROP TABLE NonAbonnes;
DROP TABLE Abonnes;


set pagesize 200;
set linesize 300;

CREATE TABLE Abonnes(
    idAbonne INTEGER PRIMARY KEY,
    nom varchar(20) NOT NULL,
    prenom varchar(20) NOT NULL,
    dateNais DATE,
    adresse varchar(50) NOT NULL,
    sexe varchar(10) CHECK (sexe IN ('feminin','masculin')),
    codeSecret NUMBER,
    cb NUMBER(16) check (cb > 0),
    lastAction varchar(5) CHECK (lastAction IN ('loc','res'))
);

--ALTER TABLE Abonnes ADD (
    --CONSTRAINT pk_abonnes PRIMARY KEY (idAbonne)
--);


--CREATE SEQUENCE Abonnes_seq START WITH 1;

CREATE OR REPLACE TRIGGER AbonnesTrigger
BEFORE INSERT ON Abonnes
FOR EACH ROW

BEGIN
  SELECT coalesce(MAX(idAbonne) + 1, 1) INTO :new.idAbonne
  FROM   Abonnes;
END;
/


Create trigger TestAbonne BEFORE insert on Abonnes
FOR  each row
Declare
abnum NUMBER:=0;
Begin
select idAbonne into abnum from Abonnes where nom=:new.nom and prenom=:new.prenom and cb=:new.cb;
if abnum<>0 then raise_application_error(-20100,' vous etes deja abonne!');
end if;
Exception when NO_DATA_FOUND then DBMS_OUTPUT.PUT_LINE ('OK');
End;
/


create or replace procedure addClientAb (nomC varchar,prenomC  varchar,dateNaissaance  date,adresse  varchar , sexe  varchar , cb  Integer ,codeSecret  Integer )
        is
        client_exist NUMBER;
        BEGIN
        SELECT count(*)
            INTO client_exist
            FROM Abonnes
            WHERE nom=nomC and prenom=prenomC;
        IF client_exist=0 then INSERT INTO Abonnes(nom ,prenom ,dateNais ,adresse ,sexe,codeSecret,cb ) VALUES (nomC ,prenomC ,TO_DATE(dateNaissaance, 'DD/MM/YYYY') ,adresse  , sexe  ,codeSecret , cb ) ;
        END IF;
        END;
/




CREATE TABLE NonAbonnes(
    idNonAbonne INTEGER PRIMARY KEY,
    cb INTEGER,
    codeSecret NUMBER
);



CREATE OR REPLACE TRIGGER NonAbonnesTrigger
BEFORE INSERT ON NonAbonnes
FOR EACH ROW

BEGIN
  SELECT coalesce(MAX(idNonAbonne) + 1, 1) INTO :new.idNonAbonne
  FROM   NonAbonnes;
END;
/


CREATE TABLE ModeleVelo(
    idModeleVelo INTEGER PRIMARY KEY ,
    libelleModele VARCHAR(50),
    type VARCHAR(20) NOT NULL,
    prix NUMBER CHECK (prix > 0)
);


CREATE TABLE Velo(
    idVelo NUMBER(4) PRIMARY KEY,
    etat VARCHAR2(5) CHECK (etat in ('OK','HS')),
    dateMiseService DATE,
    dispo varchar(5) CHECK (dispo IN ('oui','non')),
    idModeleVelo NUMBER ,
    CONSTRAINT FK_idModeleVelo FOREIGN KEY (idModeleVelo)
    REFERENCES ModeleVelo (idModeleVelo)  ON DELETE CASCADE
);

CREATE OR REPLACE TRIGGER VeloTrigger
BEFORE INSERT ON Velo
FOR EACH ROW

BEGIN
  SELECT coalesce(MAX(idVelo) + 1, 1) INTO :new.idVelo
  FROM   Velo;
END;
/



CREATE TABLE PlageHoraire(
    idPlageHoraire INTEGER PRIMARY KEY,
    dateDebut DATE ,
    dateFin DATE ,
    prime VARCHAR2(10),    
    CONSTRAINT prime CHECK (prime in ('Vplus','Vmoins','Vnull')) 
);

CREATE TABLE Station(
    idStation INTEGER PRIMARY KEY ,
    adresse VARCHAR2(20) NOT NULL,
    nbBornette INTEGER check (nbBornette > 0),
    idplage NUMBER REFERENCES PlageHoraire (idPlageHoraire)
);

CREATE TABLE Bornette(
    idBornette NUMBER(4),
    idStation NUMBER(4) REFERENCES Station (idStation),
    etat VARCHAR2(5),
    idVelo NUMBER(4),
    CONSTRAINT etat CHECK (etat in ('OK','HS')),
    CONSTRAINT PK_bornette PRIMARY KEY (idBornette,idStation)
);


CREATE OR REPLACE TRIGGER StationTrigger
BEFORE INSERT ON Station
FOR EACH ROW

BEGIN
  SELECT coalesce(MAX(idStation) + 1, 1) INTO :new.idStation
  FROM   Station;
END;
/



CREATE TABLE Location(
    idLocation INTEGER PRIMARY KEY ,
    stationDebut NUMBER REFERENCES Station(idStation),
    dateDebut DATE,
    clientAbonnes NUMBER REFERENCES Abonnes(idAbonne),
    clientNonAbonnes NUMBER REFERENCES NonAbonnes(idNonAbonne),
    velo NUMBER REFERENCES Velo(idVelo)   ON DELETE CASCADE,
    renduVelo VARCHAR(5) check (renduVelo in ('oui','non'))
);

create or replace procedure addLocation (NewidClientAb  Integer,NewidClientNAb  Integer,NewidVelo Integer,NewidStation  Integer, NewdateDebut  VARCHAR2 )
IS
idBornetteV NUMBER;
idbb NUMBER ;
begin
    select count(*) into idbb from Abonnes where idAbonne=NewidClientAb;
   if idbb=0 then
           INSERT  INTO location (stationDebut,dateDebut,clientAbonnes,clientNonAbonnes,Velo,renduVelo) Values ( NewidStation,TO_DATE(NewdateDebut,'HH24:MI:SS'),null,NewidClientNAb,NewidVelo,'non') ; 
   else
           INSERT  INTO location (stationDebut,dateDebut,clientAbonnes,clientNonAbonnes,Velo,renduVelo) Values ( NewidStation,TO_DATE(NewdateDebut,'HH24:MI:SS'),NewidClientAb,null,NewidVelo,'non') ;
           UPDATE Abonnes SET lastAction = 'loc' where idAbonne=NewidClientAb;
   end if;
        Select idBornette INTO idBornetteV FROM Bornette where idVelo=NewidVelo;      
        UPDATE velo SET dispo = 'non' where idVelo=NewidVelo;
        update Bornette set idvelo=null where idbornette=idBornetteV;
    END;
/


CREATE OR REPLACE TRIGGER LocationTrigger
BEFORE INSERT ON Location
FOR EACH ROW

BEGIN
  SELECT coalesce(MAX(idLocation) + 1, 1) INTO :new.idLocation
  FROM   Location;
END;
/


CREATE TABLE Reservations(
    idReservation NUMBER,
    idClient NUMBER REFERENCES Abonnes(idAbonne),
    --modeleVelo NUMBER REFERENCES ModeleVelo(idModeleVelo),
    idVelo Number REFERENCES Velo(idVelo),
    station NUMBER REFERENCES Station(idStation),
    renduVelo VARCHAR(5) check (renduVelo in ('oui','non')),
    dateDebut Date,
    dateDebutJour Date,
    CONSTRAINT PK_Reservation PRIMARY KEY (idReservation,idClient,idVelo)
);


create or replace procedure addReservationAbo (NewidReservation IN Integer, NewidClient IN Integer,NewidVelo IN Integer,NewidStation IN NUMBER, NewdateDebut IN VARCHAR2, NewdateDebutJour IN VARCHAR2  )
IS
idBornetteV NUMBER;
    BEGIN
        Select idBornette INTO idBornetteV FROM Bornette where idVelo=NewidVelo ;
        INSERT  INTO reservations (idReservation, idClient,idVelo,station,dateDebut,dateDebutJour) Values ( NewidReservation, NewidClient,NewidVelo,NewidStation,TO_DATE(NewdateDebut,'HH24:MI:SS'),TO_DATE(NewdateDebutJour,'DD/MM/YYYY') ) ;       
        UPDATE velo SET dispo = 'non' where idVelo=NewidVelo;
        UPDATE Abonnes SET lastAction = 'res' where idAbonne=NewidClient;
        update Bornette set idvelo=0 where idbornette=idBornetteV;
    END;
/
create or replace procedure renduVeloProcedure (Newid IN Integer,NewidBornette IN Integer, NewidClient IN Integer)
IS 
    lastA VARCHAR2(5);
    newidVelo NUMBER;
    BEGIN     
        SELECT lastAction INTO lastA from Abonnes where idAbonne = NewidClient;
        IF  lastA='res' then 
        	UPDATE reservations set renduVelo='oui' where idReservation=Newid;
        	select idVelo INTO newidVelo from reservations where idreservation=Newid;
        	update bornette set idvelo=newidVelo where idbornette=NewidBornette;
		update velo set dispo='oui' where idVelo=newidVelo;
        ELSE 
        	UPDATE Location set renduVelo='oui' where idLocation=Newid;
        	select Velo INTO newidVelo from Location where idLocation=Newid;
        	update bornette set idvelo=newidVelo where idbornette=NewidBornette;
		update velo set dispo='oui' where idVelo=newidVelo;
        END IF;
    END;
/
create or replace procedure renduVeloProcedureNAb (Newid IN Integer,NewidBornette IN Integer, NewidNonAb IN Integer)
IS  
lastA VARCHAR2(5);
    newidVelo NUMBER;
    BEGIN     
       
        	UPDATE Location set renduVelo='oui' where idLocation=Newid;
        	select Velo INTO newidVelo from Location where idLocation=Newid;
        	update bornette set idvelo=newidVelo where idbornette=NewidBornette;
		update velo set dispo='oui' where idVelo=newidVelo;
        
    END;
/





CREATE TABLE Facturation(
    idFacturation NUMBER PRIMARY KEY ,
    clientAbonnes NUMBER REFERENCES Abonnes(idAbonne),
    clientNonAbonnes NUMBER REFERENCES NonAbonnes(idNonAbonne),
    prix NUMBER CHECK ( prix > 0 ),
    idVelo NUMBER
);




CREATE OR REPLACE TRIGGER FacturationTrigger
BEFORE INSERT ON Facturation
FOR EACH ROW

BEGIN
  SELECT coalesce(MAX(idFacturation) + 1, 1) INTO :new.idFacturation
  FROM   Facturation;
END;
/


insert into PlageHoraire (idPlageHoraire,dateDebut,dateFin,prime) values (1,TO_DATE('08:00:00','HH24:MI:SS'),TO_DATE('16:00:00','HH24:MI:SS'),'Vplus');
insert into PlageHoraire (idPlageHoraire,dateDebut,dateFin,prime) values (2,TO_DATE('08:00:00','HH24:MI:SS'),TO_DATE('16:00:00','HH24:MI:SS'),'Vmoins');
insert into PlageHoraire (idPlageHoraire,dateDebut,dateFin,prime) values (3,TO_DATE('08:00:00','HH24:MI:SS'),TO_DATE('16:00:00','HH24:MI:SS'),'Vnull');


insert into PlageHoraire (idPlageHoraire,dateDebut,dateFin,prime) values (4,TO_DATE('16:00:00','HH24:MI:SS'),TO_DATE('00:00:00','HH24:MI:SS'),'Vmoins');
insert into PlageHoraire (idPlageHoraire,dateDebut,dateFin,prime) values (5,TO_DATE('16:00:00','HH24:MI:SS'),TO_DATE('00:00:00','HH24:MI:SS'),'Vnull');
insert into PlageHoraire (idPlageHoraire,dateDebut,dateFin,prime) values (6,TO_DATE('16:00:00','HH24:MI:SS'),TO_DATE('00:00:00','HH24:MI:SS'),'Vplus');


insert into PlageHoraire (idPlageHoraire,dateDebut,dateFin,prime) values (7,TO_DATE('00:00:00','HH24:MI:SS'),TO_DATE('08:00:00','HH24:MI:SS'),'Vnull');
insert into PlageHoraire (idPlageHoraire,dateDebut,dateFin,prime) values (8,TO_DATE('00:00:00','HH24:MI:SS'),TO_DATE('08:00:00','HH24:MI:SS'),'Vplus');
insert into PlageHoraire (idPlageHoraire,dateDebut,dateFin,prime) values (9,TO_DATE('00:00:00','HH24:MI:SS'),TO_DATE('08:00:00','HH24:MI:SS'),'Vmoins');



INSERT INTO Abonnes(nom ,prenom ,dateNais ,adresse ,sexe,codeSecret,cb,lastAction ) VALUES ('ayoub' ,'ghammaz' ,TO_DATE('06/02/1998', 'DD/MM/YYYY') ,'6 rue'     , 'masculin'  ,1111 , 111111111111,'res' ) ;
INSERT INTO Abonnes(nom ,prenom ,dateNais ,adresse ,sexe,codeSecret,cb,lastAction ) VALUES ('regis' ,'orand'   ,TO_DATE('11/06/1996', 'DD/MM/YYYY') ,'2 avenue'  , 'masculin'  ,2222 , 111111111110,'res' ) ;
INSERT INTO Abonnes(nom ,prenom ,dateNais ,adresse ,sexe,codeSecret,cb,lastAction ) VALUES ('phan'  ,'thang'   ,TO_DATE('05/12/1998', 'DD/MM/YYYY') ,'18 rue'    , 'masculin'  ,3333 , 111111011111,'loc' ) ;
INSERT INTO Abonnes(nom ,prenom ,dateNais ,adresse ,sexe,codeSecret,cb,lastAction ) VALUES ('adam'  ,'madide'  ,TO_DATE('11/11/2011', 'DD/MM/YYYY') ,'11 rue'    , 'masculin'  ,4444 , 101101011111,'loc' ) ;
INSERT INTO Abonnes(nom ,prenom ,dateNais ,adresse ,sexe,codeSecret,cb            ) VALUES ('alizee','orand'   ,TO_DATE('11/12/1997', 'DD/MM/YYYY') ,'2 avenue'  , 'feminin'   ,5555 , 111111111110 ) ;
INSERT INTO Abonnes(nom ,prenom ,dateNais ,adresse ,sexe,codeSecret,cb            ) VALUES ('mike'  ,'tayson'  ,TO_DATE('10/04/1968', 'DD/MM/YYYY') ,'1 temple'  , 'masculin'  ,6666 , 101111111111 ) ;
INSERT INTO Abonnes(nom ,prenom ,dateNais ,adresse ,sexe,codeSecret,cb            ) VALUES ('omer'  ,'simpson' ,TO_DATE('01/01/2001', 'DD/MM/YYYY') ,'198 milieu', 'masculin'  ,7777 , 111111111000 ) ;
INSERT INTO Abonnes(nom ,prenom ,dateNais ,adresse ,sexe,codeSecret,cb            ) VALUES ('carli' ,'morgan'  ,TO_DATE('10/10/2010', 'DD/MM/YYYY') ,'15 lili'   , 'feminin'   ,8888 , 691111111111 ) ;
INSERT INTO Abonnes(nom ,prenom ,dateNais ,adresse ,sexe,codeSecret,cb            ) VALUES ('google' ,'miguel' ,TO_DATE('31/12/2000', 'DD/MM/YYYY') ,'1 million' , 'masculin'  ,9999 , 023411111111 ) ;


INSERT INTO NonAbonnes(cb ,codeSecret ) VALUES (123456789101112,1110);
INSERT INTO NonAbonnes(cb ,codeSecret ) VALUES (123456789101113,1101);
INSERT INTO NonAbonnes(cb ,codeSecret ) VALUES (123456789101114,1011);

insert into modeleVelo (idModeleVelo,libelleModele,type,prix) values (1,'faineant','electrique',10);
insert into modeleVelo (idModeleVelo,libelleModele,type,prix) values (2,'course','caddie',5);
insert into modeleVelo (idModeleVelo,libelleModele,type,prix) values (3,'normale','basique',1);
insert into modeleVelo (idModeleVelo,libelleModele,type,prix) values (4,'nasa','fusee',99);

insert into velo (etat,dateMiseService,dispo,idModeleVelo) values ('OK',TO_DATE('01/01/2010', 'DD/MM/YYYY'),'oui',1);
insert into velo (etat,dateMiseService,dispo,idModeleVelo) values ('OK',TO_DATE('02/01/2010', 'DD/MM/YYYY'),'non',1);
insert into velo (etat,dateMiseService,dispo,idModeleVelo) values ('OK',TO_DATE('03/01/2010', 'DD/MM/YYYY'),'oui',1);
insert into velo (etat,dateMiseService,dispo,idModeleVelo) values ('OK',TO_DATE('04/01/2010', 'DD/MM/YYYY'),'oui',1);
insert into velo (etat,dateMiseService,dispo,idModeleVelo) values ('OK',TO_DATE('05/01/2010', 'DD/MM/YYYY'),'oui',1);


insert into velo (etat,dateMiseService,dispo,idModeleVelo) values ('OK',TO_DATE('15/01/2010', 'DD/MM/YYYY'),'oui',2);
insert into velo (etat,dateMiseService,dispo,idModeleVelo) values ('OK',TO_DATE('16/01/2000', 'DD/MM/YYYY'),'oui',2);
insert into velo (etat,dateMiseService,dispo,idModeleVelo) values ('OK',TO_DATE('17/01/2000', 'DD/MM/YYYY'),'oui',2);
insert into velo (etat,dateMiseService,dispo,idModeleVelo) values ('OK',TO_DATE('18/01/2000', 'DD/MM/YYYY'),'oui',2);
insert into velo (etat,dateMiseService,dispo,idModeleVelo) values ('OK',TO_DATE('19/01/2000', 'DD/MM/YYYY'),'non',2);

insert into velo (etat,dateMiseService,dispo,idModeleVelo) values ('OK',TO_DATE('31/12/2014', 'DD/MM/YYYY'),'oui',3);
insert into velo (etat,dateMiseService,dispo,idModeleVelo) values ('OK',TO_DATE('31/12/2014', 'DD/MM/YYYY'),'oui',3);
insert into velo (etat,dateMiseService,dispo,idModeleVelo) values ('HS',TO_DATE('31/12/2014', 'DD/MM/YYYY'),'oui',3); 
-- 
insert into velo (etat,dateMiseService,dispo,idModeleVelo) values ('OK',TO_DATE('31/12/2014', 'DD/MM/YYYY'),'oui',3);
insert into velo (etat,dateMiseService,dispo,idModeleVelo) values ('OK',TO_DATE('31/12/2014', 'DD/MM/YYYY'),'oui',3);
insert into velo (etat,dateMiseService,dispo,idModeleVelo) values ('OK',TO_DATE('14/02/2014', 'DD/MM/YYYY'),'oui',4);
insert into velo (etat,dateMiseService,dispo,idModeleVelo) values ('OK',TO_DATE('14/02/2014', 'DD/MM/YYYY'),'oui',4);
insert into velo (etat,dateMiseService,dispo,idModeleVelo) values ('OK',TO_DATE('14/02/2014', 'DD/MM/YYYY'),'non',4);




insert into Facturation (clientAbonnes ,clientNonAbonnes, prix, idVelo) values (1   ,null,32,1);
insert into Facturation (clientAbonnes ,clientNonAbonnes, prix, idVelo) values (2   ,null,32,2);
insert into Facturation (clientAbonnes ,clientNonAbonnes, prix, idVelo) values (null,2   ,99,17);

insert into station (adresse,nbBornette,idplage) values ('centreVille',5,1);
insert into station (adresse,nbBornette,idplage) values ('campus',5,4);
insert into station (adresse,nbBornette,idplage) values ('gare',5,7);
insert into station (adresse,nbBornette,idplage) values ('ville',5,9);

insert into bornette (idBornette,idStation,etat,idVelo) values ( 1,1,'OK',1);
insert into bornette (idBornette,idStation,etat,idVelo) values ( 2,1,'OK',null);
insert into bornette (idBornette,idStation,etat,idVelo) values ( 3,1,'OK',3);
insert into bornette (idBornette,idStation,etat,idVelo) values ( 4,1,'OK',4);
insert into bornette (idBornette,idStation,etat,idVelo) values ( 5,1,'OK',5);
insert into bornette (idBornette,idStation,etat,idVelo) values ( 6,2,'OK',6);
insert into bornette (idBornette,idStation,etat,idVelo) values ( 7,2,'OK',7);
insert into bornette (idBornette,idStation,etat,idVelo) values ( 8,2,'OK',8);
insert into bornette (idBornette,idStation,etat,idVelo) values ( 9,2,'OK',9);
insert into bornette (idBornette,idStation,etat,idVelo) values (10,2,'OK',null);
insert into bornette (idBornette,idStation,etat,idVelo) values (11,3,'OK',11);
insert into bornette (idBornette,idStation,etat,idVelo) values (12,3,'OK',12);
insert into bornette (idBornette,idStation,etat,idVelo) values (13,3,'OK',13);
insert into bornette (idBornette,idStation,etat,idVelo) values (14,3,'OK',14);
insert into bornette (idBornette,idStation,etat,idVelo) values (15,3,'OK',15);
insert into bornette (idBornette,idStation,etat,idVelo) values (16,4,'OK',16);
insert into bornette (idBornette,idStation,etat,idVelo) values (17,4,'OK',17);
insert into bornette (idBornette,idStation,etat,idVelo) values (18,4,'OK',null);
insert into bornette (idBornette,idStation,etat,idVelo) values (19,4,'OK',null);
insert into bornette (idBornette,idStation,etat,idVelo) values (20,4,'OK',null);



insert into Reservations(idReservation ,idClient ,idVelo ,station ,renduVelo ,dateDebut,dateDebutJour) values (1,1,1,1,'oui',TO_DATE('08:38:16','HH24:MI:SS'),TO_DATE('15/02/2019', 'DD/MM/YYYY')) ;
insert into Reservations(idReservation ,idClient ,idVelo ,station ,renduVelo ,dateDebut,dateDebutJour) values (2,2,2,2,'non',TO_DATE('09:30:16','HH24:MI:SS'),TO_DATE('26/03/2019', 'DD/MM/YYYY')) ;
insert into Reservations(idReservation ,idClient ,idVelo ,station ,renduVelo ,dateDebut,dateDebutJour) values (3,3,3,3,'oui',TO_DATE('05:11:49','HH24:MI:SS'),TO_DATE('14/02/2019', 'DD/MM/YYYY')) ;


insert into Location (stationDebut, dateDebut, clientAbonnes , clientNonAbonnes , velo, renduVelo) values (4 ,TO_DATE('11:26:16','HH24:MI:SS'),null,2   ,18,'non');
insert into Location (stationDebut, dateDebut, clientAbonnes , clientNonAbonnes , velo, renduVelo) values (2 ,TO_DATE('06:08:16','HH24:MI:SS'),4   ,null,10,'non');
insert into Location (stationDebut, dateDebut, clientAbonnes , clientNonAbonnes , velo, renduVelo) values (3 ,TO_DATE('18:40:16','HH24:MI:SS'),5   ,null,15,'oui');

select * from Bornette;
select * from Station;
select IDPLAGEHORAIRE,to_char(dateDebut,'HH24:MI:SS') as dateDebut,to_char(DATEFIN,'HH24:MI:SS') as DATEFIN,PRIME from plagehoraire;
select * from Velo;
select * from ModeleVelo;
select * from Abonnes;
select * from NonAbonnes;
select idReservation ,idClient ,idVelo ,station ,renduVelo ,to_char(dateDebut,'HH24:MI:SS') as dateDebut,to_char(dateDebutJour,'DD/MM/YYYY') as dateDebutJour  from Reservations;
select * from Facturation;
select idLocation ,stationDebut , to_char(dateDebut,'HH24:MI:SS') as dateDebut, clientAbonnes, clientNonAbonnes, velo ,renduVelo from Location;
                                       
--SELECt idvelo, idclient, to_char(dateD,'DD-MM-YYYY HH24:MI:SS') as dateD,to_char(dateF,'DD-MM-YYYY HH24:MI:SS') as dateF, cout, codeSecret, numbornetteD, numbornetteA FROM LesLocations;


-- demander velo ok ou hs

-- rendre velo avant 3 min et declaration etat HS pas besoin de payer


--Ajouter insert Location  et facturation


--  Abonnes  cb verifier

commit;

