/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestApplication;

import static applications.AppliClient.conn;
import static applications.AppliClient.st;
import client.Abonnee;
import client.NonAbonnee;
import enumerations.Sexe;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashSet;
import renduVelo.rendu;
import reservation_Location.Location;
import reservation_Location.Reservation;

/**
 *
 * @author ayoub22
 */
public class TestAppClient {

    /**
     * @param args the command line arguments
     */
    static final String CONN_URL = "jdbc:oracle:thin:@im2ag-oracle.e.ujf-grenoble.fr:1521:im2ag";

    static final String USER = "ghammaza";
    static final String PASSWD = "j3ib6Sm3VY";
    public static Connection conn;
    public static Statement st;

    /**
     * **************************** test ajouter
     * abonnee*********************************************
     */
    private static boolean testAddAbonne() throws SQLException {
        int avant = 0;
        int apres = 0;
        System.out.println("--------------------- test 1 : ajout abonnee -------------------------");
        ResultSet rs1 = st.executeQuery("select count(*) from abonnes");

        while (rs1.next()) {

            avant = rs1.getInt(1);
            System.out.println("il esxiste " + avant + " abonnees");
        }

        Abonnee nB = new Abonnee(conn, "test", "test", "12/12/2012", Sexe.F, "imag", 1515151515);
        ResultSet rs2 = st.executeQuery("select count(*) from abonnes");

        while (rs2.next()) {

            apres = rs2.getInt(1);
            System.out.println("maintenant il en existe " + apres);
        }

        ResultSet rs3 = st.executeQuery("select nom,prenom from abonnes where nom='test'");

        while (rs3.next()) {

            System.out.println("nouveau abonne : nom" + rs3.getString(1) + " prenom" + rs3.getString(2));
        }
        System.out.println(apres == avant + 1);
        System.out.println("---------------------------------------------------------------------");

        return apres == avant + 1;
    }

    /**
     * ********************* test 2 : add non abonnee
     * *******************************************
     */
    private static boolean testNonAddAbonne() throws SQLException {
        int avant = 0;
        int apres = 0;
        System.out.println("--------------------- test 2 : ajout non abonnee -------------------------");
        ResultSet rs1 = st.executeQuery("select count(*) from nonAbonnes");

        while (rs1.next()) {

            avant = rs1.getInt(1);
            System.out.println("il esxiste " + avant + "non  abonnees");
        }

        NonAbonnee nonAb = new NonAbonnee(conn, 1111111111);
        ResultSet rs2 = st.executeQuery("select count(*) from nonAbonnes");

        while (rs2.next()) {

            apres = rs2.getInt(1);
            System.out.println("maintenant il en existe " + apres);
        }

        System.out.println(apres == avant + 1);
        System.out.println("---------------------------------------------------------------------");
        return apres == avant + 1;
    }

    /**
     * ******************************* test 3 :test location
     * *********************************************
     */
    private static boolean testLocation() throws SQLException {
        int avant = 0;
        int apres = 0;
        System.out.println("--------------------- test 3 : louer un velo -------------------------");
        ResultSet rs1 = st.executeQuery("select count(*) from location");

        while (rs1.next()) {

            avant = rs1.getInt(1);
            System.out.println("il esxiste " + avant + "locations");
        }

        ResultSet rs23 = st.executeQuery("select idvelo,dispo from velo where idvelo=1");

        while (rs23.next()) {

            System.out.println("le velo  " + rs23.getInt(1) + " dispo " + rs23.getString(2));
        }

        //
        Location lNonAb = new Location(conn, 0, 3, 1, 1);
        ResultSet rs2 = st.executeQuery("select count(*) from location");

        while (rs2.next()) {

            apres = rs2.getInt(1);
            System.out.println("maintenant il en existe " + apres);
        }
        ResultSet rs3 = st.executeQuery("select idvelo,dispo from velo where idvelo=1");

        while (rs3.next()) {

            System.out.println("le velo  " + rs3.getInt(1) + " dispo " + rs3.getString(2));
        }

        System.out.println(apres == avant + 1);
        System.out.println("---------------------------------------------------------------------");
        return apres == avant + 1;
    }

    /**
     * *************************************** test 4 :test reservation ********************************************
     */
    private static boolean testReservation() throws SQLException {
        int avant = 0;
        int apres = 0;
        System.out.println("--------------------- test 4 : reserver un velo -------------------------");
        ResultSet rs1 = st.executeQuery("select count(*) from reservations");

        while (rs1.next()) {

            avant = rs1.getInt(1);
            System.out.println("il esxiste " + avant + "locations");
        }

        ResultSet rs23 = st.executeQuery("select idvelo,dispo from velo where idvelo=3");

        while (rs23.next()) {

            System.out.println("le velo  " + rs23.getInt(1) + " dispo " + rs23.getString(2));
        }

        //
        Reservation r = new Reservation(st, conn, 5, 1, 4, 1, "08:00:00","12/12/2019");
        ResultSet rs2 = st.executeQuery("select count(*) from reservations");

        while (rs2.next()) {

            apres = rs2.getInt(1);
            System.out.println("maintenant il en existe " + apres);
        }
        ResultSet rs3 = st.executeQuery("select idvelo,dispo from velo where idvelo=3");

        while (rs3.next()) {

            System.out.println("le velo  " + rs3.getInt(1) + " dispo " + rs3.getString(2));
        }

        System.out.println(apres == avant + 1);
        System.out.println("---------------------------------------------------------------------");
        return apres == avant + 1;
    }
    /******************** test 5 : test rendu *********************************************************/
    private static void testRendu() throws SQLException{
        System.out.println("--------------------- test 5 : rendu d'un velo -------------------------");
        ResultSet rs1 = st.executeQuery("select velo  ,dispo , renduVelo from location natural join velo where CLIENTNONABONNES=2 and location.velo=velo.idvelo");

        while (rs1.next()) {
    
            System.out.println("le velo  "+rs1.getInt(1)+" est dispo : "+rs1.getString(2)+" est il rendu : "+rs1.getString(3));
        }
        rendu r = new rendu(conn,1 , 20, 2,false);
        System.out.println("apres avoir rendre le velo");
        ResultSet rs2 = st.executeQuery("select velo  ,dispo , renduVelo from location natural join velo where CLIENTNONABONNES=2 and location.velo=velo.idvelo");

        while (rs2.next()) {
    
            System.out.println("le velo  "+rs2.getInt(1)+" est dispo : "+rs2.getString(2)+" est il rendu : "+rs2.getString(3));
        }
       
   
    }
    
    
    /*********************test 6 : facturation**************************************************************************/

    public static void main(String[] args) throws SQLException {
        // TODO code application logic here
        // Enregistrement du driver Oracle
       
        try {

            // Enregistrement du driver Oracle
            System.out.print("Loading Oracle driver... ");
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
            System.out.println("loaded");
            // Etablissement de la connection
            System.out.print("Connecting to the database... ");
            conn = DriverManager.getConnection(CONN_URL, USER, PASSWD);
            System.out.println("connected");
            conn.setAutoCommit(true);
            st = conn.createStatement();
            Collection<Abonnee> abn = new HashSet<Abonnee>();
            abn = new HashSet<Abonnee>();
            System.out.println("Bonjour sur votre application velo  ...");

            // traitement d'exception
            testAddAbonne();
            testNonAddAbonne();
            testLocation();
            testReservation();
            testRendu();
             conn.close();

            System.out.println("bye.");

            // traitement d'exception
        } catch (SQLException e) {
            System.err.println("failed");
            System.out.println("Affichage de la pile d'erreur");
            e.printStackTrace(System.err);
            System.out.println("Affichage du message d'erreur");
            System.out.println(e.getMessage());
            System.out.println("Affichage du code d'erreur");
            System.out.println(e.getErrorCode());

        }

        } 

}
