package velo;
import java.sql.Date;

import enumerations.Etat;

public class Velo {
	private static Integer idVelo;
	public Etat etat ;
	public Date dateMiseService ; 
	public ModelVelo model;
	
	public Velo(Etat etat , Date dateMiseService ,ModelVelo model) {
		this.etat=etat;
		this.dateMiseService=dateMiseService;
		this.model=model;
		idVelo++;
	}
	
}
