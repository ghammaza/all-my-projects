package client;

import java.sql.*;

public class NonAbonnee extends Client{
	//private static int idClient;
	private int codeSecret;
        private static int idclasse;
	
	public NonAbonnee(Connection conn ,long cb) throws SQLException {
		super(cb);
		this.codeSecret=hashCodeC();
		Statement st = conn.createStatement();
		st.executeUpdate("insert into NonAbonnes (cb ,codeSecret ) Values ("+cb+","+this.codeSecret+")");
		st.close();
                idclasse++;
	}
	public int hashCodeC(){ 
		int res = Long.toString(pointHasard(12, 9999)+idclasse).hashCode();
		return res;
	}

    public int getCodeSecret() {
        return Math.abs(codeSecret);
    }
    private  int pointHasard(int min, int max) {// generer des entiers 
	    return min + (int)Math.floor((max - min + 1) * Math.random());
	      }
     
}
