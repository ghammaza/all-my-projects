package client;
import java.sql.*;
//import java.io.*;
import enumerations.Sexe;

public class Abonnee extends Client implements Comparable<Abonnee> {

	//private static int idClient;	
	private int codeSecret;
	public String nom;
	public String prenom;
	public String dateNaissance;
	public Sexe sexe;
	public String adresse;
	
	public Abonnee(Connection conn ,String nom ,String prenom ,String dateNaissance ,Sexe sexe , String adresse , long cb ) throws SQLException {
		super(cb);
		this.nom=nom;
		this.prenom=prenom;
		this.adresse= adresse;
		this.sexe=sexe;
		this.dateNaissance=dateNaissance;
		this.codeSecret= hashCodeC();
                 String s = sexe.getSexe();
		 CallableStatement st1 = conn.prepareCall ("{call addClientAb (? , ? ,? ,? ,? ,? ,? )}");
		 	st1.setString (1, this.nom);  
		    st1.setString (2, this.prenom);        // The raise argument is the third ?
		    st1.setString (3, this.dateNaissance); 
		    st1.setString (4, adresse); 
		    st1.setString (5,s );
		    st1.setFloat(6, cb);
		    st1.setInt(7, this.codeSecret);
		    // Do the raise
		    st1.execute ();
		  
		    // Close the statement
		    st1.close();
		
	}
	
	public int hashCodeC(){
		int res = nom.hashCode();
		res+=prenom.hashCode();
		return res;
	}

	@Override
	public int compareTo(Abonnee arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

    public int getCodeSecret() {
        return Math.abs(codeSecret);
    }
	
	
}
