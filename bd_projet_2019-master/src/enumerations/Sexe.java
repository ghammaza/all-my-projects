package enumerations;

public enum Sexe {
	M("masculin"),
	F("feminin");
	private Sexe (String sexe) {
		this.sexe=sexe;
	}
	private String sexe;
	public String getSexe() {
		return sexe;
	}
	
}
