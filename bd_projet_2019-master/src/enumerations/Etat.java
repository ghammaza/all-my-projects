package enumerations;

public enum Etat {
	OK("OK"),
	HS("HS");
	
	private Etat(String etat) {
		this.etat=etat;
	}
	private String etat;
	public String getEtat() {
		return etat;
	}
}
