package enumerations;

public enum Prime {
	vPlus("vPlus"),
	vMoins("vMoins"),
	vNull("vNull");
	
	private Prime(String prime) {
		this.prime=prime;
	}
	private String prime;
	public String getString() {
		return prime;
	}
}
