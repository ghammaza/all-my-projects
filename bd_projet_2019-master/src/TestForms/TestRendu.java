/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestForms;

import Forms.FormRondu;
import client.Abonnee;
import java.sql.*;
import java.text.ParseException;
import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author ghammaza
 */
public class TestRendu {
 static final String CONN_URL = "jdbc:oracle:thin:@im2ag-oracle.e.ujf-grenoble.fr:1521:im2ag";

    static final String USER = "ghammaza";
    static final String PASSWD = "j3ib6Sm3VY";
    public static Connection conn;
    public static Statement st;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ParseException {
        // TODO code application logic here
         try {

            // Enregistrement du driver Oracle
            System.out.print("Loading Oracle driver... ");
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
            System.out.println("loaded");
            // Etablissement de la connection
            System.out.print("Connecting to the database... ");
            conn = DriverManager.getConnection(CONN_URL, USER, PASSWD);
            System.out.println("connected");
            conn.setAutoCommit(true);

            System.out.println("Bonjour sur votre application velo  ...");
            st = conn.createStatement();
            FormRondu r = new FormRondu(conn,1101,"nonAbn");
           // r.getBornetteDispo(4);
                  
        conn.close();

            System.out.println("bye.");

            // traitement d'exception
        } catch (SQLException e) {
            System.err.println("failed");
            System.out.println("Affichage de la pile d'erreur");
            e.printStackTrace(System.err);
            System.out.println("Affichage du message d'erreur");
            System.out.println(e.getMessage());
            System.out.println("Affichage du code d'erreur");
            System.out.println(e.getErrorCode());

        }
    }
    
}
