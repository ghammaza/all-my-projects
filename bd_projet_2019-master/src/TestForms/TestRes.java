/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestForms;

import Forms.FormRes;
import static applications.AppliClient.conn;
import client.Abonnee;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import lectureClavier.LectureClavier;
import static oracle.jdbc.replay.OracleDataSource.USER;

/**
 *
 * @author ghammaza
 */
public class TestRes {

    static final String CONN_URL = "jdbc:oracle:thin:@im2ag-oracle.e.ujf-grenoble.fr:1521:im2ag";

    static final String USER = "ghammaza";
    static final String PASSWD = "j3ib6Sm3VY";
    public static Connection conn;
    public static Statement st;

    /**
     * @param args the command line arguments
     * @throws java.sql.SQLException
     * @throws java.text.ParseException
     */
    
    private static boolean verifFormDate(String date){
         String jourStr=date.charAt(0)+""+date.charAt(1);
         int jourInt =Integer.parseInt(jourStr);
         
         String moisStr=date.charAt(3)+""+date.charAt(4);
         int moisInt =Integer.parseInt(moisStr);
         
         String anneStr=date.charAt(6)+""+date.charAt(7)+""+date.charAt(8)+""+date.charAt(9);
         int anneeInt =Integer.parseInt(anneStr);
         
        return 1<=jourInt && jourInt<=31 && 1<=moisInt && moisInt<=12 && anneeInt>2018;
       
     }
    
    private static boolean verifHeure(String heure){
    int heureInt =Integer.parseInt(heure.charAt(0)+""+heure.charAt(1));
    int minute=Integer.parseInt(heure.charAt(3)+""+heure.charAt(4));
    
    return 0<=heureInt && heureInt<=23 && 0<=minute && minute<=59;
    }
    public static void main(String[] args) throws SQLException, ParseException {
        // TODO code application logic here
          try {

            // Enregistrement du driver Oracle
            System.out.print("Loading Oracle driver... ");
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
            System.out.println("loaded");
            // Etablissement de la connection
            System.out.print("Connecting to the database... ");
            conn = DriverManager.getConnection(CONN_URL, USER, PASSWD);
            System.out.println("connected");
            conn.setAutoCommit(true);
            Collection<Abonnee> abn = new HashSet<Abonnee>();
            abn = new HashSet<Abonnee>();
            System.out.println("Bonjour sur votre application velo  ...");
            st = conn.createStatement();
        FormRes r = new FormRes(conn,1111);
        
        conn.close();

            System.out.println("bye.");

            // traitement d'exception
        } catch (SQLException e) {
            System.err.println("failed");
            System.out.println("Affichage de la pile d'erreur");
            e.printStackTrace(System.err);
            System.out.println("Affichage du message d'erreur");
            System.out.println(e.getMessage());
            System.out.println("Affichage du code d'erreur");
            System.out.println(e.getErrorCode());

        }
       /* SimpleDateFormat dateDebForm = new SimpleDateFormat("dd/MM/yyyy");// forme de date reservation
        System.out.println("Veillez saisir la date de reservation (JJ/MM/AAAA)");
        String dateInString =LectureClavier.lireChaine();
         Date date = dateDebForm.parse(dateInString);
       String datDeb=  dateDebForm.format(date);
       
        while(!(verifFormDate(datDeb))){
            System.out.println("date non valide , veillez saisie la date selon le modele JJ/MM/AAAA");
            dateInString =LectureClavier.lireChaine();
        }
        
        System.out.println("A quelle heure voulez vous reserver ?(HH:MM)");
        String heureMin=LectureClavier.lireChaine();
        while(!(verifHeure(heureMin))){
             System.out.println("heure et minute non valide , veillez saisie la date selon le modele HH:MM");
            heureMin=LectureClavier.lireChaine();
        
        }
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        String lastDate=datDeb+" "+heureMin;
        
        Date dateL = dateDebForm.parse(lastDate);
       String  lastDateL=  dateDebForm.format(dateL);
        
       DateFormat format1 = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date date1 = new Date();
        
        System.out.println(date1.before(dateL));*/
        //System.out.println(verifFormDate("12/02/2019"));
        //System.out.println(verifFormDate("blabla2"));
        //2FormRes r = new FormRes(conn,1111);
    }
    

}
