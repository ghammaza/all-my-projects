package station;
import enumerations.Prime;
import java.sql.Date;

public class PlageHoraire {
	public Date horaireDeb;
	public Date horaireFin;
	public Prime prime;
	
	public PlageHoraire(Date horaireDeb , Date horaireFin ,Prime prime ) {
		this.horaireDeb=horaireDeb;
		this.horaireFin=horaireFin;
		this.prime=prime;
	}
        public Prime getPrime(){
        return prime;
        }
}
