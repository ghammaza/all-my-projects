package station;
import java.sql.Connection;
import java.sql.Statement;

import enumerations.Etat;
import velo.Velo;

public class Bornettes {
	private static Integer idBornette;
	public Etat etat;
	public Velo velo;
	
	public Bornettes(Connection conn ,Statement st,Etat etat , Velo velo ) {
		this.etat=etat;
		this.velo=velo;
		idBornette++;
	}

}
