package station;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;

public class Station {
	//private Connection conn;
	//private static final String PASSWD = null;
	//private static final String USER = null;
	//private static final String CONN_URL = null;
	private static Integer idStation;
	public String adresse ;
	public Integer nbBornettes;
	public ArrayList<Bornettes> bornettes;
//	public PlageHoraire plage;
	public String plage;
												
	public Station(Connection conn ,Statement st, Integer idStation1, Integer idBornette1, Integer idVelo1, String newAdresse , Integer nbBornette1, String plage1, String Mess) throws SQLException {
		this.adresse=newAdresse;
		this.nbBornettes=nbBornette1;
		//this.bornettes= new ArrayList<Bornettes>();
		this.plage=plage1;
		
		st = conn.createStatement();
		/*st.execute("create or replace procedure addStation (idStation1 IN INTEGER,idBornette1 IN INTEGER,idVelo1 IN NUMBER,newAdresse IN VARCHAR2 , nbBornette1 IN INTEGER , plage1 IN VARCHAR2, Mess OUT INTEGER ) is station_exist NUMBER:=0; \n"+ 
				" Mess:=0"+
				"BEGIN \n"+
				"SELECT count(*) \n"+ 
				    "INTO station_exist \n"+
				    "FROM Station \n"+
					"WHERE adresse=newAdresse; \n"+
				"IF station_exist=0 then INSERT INTO Station (idStation ,idBornette ,idVelo ,adresse ,nbBornette,plage) VALUES (idStation1 ,idBornette1 ,idVelo1 ,newAdresse  , nbBornette1  ,plage1) ; \n"+
				"END IF; \n "+
				 "EXCEPTION \n"+
		   		 "WHEN NO_DATA_FOUND \n"+
					" Mess := -1 \n "+
				"END;");*/
		
		
		
		/*
		
		
		
		create or replace procedure addStation (idStation1 IN INTEGER,idBornette1 IN INTEGER,idVelo1 IN INTEGER,newAdresse IN VARCHAR2 , nbBornette1 IN INTEGER , plage1 IN VARCHAR2 )
		 is station_exist NUMBER:=0; 
				
				BEGIN 
				SELECT count(*) 
				    INTO station_exist
				    FROM Station 
					WHERE adresse=newAdresse; 
				IF station_exist=0 then INSERT INTO Station (idStation ,idBornette ,idVelo ,adresse ,nbBornette,plage) VALUES (idStation1 ,idBornette1 ,idVelo1 ,newAdresse  , nbBornette1  ,plage1) ; 
				
				ELSE  DBMS_OUTPUT.PUT_LINE('La station existe');  
				END IF;
				END;
/



			Create trigger TestSation BEFORE insert on Station
			FOR  each row
			Declare
			abnum NUMBER:=0;
			Begin
			select idStation into abnum from Station where adresse=:new.adresse;  
			if abnum<>0 then raise_application_error(-20100,' La sation existe deja !');
			end if;
			Exception when NO_DATA_FOUND then DBMS_OUTPUT.PUT_LINE ('OK');
			End;
			/
		
		
		*/
		
		
		
		st.close();
		CallableStatement st1 = conn.prepareCall ("{call addStation (?,?,?,?,?,?,? )}");
		st1.registerOutParameter(7, Types.VARCHAR);
		
		 	st1.setInt(1, idStation1);  
		    st1.setInt(2, idBornette1);  
		    st1.setInt(3, idVelo1); 
		    st1.setString (4, newAdresse);  
		    st1.setInt(5, nbBornette1);  
		    st1.setString (6, plage1); 
		   
		    st1.execute ();
		    int msg = st1.getInt(7);
		    
		    if(msg < 0) {
		    	//affichage du message d'erreur
		    }
		  
		    st1.close();
		
	}
	
}
