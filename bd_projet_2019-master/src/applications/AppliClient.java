package applications;

import Forms.FormAddAb;
import Forms.FormLoc;
import Forms.FormNonAb;
import Forms.FormRes;
import Forms.FormRondu;
import static TestForms.TestRendu.conn;
import client.*;
import enumerations.*;
import java.sql.*;
import java.text.ParseException;
import java.util.Collection;
import java.util.HashSet;
import lectureClavier.LectureClavier;

public class AppliClient {

    static final String CONN_URL = "jdbc:oracle:thin:@im2ag-oracle.e.ujf-grenoble.fr:1521:im2ag";

    static final String USER = "ghammaza";
    static final String PASSWD = "j3ib6Sm3VY";
    public static Connection conn;
    public static Statement st;
    public static Collection<Abonnee> abn;
    private static NonAbonnee nonAb;

    private String[] getInfoAbonnee(int codeSec) throws SQLException {
        String[] info = new String[4];// case 0 : nom ; case 1 : sexe; case 2 :cb ; id?
        ResultSet rs = st.executeQuery("select id, nom , sexe , cb from Abonnes where codeSecret=" + codeSec + ";");
        while (rs.next()) {
            info[0] = Long.toString(rs.getInt(1));
            info[1] = rs.getString(2);
            info[2] = rs.getString(3);
            info[3] = Long.toString(rs.getInt(4));

        }
        st.close();
        return info;

    }

    private String[] getInfoAction(int codeSecret) throws SQLException {// pour les abonnes : retour de loc et res
        String[] infoAction = new String[4];
        int idAbonne = 0;
        ResultSet rs1 = st.executeQuery("select lastAction,idAbonne from Abonnee where codeSecret =" + codeSecret);// voir la derniere action du client : si il a fait une location ou une reservation
        while (rs1.next()) {
            infoAction[0] = rs1.getString(1);
            idAbonne = rs1.getInt(2);
            infoAction[1] = Long.toString(idAbonne);

        }
        if (infoAction[0].equals("loc")) {
            ResultSet rs2 = st.executeQuery("select idvelo , dateDebut from location natural join Velo where clientAbonnes =" + idAbonne + " and dispo = 'non'");// voir la derniere action du client : si il a fait une location ou une reservation

            while (rs2.next()) {

                infoAction[2] = Long.toString(rs2.getInt(1));
                infoAction[3] = rs2.getString(2);
            }
        } else {
            ResultSet rs3 = st.executeQuery("select idvelo , dateDebut from Reservations natural join Velo  where clientAbonnes =" + idAbonne + " and dispo = 'non'");// voir la derniere action du client : si il a fait une location ou une reservation

            while (rs3.next()) {
                infoAction[2] = Long.toString(rs3.getInt(1));
                infoAction[3] = rs3.getString(2);
            }
        }
        return infoAction;
    }

    private static boolean dejaRes(int codeS) throws SQLException {
        int c = 0;
        ResultSet rs3 = st.executeQuery("select count(*) from Location natural join Abonnes where location.CLIENTABONNES =abonnes.idAbonne and renduVelo ='non' and codeSecret=" + codeS);
        while (rs3.next()) {
            c = rs3.getInt(1);
        }

        return c == 0;
    }

    private static boolean dejaLoc(int codeS) throws SQLException {
        int c = 0 ;
        ResultSet rs3 = st.executeQuery("select count(*) from Location natural join NonAbonnes where location.CLIENTNONABONNES =NonAbonnes.IDNONABONNE and renduVelo ='non' and codeSecret=" + codeS);
        while (rs3.next()) {
            c = rs3.getInt(1);
        }

        return c == 0;

    }

    /*
	 getinfoloc
	 select action from Abonnee where code secret =....
	 if action = loc : select idvelo , dateDebut from location where idClint = ... and dispo = non
	 else meme chose mais pour reservation
	 return idclient idvelo datedeb ,stations
     */
    // pour reservation : requette qui montre le moodel de velo dispo
    /**
     * *********** auth : verifie si le client existe dans la bd
     * *******************************
     */
    private static boolean auth(int codeSecret, String client) throws SQLException {
        boolean isAuth = false;
        /**
         * *********partir abonnee**********
         */
        if (client.equals("Abonnes")) {// verifier si il y a un abonne correspond a code secret 
            ResultSet rs = st.executeQuery("select count(*) from Abonnes where codeSecret=" + codeSecret);
            int count = 0;
            while (rs.next()) {
                count = rs.getInt(1);
            }
            if (count != 0) {
                isAuth = true;
            }
            return isAuth;
        } /**
         * ****** partir non abonne***********
         */
        else {
            ResultSet rs = st.executeQuery("select count(*) from NonAbonnes where codeSecret=" + codeSecret);
            int count = 0;
            while (rs.next()) {
                count = rs.getInt(1);
            }
            if (count != 0) {
                isAuth = true;
            }
            return isAuth;
        }

    }

    public static void main(String[] args) throws ParseException {
        // TODO Auto-generated method stub

        try {

            // Enregistrement du driver Oracle
            System.out.print("Loading Oracle driver... ");
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
            System.out.println("loaded");
            // Etablissement de la connection
            System.out.print("Connecting to the database... ");
            conn = DriverManager.getConnection(CONN_URL, USER, PASSWD);
            System.out.println("connected");
            conn.setAutoCommit(true);
            st = conn.createStatement();
            Collection<Abonnee> abn = new HashSet<Abonnee>();
            abn = new HashSet<Abonnee>();
            System.out.println("Bonjour sur votre application velo  ...");
            /**
             * ************************** variables utilisees dans la
             * boucle*************************
             */
            boolean choix = true;
            boolean reduNAb = false;
            int codeS = 0;
            int codeSNonab = 0;
            int choix1 = -1;
            boolean boucle1 = true;
            boolean boucle2 = true;
            boolean identAb = false;
            boolean identNoAb = false;
            int insc;// variable d inscription pour les non abonnees
            String[] inf;
            do {

                /**
                 * ********************* etape une : boucle login incr
                 * **********************************************
                 */
                while (boucle1) {// premier meunu

                    System.out.println("Vous etes abonne a nos services ?");
                    System.out.println("1) oui                    2) non ");
                    choix1 = LectureClavier.lireEntier("1-->");
                    switch (choix1) {// abonne ou pas 
                        case 1:
                            System.out.println("Veuillez saisir votre numero secret ");
                            codeS = LectureClavier.lireEntier("--> votre code secret est : ");
                            // verifier si le code existe , si oui renvoyer l instance abonne 
                            //sinon demander de l inscrir   

                            // appel a la lethode getInfoAbonnee
                            // je l ai pas utiliser encore car je pense chercher dans la collection ou de faire une requette sql
                            if (auth(codeS, "Abonnes")) {
                                System.out.println("bonjour x");
                                identAb = true;// entrer dans la boucle des choix
                                boucle1 = false;// pour ne plus revenir sur cette boucle
                                boucle2 = false;//pour ne pas entrer a la boucle suivante 
                                break;
                            } else {
                                int i = 0;
                                while (!(auth(codeS, "Abonnes")) && i < 3) {
                                    System.out.println("On vous trouve pas ");

                                    codeS = LectureClavier.lireEntier("--> votre code secret est : ");
                                    i++;
                                }
                            }

                        case 2:
                            System.out.println("Que voulez vous faire ? ?");
                            System.out.println("1) s'identifier                    2) S'inscrir             3)ne pas s'inscrir");
                            // pour aller a la boucle suivante
                            boucle1 = false;// pour ne plus revenir a cette boucle
                            break;

                        default:
                            System.out.println("ce choix n existe pas");
                            break;
                    }
                }
                /**
                 * *******************************************************************************************
                 */

                /**
                 * ***************** etape deux : on cas de nouvelle
                 * inscription *****************************
                 */
                while (boucle2) {
                    insc = LectureClavier.lireEntier("-->");
                    switch (insc) {
                        case 1:
                            codeSNonab = LectureClavier.lireEntier("--> votre code secret est : ");

                            if (auth(codeSNonab, "nonAbonnes")) {
                                System.out.println("bonjour x");
                                identAb = false;// entrer dans la boucle des choix
                                boucle1 = false;// pour ne plus revenir sur cette boucle
                                boucle2 = false;//pour ne pas entrer a la boucle suivante 
                                break;
                            } else {
                                int i = 0;
                                while (!(auth(codeSNonab, "nonAbonnes")) && i < 3) {
                                    System.out.println("On vous trouve pas ");
                                    codeSNonab = LectureClavier.lireEntier("--> votre code secret est : ");
                                    i++;
                                }

                            }

                        case 2:// a revoir !!
                            System.out.println("on commence l'inscribtion ? ");
                            int inscription = LectureClavier.lireEntier("1) oui            2) non");
                            if (inscription == 1) {
                                FormAddAb f = new FormAddAb();
                                codeS = f.getCode();
                                identAb = true;// entrer dans la boucle des choix
                                boucle1 = false;// pour ne plus revenir sur cette boucle
                                boucle2 = false;//pour ne pas entrer a la boucle suivante
                                break;
                            } else {
                                identAb = false;// entrer dans la boucle des choix
                                boucle1 = false;// pour ne plus revenir sur cette boucle
                                boucle2 = false;//pour ne pas entrer a la boucle suivante

                            }
                        case 3:
                            FormNonAb fNa = new FormNonAb();
                            codeSNonab = fNa.getCode();
                         
                            identNoAb = true;// boucle location non ab
                            boucle2 = false;
                            break;

                        default:
                            System.out.println("ce choix n existe pas");
                            break;
                    }
                }
                /**
                 * ******************************************************************************************
                 */
                // etape 3 : reservation et location
                /**
                 * ******************* etape 3 : reservation ou location
                 * *************************************
                 */
                if (identAb) {// chox pour les abonnes
                    boolean boucle3 = true;
                    while (boucle3) {
                        System.out.println("Voulez vous faire une reservation ou une location ou rendre votre velo ?");
                        int choixResLoc = LectureClavier.lireEntier("1)reserver               2)louer               3) rendu de velo");
                        switch (choixResLoc) {
                            case 1:
                                FormRes r = new FormRes(conn, codeS);
                                boucle3 = false;
                                break;
                            case 2:
                                if (dejaRes(codeS)) {
                                    FormLoc locab = new FormLoc(conn, codeS, "abn");
                                } else {
                                    System.out.println(" ");
                                    System.out.println("Vous avez une location non  rendu ! veillez rendre votre velo");
                                    System.out.println(" ");
                                    System.out.println(" ");
                                }
                                boucle3 = false;
                                break;
                            case 3:
                                //rendu
                                FormRondu r1 = new FormRondu(conn, codeS, "abn");
                                boucle3 = false;
                                break;
                            default:
                                System.out.println("Ce choix n'existe pas");
                                break;
                        }
                    }
                } else {

                    boolean boucle4 = true;
                    while (boucle4) {
                        System.out.println("Voulez vous faire une reservation ou une location ou rendre votre velo ?");
                        int choixResLoc = LectureClavier.lireEntier("1)louer               2) rendu de velo");
                        switch (choixResLoc) {
                            case 1:
                                if (dejaLoc(codeSNonab)) {
                                    FormLoc locab = new FormLoc(conn, codeSNonab, "nonAbn");
                                } else {
                                    System.out.println(" ");
                                    System.out.println("Vous avez une location non  rendu ! veillez rendre votre velo");
                                    System.out.println(" ");
                                    System.out.println(" ");
                                }

                                boucle4 = false;
                                break;
                            case 2:
                                FormRondu r = new FormRondu(conn, codeSNonab, "nonAbn");
                                boucle4 = false;
                                break;

                            default:
                                System.out.println("Ce choix n'existe pas");
                                break;
                        }
                    }

                }
                /**
                 * *******************************************************************************************
                 */
                System.out.println("Voulez vous continuer sur l application");
                boolean cont = true;

                while (cont) {
                    System.out.println("Voules vous continuer ?");
                    int contt = LectureClavier.lireEntier("1)oui                2)non");
                    switch (contt) {
                        case 1:
                            choix = true;
                            cont = false;
                            break;
                        case 2:
                            choix = false;
                            cont = false;
                            break;
                        default:
                            System.out.println("Choix indisponible");

                    }

                }
            } while (choix);

            conn.close();

            System.out.println("bye.");

            // traitement d'exception
        } catch (SQLException e) {
            System.err.println("failed");
            System.out.println("Affichage de la pile d'erreur");
            e.printStackTrace(System.err);
            System.out.println("Affichage du message d'erreur");
            System.out.println(e.getMessage());
            System.out.println("Affichage du code d'erreur");
            System.out.println(e.getErrorCode());

        }

    }

}
