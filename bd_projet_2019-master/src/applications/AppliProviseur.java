/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applications;

import static java.lang.String.format;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import static java.sql.Types.NULL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;


/**
 *
 * @author Phan Thanh Thang
 */
public class AppliProviseur {
    
    static final String CONN_URL = "jdbc:oracle:thin:@im2ag-oracle.e.ujf-grenoble.fr:1521:im2ag";

    static final String USER = "phant";
    static final String PASSWD = "Thang0512";
    public static Connection conn;
    public static Statement st;
    
    public AppliProviseur(){       
    }

    
    private void getNbVeloHorsService() throws SQLException{
        st = conn.createStatement();
        int velo = 0;
        String msg = "Le nombre des vélos qui sont en panne est: \n";
        String sql = "select count(distinct(idVelo)),idStation from Station Natural Join Bornette NAtural join (select idVelo from Velo where etat='HS' )   GROUP BY idStation";
        ResultSet rs = st.executeQuery(sql);
        while(rs.next()){      
            msg +="Le staion numéro "+ rs.getInt(2)+" a "+rs.getInt(1) +" vélos en panne. \n";
        }
        System.out.println(msg);
    }
    private void getNbVelo() throws SQLException{
        st = conn.createStatement();
        int velo = 0;
        String sql = "select count(distinct(idVelo)),idStation from Station Natural Join Bornette NAtural join (select idVelo from Velo)   GROUP BY idStation";
        String msg = "Le nombre des vélos dans chaque staion est: \n";
        ResultSet rs = st.executeQuery(sql);
        while(rs.next()){
            msg +="Le staion numéro "+ rs.getInt(2)+" a "+rs.getInt(1) +" vélos. \n";
        }
        System.out.println(msg);
    }
    
    private void getPlaceLibre() throws SQLException{
        st = conn.createStatement();
        int place = 0;
        String msg = "Le nombre des places libres dans chaque staion est: \n";
        String sql =" Select Count(idBornette),idStation from Station natural join Bornette where (idVelo is null) group by idStation";
        ResultSet rs = st.executeQuery(sql);
        while(rs.next()){
            msg +="Le staion numéro "+ rs.getInt(2)+" a "+rs.getInt(1) +" places libres. \n";
        }
        System.out.println(msg);
    }
    
    private int compareDate(String s, String r){
        int res ;
        int a =Integer.parseInt(""+s.charAt(0)+s.charAt(1));
        int b = Integer.parseInt(""+r.charAt(0)+r.charAt(1));
        if(a > b) res = 1;
        else if(a < b) res = -1;
        else{
            a =Integer.parseInt(""+s.charAt(3)+s.charAt(4));
            b = Integer.parseInt(""+r.charAt(3)+r.charAt(4));
            if(a > b) res = 1;
            else if(a < b) res = -1;
            else {
                a =Integer.parseInt(""+s.charAt(6)+s.charAt(7));
                b = Integer.parseInt(""+r.charAt(6)+r.charAt(7));
                if(a > b) res = 1;
                else if(a < b) res = -1;
                else res = 0;
            }
        }
        return res;
    }
    
    private void getInfoPlageHoraire() throws SQLException, ParseException{
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss") ;
        Calendar calendar = Calendar.getInstance();
        String sql = "select IDPLAGEHORAIRE,to_char(dateDebut,'HH24:MI:SS') as dateDebut,to_char(DATEFIN,'HH24:MI:SS') as DATEFIN,PRIME from plagehoraire";
        String msg ="La liste des horraires des primes est: \n";
        ResultSet rs = st.executeQuery(sql);
        String s = format.format(calendar.getTime());
        while(rs.next()){   
            String debut = rs.getString(2);
            String fin = rs.getString(3);
            if(compareDate(s, debut)>=0 && compareDate(s, fin)<=0){
                msg+= "La plage numéro "+rs.getInt(1)+ " est de " +debut + " à "+fin + " au "+rs.getString(4)+".\n";
            }
        }
        System.out.println(msg);
        
    }
    
    //select IDPLAGEHORAIRE,to_char(dateDebut,'HH24:MI:SS') as dateDebut,to_char(DATEFIN,'HH24:MI:SS') as DATEFIN,PRIME from plagehoraire where to_char(08:30:21,'HH24:MI:SS') between dateDebut AND dateFin
    
    private void modifierPlageHoraire(int idPlage, int idStation) throws SQLException{
        st = conn.createStatement();
        String sql = "UPDATE Station SET idPlage = "+idPlage+" WHERE idStation = "+idStation;
        ResultSet rs = st.executeQuery(sql);
    }
    
    
    public void menu() throws SQLException, ParseException{
        conn = DriverManager.getConnection(CONN_URL,USER,PASSWD);
        // Enregistrement du driver Oracle
        System.out.print("Loading Oracle driver... "); 
        DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());  	    

        // Etablissement de la connection
        conn = DriverManager.getConnection(CONN_URL,USER,PASSWD);
        st = conn.createStatement();
        conn.setAutoCommit(true);
        ArrayList<Integer> arr = new ArrayList<Integer>();
        ArrayList<Integer> arr1 = new ArrayList<Integer>();
        System.out.println("------------------------------Menu------------------------------");
        System.out.println("1. Affichage du nombre de vélos dans chaque station");
        System.out.println("2. Affichage du nombre de vélos en panne dans chaque station");
        System.out.println("3. Affichage du nombre de places libres dans chaque station");
        System.out.println("4. Affichage de la liste de horaires de prime dans chaque station");
        System.out.println("5. Modifiez les plages horaires");
        System.out.println("6. Quitter l'application");
        System.out.println("Veuillez choisir la fonction de l'application");
        Scanner sc = new Scanner(System.in);
        int choix = sc.nextInt();
        switch(choix){
            case 1:
                getNbVelo();
                conn.close(); 
                System.out.println("bye.");
                menu();
            case 2:
                getNbVeloHorsService();
                conn.close(); 
                System.out.println("bye.");
                menu();
            case 3:
                getPlaceLibre();
                conn.close(); 
                System.out.println("bye.");
                menu();
            case 4:
                getInfoPlageHoraire();
                conn.close(); 
                System.out.println("bye.");
                menu();
            case 5:
                SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss") ;
                Calendar calendar = Calendar.getInstance();
                
                String sql1 =" select idStation, adresse,nbBornette,idPlage from Station";
                ResultSet rs1 = st.executeQuery(sql1);
                String mess ="La liste des station est: \n";
                while(rs1.next()){
                    mess+= "Le station numéro "+rs1.getInt(1)+" à "+rs1.getString(2)+" avec la plage numéro "+rs1.getInt(4) +"\n";
                    arr1.add(rs1.getInt(1));
                }
                System.out.println(mess);

                String sql = "select IDPLAGEHORAIRE,to_char(dateDebut,'HH24:MI:SS') as dateDebut,to_char(DATEFIN,'HH24:MI:SS') as DATEFIN,PRIME from plagehoraire";
                ResultSet rs = st.executeQuery(sql);
                String msg = "La liste de la plage horaire possible est:\n";
                String s = format.format(calendar.getTime());
                while(rs.next()){   
                    String debut = rs.getString(2);
                    String fin = rs.getString(3);
                    if(compareDate(s, debut)>=0 && compareDate(s, fin)<=0){
                    msg+= "La plage numéro "+rs.getInt(1)+ " est de " +debut + " à "+fin + " au "+rs.getString(4)+".\n";
                    arr.add(rs.getInt(1));
                    }
                }
                
                System.out.println("Veuillez choisir le numéro de la station que vous voulez changer");
                int ch = sc.nextInt();
                while(!arr1.contains(ch)){
                    System.out.println("Votre choix n'existe pas dans la liste! Veuillez choisir un autre!!");
                    System.out.println(mess);
                    ch = sc.nextInt();
                } 
                System.out.println(msg);
                System.out.println("Veuillez choisir le numéro de la plage que vous voulez changer");
                int id = sc.nextInt();
                while(!arr.contains(id)){
                    System.out.println("Votre choix n'existe pas dans la liste! Veuillez choisir un autre!!");
                    System.out.println(msg);
                    System.out.println("Veuillez rechoisir un nouvel numéro!");
                    id = sc.nextInt();
                }
                modifierPlageHoraire(id,ch);
                System.out.println("Votre commande est bien enregistrée!!");
                conn.close(); 
                System.out.println("bye.");
                menu();
            case 6:
                System.out.println("Merci de l'utilisation notre appication!!!");
                conn.close(); 
                System.out.println("bye.");
                System.exit(0);
            default:
                menu();
        }
    }
    
    
}
