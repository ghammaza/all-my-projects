/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package renduVelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import lectureClavier.LectureClavier;


/**
 *
 * @author ghammaza
 */
public class Facturation {
static final String CONN_URL = "jdbc:oracle:thin:@im2ag-oracle.e.ujf-grenoble.fr:1521:im2ag";

    public static Statement st;
    public int prixClasse;
    public String primeA;
    public int idVelo;
    
    public int getPrixClasse() {
		return prixClasse;
	}

	/**
     * @param args the command line arguments
     * @throws java.sql.SQLException
import sun.util.calendar.BaseCalendar.Date;
     * @throws java.text.ParseException
     */
    
/*
	System.out.println("Debut");
	int cage = LectureClavier.lireEntier("Saisir numero cage: ");
	System.out.println("Saisir sa nouvelle fonctionnalite :");
	String newFonction = LectureClavier.lireChaine();
  	Statement requete = conn.createStatement();
  	
	int resultat = requete.executeUpdate(
    " UPDATE LesCages"+
	" SET fonction= '"+newFonction+"' "+
	" WHERE noCage = "+cage);
  	
  	*/
    

     public Facturation(Connection conn, int idReserLoca, int idClient, boolean b, String primeA) throws SQLException {
        // TODO code application logic here
        
            this.st = conn.createStatement();
            this.primeA = primeA;
            
            int result = -1;
            ResultSet rs1;
            //  start BD_Regis_Reservation
            if (b) {
                rs1 = st.executeQuery("select lastAction from Abonnes where idabonne =" + idReserLoca);
                
                String lastA = "";
                while (rs1.next()) {
                    lastA = rs1.getString(1);
                    prixClasse = getPrix(idReserLoca, b,lastA);
                }
                if (!lastA.equals("loc")) {
                    System.out.println("b true et !loc");
                    System.out.println("La Reservations n°" + idReserLoca + " de l'abonne cout " + prixClasse);
                    st.executeQuery("insert into Facturation (clientAbonnes ,clientNonAbonnes, prix, idVelo) values (" + idClient + ",null," + prixClasse + "," + idVelo + ")");
                    st.executeUpdate("update reservations set renduVelo='oui' where idreservation=" + idReserLoca);
                    st.executeUpdate("UPDATE velo SET dispo = 'oui' where idVelo=" + idVelo);
                } else {
                    System.out.println("b true et !loc");
                    System.out.println("La Location n°  " + idReserLoca + " de l'abonnee cout " + prixClasse);
                    st.executeQuery("insert into Facturation (clientAbonnes ,clientNonAbonnes, prix, idVelo) values (" + idClient + " ,null," + prixClasse + " ," + idVelo + ")");
                    st.executeUpdate("update Location set renduVelo='oui' where idLocation=" + idReserLoca);
                    st.executeUpdate("UPDATE velo SET dispo = 'oui' where idVelo=" + idVelo);
                }
            } else {
                prixClasse = getPrix(idReserLoca, b,"loc");
                System.out.println("La Location n° " + idReserLoca + " du nonAbonnee cout " + prixClasse);
                st.executeQuery("insert into Facturation (clientAbonnes ,clientNonAbonnes, prix, idVelo) values (null," + idClient + "," + prixClasse + " ," + idVelo + ")");
                st.executeUpdate("update Location set renduVelo='oui' where idLocation=" + idReserLoca);
                st.executeUpdate("UPDATE velo SET dispo = 'oui' where idVelo=" + idVelo);
            }
// update reservations SET dateDebut = TO_DATE('23:08:16','HH24:MI:SS') where idReservation=1;
//select idReservation ,idClient ,idVelo ,station ,renduVelo ,to_char(dateDebut,'HH24:MI:SS') as dateDebut,to_char(dateDebutJour,'DD/MM/YYYY') as dateDebutJour  from Reservations;
       
    }
    
//    IDVELO     DATEDEBU
//    ---------- --------
//    1 		 08:38:16
//    idreservations=1

	
	
	public int setVraiPrix(int minute,int tempsEcoule,int prime,int prix) {
		int prixFinale = 0;
        tempsEcoule = minute / 30 + 1;
    	int prixN = prix*tempsEcoule;
    	System.out.println("minute :"+minute);
        if(minute<3) {
        	int rep=0;
        	do {
        		System.out.println("Le velo est-il HS :");
        		rep = LectureClavier.lireEntier("1) Oui      2) Non");
	    		switch(rep) {
	    			case 1:
	    				prixFinale=0;
	    				break;
	    			case 2:
	    				prixFinale=prixN;
	    				break;
	    			default:
	    				System.out.println("Veuillez- repondre correctement");
	    				break;
	    		}
        	}while(rep!=1 && rep!=2);
        }else {
        	prixFinale = prixN;
        }
		return Math.max(prixFinale-getPromo(this.primeA),0);
	}
	
	public int getPromo(String primeD) {
		int promo=0;
		if(primeD.equals("Vplus")) {
			promo-=3;
		}else if(primeD.equals("Vmoins")){
			promo+=3;
		}
		
		if(primeA.equals("Vplus")) {
			promo-=3;
		}else if(primeA.equals("Vmoins")){
			promo+=3;
		}
		return promo;
	}
	
//  IDVELO     DATEDEBU
//  ---------- --------
//  1 		   08:38:16
//  idreservations=1
	public int getMinute(String dateDebut) throws SQLException {
        int minute=0,dateD=0;
        long dateFin = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");    
        Date resultdate = new Date(dateFin);
        String dateFFin=""+sdf.format(resultdate);
        //System.out.println("dateFin :"+dateFFin);
        
        dateD = convertisseur(dateDebut);
        dateFin = convertisseur(dateFFin);
        minute = (int)dateFin - dateD;
        int tempsEcoule = minute;
        //System.out.println("dateDMin :"+dateD+"  dateFinMin:"+dateFin+"  minuteM:"+minute);
        return tempsEcoule;
    }
	

	
	public int getMinuteRandom(int idReserLoca, String dateDebut) throws SQLException {
        int minute=0,dateD=0;
        dateD = convertisseur(dateDebut);
        int dateFin = convertisseurRandom(dateDebut);
        minute = (int)dateFin - dateD;
        int tempsEcoule = minute;
        //System.out.println("dateDMin :"+dateD+"  dateFinMin:"+dateFin+"  minuteM:"+minute);
        return tempsEcoule;
    }
	
	public static int nombreAleatoire(int min, int max) {
	    return min + (int)Math.floor((max - min + 1) * Math.random());
	}
	
	public int convertisseur(String dateDebut) {
		int minuteDebut=0;
                System.out.println(dateDebut);
		int heure  = Integer.parseInt(dateDebut.substring(0,2));
		int minute = Integer.parseInt(dateDebut.substring(3,5));
		minuteDebut = heure*60+minute;
		//int minuteFin = nombreAleatoire(heure+1,23)*60+nombreAleatoire(0,23);
		return minuteDebut;
	}
	
	public int convertisseurRandom(String dateDebut) {
		int heure  = Integer.parseInt(dateDebut.substring(0,2));
		int newHeure = nombreAleatoire(heure+1,23);
		int newMinute = nombreAleatoire(0,60);
        int dateFin = newHeure*60+newMinute;
        //System.out.println("heure random->"+newHeure+":"+newMinute);
		return dateFin;
	}
	
    
    
    public int getPrixModele(int idModeleVelo) throws SQLException {
        int prix = 0;
        ResultSet rs = st.executeQuery("select prix from modeleVelo where idModeleVelo=" + idModeleVelo);
        while (rs.next()) {
        	// insert into modeleVelo (idModeleVelo,libelleModele,type,prix) values (4,'nasa','fusee',99);
        	prix=rs.getInt(1);
        }
        return prix;
    }
    
    public String getPrimePlageHoraireReserv(int idReserLoca) throws SQLException {
    	 String Prime="";
         ResultSet rs = st.executeQuery("select PRIME from plageHoraire WHERE idPlageHOraire IN (select station from Reservations where idReservation="+idReserLoca+")");
         while (rs.next()) {
         	// idReservation ,idClient ,idVelo ,station ,renduVelo ,dateDebut  sysdate
        	 Prime =  rs.getString(1);
         }
         return Prime;
    }
    
    public String getPrimePlageHoraireLocat(int idReserLoca) throws SQLException {
   	 String Prime="";
        ResultSet rs = st.executeQuery("select PRIME from plageHoraire WHERE idPlageHOraire IN (select stationdebut from location where idlocation="+idReserLoca+")");
        while (rs.next()) {
        	// idReservation ,idClient ,idVelo ,station ,renduVelo ,dateDebut  sysdate
       	 Prime =  rs.getString(1);
        }
        return Prime;
   }
    
    public int getModeleVelo(int idVelo)  throws SQLException {
        int idmod = 0;
        ResultSet rs = st.executeQuery("select idModeleVelo from Velo where idVelo=" + idVelo);
        while (rs.next()) {
        	// insert into modeleVelo (idModeleVelo,libelleModele,type,prix) values (4,'nasa','fusee',99);
        	idmod=rs.getInt(1);
        }
        return idmod;
    }
    

	
 public int getPrix(int idReserLoca, boolean b, String resloc) throws SQLException {
        int prix = 0, tempsEcoule = 0, minute = 0;
        String dateDebut = "";
        ;
        if (b) {
            if (resloc.equals("res")) {
                ResultSet rs = st.executeQuery("select idVelo,to_char(dateDebut,'HH24:MI:SS') as dateDebut from Reservations where idReservation=" + idReserLoca);
                while (rs.next()) {
                    // idReservation ,idClient ,idVelo ,station ,renduVelo ,dateDebut  sysdate
                    this.idVelo = rs.getInt(1);
                    dateDebut = rs.getString(2);
                }
            } else {
                ResultSet rs1 = st.executeQuery("select velo,to_char(dateDebut,'HH24:MI:SS') as dateDebut from location where idlocation=" + idReserLoca);
                while (rs1.next()) {
                    // idReservation ,idClient ,idVelo ,station ,renduVelo ,dateDebut  sysdate
                    this.idVelo = rs1.getInt(1);
                    dateDebut = rs1.getString(2);
                }
            }
        } else {
            ResultSet rs2 = st.executeQuery("select velo,to_char(dateDebut,'HH24:MI:SS') as dateDebut from location where idlocation=" + idReserLoca);
            while (rs2.next()) {
                // idReservation ,idClient ,idVelo ,station ,renduVelo ,dateDebut  sysdate
                this.idVelo = rs2.getInt(1);
                dateDebut = rs2.getString(2);
            }
        }

        System.out.println("idVeloPrix :" + this.idVelo + " dateDebutPrix :" + dateDebut);
        prix = getPrixModele(getModeleVelo(this.idVelo));
        minute = getMinute(dateDebut);
        //minute = getMinuteRandom(idReservation,dateDebut);
        int prime = 0;
        if (b) {
            prime = getPromo(getPrimePlageHoraireReserv(idReserLoca));
        } else {
            prime = getPromo(getPrimePlageHoraireLocat(idReserLoca));
        }
        //System.out.println("prixFinale "+prixFinale+" prix:"+prix+" tempsEcoule:"+tempsEcoule+"");
        return setVraiPrix(minute, tempsEcoule, prime, prix);
    }
    
}
