package renduVelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashSet;

import Forms.FormAddAb;
import Forms.FormNonAb;
import Forms.FormRes;
import client.Abonnee;
import lectureClavier.LectureClavier;

public class TestFacturation {

    static final String CONN_URL = "jdbc:oracle:thin:@im2ag-oracle.e.ujf-grenoble.fr:1521:im2ag";

    static final String USER = "ghammaza";
    static final String PASSWD = "j3ib6Sm3VY";

    
   public static Connection conn;
    public static Statement st;
    
    public static void main(String[] args) {
        try {
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
            conn = DriverManager.getConnection(CONN_URL, USER, PASSWD);
            conn.setAutoCommit(true);
            st=conn.createStatement();
            //Facturation f1 = new Facturation(st,1,2,false,"Vnull");
            Facturation f = new Facturation(conn,2,2,true,"Vnull");
            //Facturation f2 = new Facturation(st,3,3,true,"Vnull");
            conn.close();
        } catch (SQLException e) {
            System.err.println("failed");
            System.out.println("Affichage de la pile d'erreur");
            e.printStackTrace(System.err);
            System.out.println("Affichage du message d'erreur");
            System.out.println(e.getMessage());
            System.out.println("Affichage du code d'erreur");
            System.out.println(e.getErrorCode());

        }
	}
 // update reservations SET dateDebut = TO_DATE('20:08:16','HH24:MI:SS') where idReservation=1;
  //select idReservation ,idClient ,idVelo ,station ,renduVelo ,to_char(dateDebut,'HH24:MI:SS') as dateDebut,to_char(dateDebutJour,'DD/MM/YYYY') as dateDebutJour  from Reservations;

//    
//    IDBORNETTE  IDSTATION ETAT      IDVELO
//    ---------- ---------- ----- ----------
//             1          1 OK             1
//             2          1 OK             2
//             3          1 OK             3
//             4          1 OK             4
//             5          1 OK             5
//             6          2 OK             6
//             7          2 OK             7
//             8          2 OK             8
//             9          2 OK             9
//            10          2 OK            10
//            11          3 OK            11
//            12          3 OK            12
//            13          3 OK            13
//            14          3 OK            14
//            15          3 OK            15
//            16          4 OK            16
//            17          4 OK            17
//            18          4 OK            18
//            19          4 OK
//            20          4 OK
//
//    20 rows selected.
//
//
//     IDSTATION ADRESSE              NBBORNETTE    IDPLAGE
//    ---------- -------------------- ---------- ----------
//             1 centreVille                   5          1
//             2 campus                        5          4
//             3 gare                          5          7
//             4 ville                         5          9
//
//
//    IDPLAGEHORAIRE DATEDEBU DATEFIN  PRIME
//    -------------- -------- -------- ----------
//                 1 08:00:00 16:00:00 Vplus
//                 2 08:00:00 16:00:00 Vmoins
//                 3 08:00:00 16:00:00 Vnull
//                 4 16:00:00 00:00:00 Vmoins
//                 5 16:00:00 00:00:00 Vnull
//                 6 16:00:00 00:00:00 Vplus
//                 7 00:00:00 08:00:00 Vnull
//                 8 00:00:00 08:00:00 Vplus
//                 9 00:00:00 08:00:00 Vmoins
//
//    IDVELO ETAT  DATEMISES DISPO IDMODELEVELO
//---------- ----- --------- ----- ------------
//         1 OK    01-JAN-10 oui              1
//         2 OK    02-JAN-10 oui              1
//         3 OK    03-JAN-10 oui              1
//         4 OK    04-JAN-10 oui              1
//         5 OK    05-JAN-10 oui              1
//         6 OK    15-JAN-10 oui              2
//         7 OK    16-JAN-00 oui              2
//         8 OK    17-JAN-00 oui              2
//         9 OK    18-JAN-00 oui              2
//        10 OK    19-JAN-00 oui              2
//        11 OK    31-DEC-14 oui              3
//        12 OK    31-DEC-14 oui              3
//        13 HS    31-DEC-14 oui              3
//        14 OK    31-DEC-14 oui              3
//        15 OK    31-DEC-14 oui              3
//        16 OK    14-FEB-14 oui              4
//        17 OK    14-FEB-14 oui              4
//        18 OK    14-FEB-14 oui              4
//
//18 rows selected.
//
//
//IDMODELEVELO LIBELLEMODELE                                      TYPE                       PRIX
//------------ -------------------------------------------------- -------------------- ----------
//           1 faineant                                           electrique                   10
//           2 course                                             caddie                        5
//           3 normale                                            basique                       1
//           4 nasa                                               fusee                        99
//
//
//  IDABONNE NOM                  PRENOM               DATENAIS  ADRESSE                                            SEXE       CODESECRET         CB LASTA
//---------- -------------------- -------------------- --------- -------------------------------------------------- ---------- ---------- ---------- -----
//         1 ayoub                ghammaz              06-FEB-98 6 rue                                              masculin         1111 1.1111E+11 res
//         2 regis                orand                11-JUN-96 2 avenue                                           masculin         2222 1.1111E+11 res
//         3 phan                 thang                05-DEC-98 18 rue                                             masculin         3333 1.1111E+11 loc
//         4 adam                 madide               11-NOV-11 11 rue                                             masculin         4444 1.0110E+11 loc
//         5 alizee               orand                11-DEC-97 2 avenue                                           feminin          5555 1.1111E+11
//         6 mike                 tayson               10-APR-68 1 temple                                           masculin         6666 1.0111E+11
//         7 omer                 simpson              01-JAN-01 198 milieu                                         masculin         7777 1.1111E+11
//         8 carli                morgan               10-OCT-10 15 lili                                            feminin          8888 6.9111E+11
//         9 google               miguel               31-DEC-00 1 million                                          masculin         9999 2.3411E+10
//
//9 rows selected.
//
//
//IDNONABONNE         CB CODESECRET
//----------- ---------- ----------
//          1 1.2346E+14       1110
//          2 1.2346E+14       1101
//          3 1.2346E+14       1011
//
//
//IDRESERVATION   IDCLIENT     IDVELO    STATION RENDU DATEDEBU DATEDEBUTJ
//------------- ---------- ---------- ---------- ----- -------- ----------
//            1          1          1          1 oui   08:38:16 15/02/2019
//            2          2          2          2 oui   09:30:16 26/03/2019
//            3          3          3          3 oui   05:11:49 14/02/2019
//
//
//IDFACTURATION CLIENTABONNES CLIENTNONABONNES       PRIX     IDVELO
//------------- ------------- ---------------- ---------- ----------
//            1             1                          32          1
//            2             2                          32          2
//            3                              2         99         17
//
//
//IDLOCATION STATIONDEBUT DATEDEBU CLIENTABONNES CLIENTNONABONNES       VELO RENDU
//---------- ------------ -------- ------------- ---------------- ---------- -----
//         1            4 11:26:16                              2         18 oui
//         2            2 06:08:16             4                          10 oui
//         3            3 18:40:16             5                          15 oui

	

}
