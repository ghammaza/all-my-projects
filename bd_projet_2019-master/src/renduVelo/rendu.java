package renduVelo;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class rendu {

    public int idReservation;
    public int idVelo;
    public int idBornette;
    public int idClient;

    public rendu(Connection conn, int idReservation, int idBornette, int idClient,boolean abonne) throws SQLException {
        this.idReservation = idReservation;
        this.idVelo = idVelo;
        this.idBornette = idBornette;
        this.idClient = idClient;
        //addReservationAbo (NewidClient IN Integer,NewidVelo IN Integer,NewidStation IN String, NewdateDebut IN VARCHAR2 )
        if(abonne){
        CallableStatement st1 = conn.prepareCall("{call renduVeloProcedure (? ,? ,? )}");
        st1.setInt(1, this.idReservation);
        st1.setInt(2, this.idBornette);
        st1.setInt(3, this.idClient);
        // Do the raise
        st1.execute();

        // Close the statement
        st1.close();
        }
        else{
        CallableStatement st2 = conn.prepareCall("{call renduVeloProcedureNAb (? ,? ,? )}");
        st2.setInt(1, this.idReservation);
        st2.setInt(2, this.idBornette);
        st2.setInt(3, this.idClient);
        // Do the raise
        st2.execute();

        // Close the statement
        st2.close();
        
        }
    }
}


/*
 
  select lastAction from Abonnes where idAbonne = 1; 
  
create or replace procedure renduVeloProcedure (NewidReservation IN Integer,
 NewidVelo IN Integer,NewidBornette IN Integer, NewidClient IN Integer) 
IS  
	BEGIN 	
		UPDATE velo SET dispo = 'oui' where idVelo=NewidVelo;
		UPDATE Bornette set idvelo=NewidVelo where idbornette=NewidBornette;
		
		UPDATE reservations set renduVelo='oui' where idReservation=NewidReservation;
		
		
	END;
/

select * from reservations;




create or replace procedure addReservationAbo (NewidReservation IN Integer, NewidClient IN Integer,NewidVelo IN Integer,NewidStation IN NUMBER, NewdateDebut IN VARCHAR2, NewdateDebutJour IN VARCHAR2  ) 
IS 
	BEGIN 
		Select idBornette INTO idBornetteV FROM Bornette where idVelo=NewidVelo ;
		INSERT  INTO reservations (idReservation, idClient,idVelo,station,dateDebut,dateDebutJour) Values ( NewidReservation, NewidClient,NewidVelo,NewidStation,TO_DATE(NewdateDebut,'HH24:MI:SS'),TO_DATE(NewdateDebutJour,'DD/MM/YYYY') ) ;		
		UPDATE velo SET dispo = 'non' where idVelo=NewidVelo;
		UPDATE Abonnes SET lastAction = 'res' where idAbonne=NewidClient;
		update Bornette set idvelo=0 where idbornette=idBornetteV;
	END;
/


create or replace procedure renduVeloProcedure (Newid IN Integer, NewidVelo IN Integer,NewidBornette IN Integer, NewidClient IN Integer) 
IS  
	lastA NUMBER;
	BEGIN 	
		SELECT count(*) INTO lastA from Abonnes where idAbonne =Newid and lastAction='res'; 
		UPDATE velo SET dispo = 'oui' where idVelo=NewidVelo;
		update Bornette set idvelo=NewidVelo where idbornette=NewidBornette;
		IF  lastA=0	² then
			UPDATE reservations set renduVelo='oui' where idReservation=Newid;
		ELSE
			UPDATE Location set renduVelo='oui' where idLocation=Newid;
		ENDIF;
	END;
/


SELECT count(*) from Abonnes where idAbonne = 1 and lastAction='res'; 

SELECT lastAction from Abonnes where idAbonne = 1; 
UPDATE velo SET dispo = 'oui' where idVelo=1;
update Bornette set idvelo=1 where idbornette=16;
IF  lastA='res' then
	UPDATE reservations set renduVelo='oui' where idReservation=1;
ELSE
	UPDATE Location set renduVelo='oui' where idLocation=Newid;
ENDIF;

IFNULL( count(Velo.idVelo), 0 ) AS nbVelo
IF( count(Velo.idVelo) > 0,  count(Velo.idVelo), 0 ) AS nbVelo


select coalesce(MAX(count(idStation) ),0 ) from Station where idStation=4;
ISNULL(COUNT(*), '0')




select Station.idStation as numStation
from Station LEFT OUTER JOIN Bornette ON (Station.idStation=Bornette.idStation) 
LEFT OUTER JOIN Velo ON (Bornette.idVelo=Velo.idVelo) 
where Velo.etat='OK' group by (Station.idStation) IN (select count(*) from Velo where etat='HS')  ;
  
 select idStation,nbVelo from Station HAVING (  select count(*) as nbVelo from Velo where etat='HS' ) > 0;
  
  select count(*) as nbVelo,idStation from Velo natural join Bornette Natural join Station where etat='HS' and idStation IN (select idStation from Station) group by idStation;
  
  
  
  
  
   select count(*) as nbVelo from Velo where etat='OK' UNION select idStation from Station natural join Bornette natural join Velo where etat='OK';
  
  
  
  
  
  select idStation from Station IN (select count(*) as nbVelo from Velo);
  
  
  
  
  
  
  
  
  
  
  
  
  
select sum(Velo.idVelo) as nbVelo,Station.idStation from Station 
	LEFT OUTER JOIN Bornette ON (Station.idStation=Bornette.idStation) 
	LEFT OUTER JOIN Velo ON (Bornette.idVelo=Velo.idVelo) 
	where Velo.etat='HS' 
	group by (Station.idStation,nbVelo) 
UNION ALL
select '',0 from dual where not exists ( nbVelo);
   
    
   select sum(Velo.idVelo),Station.idStation from Station LEFT OUTER JOIN Bornette ON (Station.idStation=Bornette.idStation) LEFT OUTER JOIN Velo ON (Bornette.idVelo=Velo.idVelo) where Velo.etat='HS' group by Station.idStation;
   
   show parameter reservations;
   
 */
