package reservation_Location;
import static applications.AppliClient.conn;
import client.Client;
import java.sql.CallableStatement;
import java.sql.Connection;
import velo.Velo;
import station.Station;
import java.util.Date;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Location {
	public static Integer idReservation;
	public Client client;
	public Station stationDeb ;
	public Station stationArr ;
	public Velo velo ;
	public Date dateDeb;
	public Date dateFin;
	
	public Location(Connection conn,int  idAbonne ,int idNonAb, int velo , int stationDeb) throws SQLException {
            DateFormat format1 = new SimpleDateFormat("HH:mm:ss");
        Date date1 = new Date();
        String dateDeb=format1.format(date1);
        
		CallableStatement st1 = conn.prepareCall ("{call addLocation (? , ? , ? ,?,? )}");

		st1.setInt(1,idAbonne);
	 	st1.setInt (2,idNonAb);  
                st1.setInt (3, velo);        // The raise argument is the third ?
                st1.setInt (4,stationDeb); 
                st1.setString (5,dateDeb); 
	    // Do the raise
	    st1.execute ();
	  
	    // Close the statement
	    st1.close();
	}
        //create or replace procedure addLocation (NewidClientAb IN Integer,NewidClientNAb IN Integer,NewidVelo IN Integer,NewidStation IN Integer, NewdateDebut IN VARCHAR2 ) 
}
