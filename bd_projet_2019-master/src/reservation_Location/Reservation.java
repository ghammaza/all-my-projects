package reservation_Location;

import java.sql.*;
import java.sql.SQLException;

public class Reservation  {

	public int idReservation;
	public int idClient;
	public int idvelo ;
	public int idStation ;
	public String dateDeb;
	
	public Reservation (Statement st,Connection conn, int idReservation, int idClient , int idvelo , int idstation, String dateDeb,String jour ) throws SQLException {
		this.idReservation=idReservation;
		this.idClient=idClient;
		this.idvelo=idvelo;
		this.idStation=idstation;
		this.dateDeb=dateDeb;
		//addReservationAbo (NewidClient IN Integer,NewidVelo IN Integer,NewidStation IN String, NewdateDebut IN VARCHAR2 )
		CallableStatement st1 = conn.prepareCall ("{call addReservationAbo (? , ? , ? ,? ,?,?)}");
		st1.setInt(1, this.idReservation);
	 	st1.setInt (2, this.idClient);  
	    st1.setInt (3, this.idvelo);        // The raise argument is the third ?
	    st1.setInt (4, this.idStation); 
	    st1.setString (5, this.dateDeb);
            st1.setString (6, jour);
	    // Do the raise
	    st1.execute ();
	  
	    // Close the statement
	    st1.close();
	}
}
/*
 * 
create or replace procedure addReservationAbo (NewidClient IN Integer,NewidVelo IN Integer,NewidStation IN String, NewdateDebut IN VARCHAR2 ) 
	is reservation_exist NUMBER;
	BEGIN 
	SELECT count(*)
	    INTO reservation_exist 
	    FROM Reservation
	    WHERE idClient=NewidClient and idVelo=NewidVelo and station=NewidStation and dateDebut=NewdateDebut;
	IF reservation_exist=0 then 
		INSERT  INTO reservations (idClient,idVelo,station,dateDebut) Values ( NewidClient,NewidVelo,NewidStation,TO_DATE(NewdateDebut,'DD/MM/YYYY') ) ;		
		UPDATE velo SET dispo = 'non' where idVelo=NewidVelo;
		UPDATE Abonnes SET lastAction = 'res' where idAbonne=NewidClient;
		UPDATE Bornette SET idVelo=NULL;
	END IF;
	END;
/

		INSERT  INTO reservations (idClient,idVelo,station,dateDebut) Values ( 1,1,1,TO_DATE('11/11/2019','DD/MM/YYYY') ) ;		


 */