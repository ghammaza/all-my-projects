/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Forms;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import lectureClavier.LectureClavier;
import reservation_Location.Location;

/**
 *
 * @author ghammaza
 */
public class FormLoc {
    private ArrayList<String> veloDispo;
    private ArrayList<String> stationDispo;
    public FormLoc(Connection conn, int codeSecret, String abNon) throws SQLException, ParseException {
        Statement st = conn.createStatement();
        System.out.println("-----------------Location-----------------");
        int idAbN = 0;
        if (abNon.equals("nonAbn")) {
            idAbN = getIdAbNon(st, codeSecret, abNon);// id  abonne
        } else {
            idAbN = getIdAbNon(st, codeSecret, abNon);// id non abonne
        }
        
        /**
         * ******************* choix de station**********************
         */
        stationDispo = new ArrayList<String>();
        getStationDispo(st);
        printStationDispo();
        System.out.println("Veuillez saisire le numero de station !!");
        int idStat = LectureClavier.lireEntier("Veillez entrer le numero de station de votre choix ");
        String stat = Long.toString(idStat);
        idStat = checkStationToReserve(stat);// id final de la station 

        /**
         * ******************* choix de station fin *****************
         */
        // derniere etape , choix du velo
        veloDispo = new ArrayList<String>();
        getVeloDispo(st, idStat);// recuperer les velo disponible a reserver                         
        printInfoModeleVelo(st);// imprimer les modeles existant
        printVeloDispo();//imprimer les velo disponible a reserver
         int idVelo = LectureClavier.lireEntier("Veillez entrer le numero de velo de votre choix");
            idVelo = checkBikeToRes(Long.toString(idVelo));// verifier si le choix est l un des proposition qu on afficher
        if (abNon.equals("nonAbn")) {
            Location lNonAb = new Location(conn,0,idAbN, idVelo, idStat);
        } else {
            Location lAb = new Location(conn,idAbN,0, idVelo, idStat);
        }
        
        
    }

    /**
     * *************getIdNonAb : recuperer le client non abonne qui corepend au
     * code secret************************************************************************
     */
    private int getIdAbNon(Statement st, int codeSecret, String abNon) throws SQLException {
        int idNonAbonne = 0;
        if (abNon.equals("nonAbn")) {
            ResultSet rs1 = st.executeQuery("select idNonAbonne from NonAbonnes where codeSecret =" + codeSecret);// recuper lid de l abonnee pour l inserer directement dans la table reservation
            while (rs1.next()) {
                idNonAbonne = rs1.getInt(1);//
            }
        }
        else{
        ResultSet rs1 = st.executeQuery("select idAbonne from Abonnes where codeSecret =" + codeSecret);// recuper lid de l abonnee pour l inserer directement dans la table reservation
        while (rs1.next()) {
            idNonAbonne = rs1.getInt(1);//
        }
        
        }
        return idNonAbonne;

    }
    /**
     * ************** fin methode getIdNonAb*************************************************************************************************************************
     */
    
     /**
     * ******************* getVeloDispo : recuperer les velo quand dispo = oui *******************************************************************
     */
    private void getVeloDispo(Statement st, int idstat) throws SQLException {// recuperer tous les velo disponible avec leur modele

        ResultSet rs1 = st.executeQuery("select idVelo ,type from bornette natural join station natural join velo natural join ModeleVelo where velo.dispo='oui' and etat='OK' and etat='OK' and idStation=" + idstat);// recuper lid des velo disponuble avec leur medele
        int i = 0;
        while (rs1.next()) {
            String velo = Long.toString(rs1.getInt(1));
            veloDispo.add(i, velo);// stocker l 'id velo dans la case i et son modele dans la case i+1
            veloDispo.add(i + 1, rs1.getString(2));// stocker le modele du velo qui existe dans la case d avant
            i = i + 2;
        }

    }

    /**
     * *******************************************************************************************************************************************
     */

    /**
     * ***************** printVeloDispo : imprimer les velo disponoble qu on
     * stocker dans veloDispo ***********************************************
     */
    private void printVeloDispo() {
        String aff = "Voici les velos disponibles \n";
        int i = 0;

        System.out.println(veloDispo);
        while (i < veloDispo.size() - 1) {
            aff += "Velo num :" + veloDispo.get(i) + " ----- " + veloDispo.get(i + 1) + " \n";
            i = i + 2;
        }
        System.out.println(aff);
    }

    /**
     * ********************************************************************************************************************************************
     */
    /**
     * *************** printInfoModeleVelo afficher tt les modeles qu on dispose
     * avec leur prix *******************************************************
     */
    private void printInfoModeleVelo(Statement st) throws SQLException {
        String infoModele = "Rensegnement sur les prix des modeles velo \n";
        ResultSet rs1 = st.executeQuery("select type , prix from ModeleVelo");
        while (rs1.next()) {
            infoModele += "Modele :" + rs1.getString(1) + " : " + rs1.getInt(2) + " euro pour 30 minutes \n";// modele du velo + son prix en euro 
        }
        System.out.println(infoModele);

    }

    /**
     * *********************************************************************************************************************************************
     */

    /**
     * ********** checkBikeToRes : verifier le num de velo saisi par le client
     * existe parmi les choix proposes*****************************************
     */
    private int checkBikeToRes(String idVelo) {

        while (!(veloDispo.contains(idVelo))) {
            System.out.println("Ce choix n'existe pas ");
            int d = LectureClavier.lireEntier(" Veillez resaisir un chiffre existant dans les choix !!");
            idVelo = Long.toString(d);
        }
        int veloExiste = Integer.parseInt(idVelo);
        return veloExiste;
    }

    /**
     * ********************************************************************************************************************************************
     */
    /**
     * ************* getStationDispo : stocker les stations disponible pour une reservation********************************************************
     */
    private void getStationDispo(Statement st) throws SQLException {
        ResultSet rs1 = st.executeQuery("select idStation , adresse ,prime from Station natural join PlageHoraire where Station.idplage=PlageHoraire.idPlageHoraire");
        // recuper lid des velo disponuble avec leur medele
        // select idStation , adresse ,prime from Station natural join PlageHoraire where Station.idplage=PlageHoraire.idPlageHoraire;
        int i = 0;
        while (rs1.next()) {
            String stat = Long.toString(rs1.getInt(1));
            stationDispo.add(i, stat);// stocker l 'id velo dans la case i et son modele dans la case i+1
            String fusion = "Adresse : " + rs1.getString(2) + " ---> " + rs1.getString(3);
            //String fusion="Adresse : "+rs1.getString(2)+" ---> ";
            stationDispo.add(i + 1, fusion);// stocker le modele du velo qui existe dans la case d avant
            i = i + 2;
        }
    }

    /**
     * ********************************************************************************************************************************************
     */

    /**
     * ************** printStationsDispo***********************************************************************************************************
     */
    public void printStationDispo() {
        String aff = "Les stations disponible pour votre reservation \n";
        int i = 0;
        while (i < stationDispo.size() - 1) {
            aff += "Station num : " + stationDispo.get(i) + " , " + stationDispo.get(+1) + " \n";
            i = i + 2;
        }
        System.out.println(aff);
    }

    /**
     * ********************************************************************************************************************************************
     */
    /**
     * *************checkStationToRes : verifier le numero de station saisie par
     * le client *******************************************************
     */
    private int checkStationToReserve(String idStation) {
        while (!(stationDispo.contains(idStation))) {
            System.out.println("Ce choix n'existe pas");
            int d = LectureClavier.lireEntier(" Veillez resaisir un chiffre existant dans les choix !!");
            idStation = Long.toString(d);
        }
        int stationExiste = Integer.parseInt(idStation);
        return stationExiste;
    }

    /**
     * *******************************************************************************************************************************************
     */

}
