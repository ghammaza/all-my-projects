/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Forms;
//import static applications.AppliClient.conn;
//import static applications.AppliClient.st;

import java.sql.Connection;
import reservation_Location.Reservation;
import java.util.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import lectureClavier.LectureClavier;

/**
 *
 * @author ghammaza
 */
public class FormRes {

    private ArrayList<String> veloDispo;
    private ArrayList<String> stationDispo;

    public FormRes(Connection conn, int codeSecret) throws SQLException, ParseException {

        Statement st = conn.createStatement();
        System.out.println("-----------------Reservation-----------------");
        int idAb = getIdAb(st, codeSecret);// id abonnee
        int idRes = getLastIdRes(st);//recuperer l id de la derniere reservation et l increment apres
        /**
         * ******************* choix de date ************************
         */
        // choisir la date de debut 
        boolean bb = false;
        String lastDateL = "";
        String datDeb = null;
        do {
            SimpleDateFormat dateDebForm = new SimpleDateFormat("dd/MM/yyyy");// forme de date reservation
            System.out.println("Veillez saisir la date de reservation (JJ/MM/AAAA)");
            String dateInString = LectureClavier.lireChaine();
            Date date = dateDebForm.parse(dateInString);
             datDeb = dateDebForm.format(date);

            while (!(verifFormDate(datDeb))) {
                System.out.println("date non valide , veillez saisie la date selon le modele JJ/MM/AAAA");
                dateInString = LectureClavier.lireChaine();
            }

            System.out.println("A quelle heure voulez vous reserver ?(HH:MM)");
            String heureMin = LectureClavier.lireChaine();
            while (!(verifHeure(heureMin))) {
                System.out.println("heure et minute non valide , veillez saisie la date selon le modele HH:MM");
                heureMin = LectureClavier.lireChaine();

            }
            DateFormat format = new SimpleDateFormat("HH:mm:ss");
            String lastDate =heureMin;
            //datDeb + " " +
            Date dateL = format.parse(lastDate);
            lastDateL = format.format(dateL);
            
            Date jour =dateDebForm.parse(datDeb);
            datDeb=dateDebForm.format(jour);
            
            DateFormat format1 = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            Date date1 = new Date();
            bb = date1.before(date);
            if (!bb) {
                System.out.println("Veillez saisir une date superieur ou egale a la date actuelle");
            }
        } while (!bb);

        /**
         * ******************* choix de date fin  *******************
         */
        /**
         * ******************* choix de station**********************
         */
        stationDispo = new ArrayList<String>();
        getStationDispo(st);
        printStationDispo();
        System.out.println("Veuillez saisire le numero de station !! ");
        int idStat = LectureClavier.lireEntier("Veillez entrer le numero de station de votre choix \n");
        String stat = Long.toString(idStat);
        idStat = checkStationToReserve(stat);// id final de la station 

        /**
         * ******************* choix de station fin *****************
         */
        // derniere etape , choix du velo
        veloDispo = new ArrayList<String>();
        getVeloDispo(st, idStat);// recuperer les velo disponible a reserver                         
        printInfoModeleVelo(st);// imprimer les modeles existant
        printVeloDispo();//imprimer les velo disponible a reserver
        int nbVelo = LectureClavier.lireEntier("Combien de velo desirez vous reserver ?");
        while (nbVelo > 6) {
            System.out.println("Vous pouvez pas depasser 6 velos pour une reservation");
            nbVelo = LectureClavier.lireEntier("Combien de velo desirez vous reserver ?");
        }
        int j = 0;//compteur
        while (j < nbVelo) {
            int idVelo = LectureClavier.lireEntier("Veillez entrer le numero de velo de votre choix");
            idVelo = checkBikeToRes(Long.toString(idVelo));// verifier si le choix est l un des proposition qu on afficher
            
            // reservation a faire !!!
            Reservation r = new Reservation(st, conn, idRes, idAb, idVelo, idStat, lastDateL,datDeb);
            j++;
        }

        st.close();
    }

    /**
     * ************** fin constructeur ************************************************************************************************************
     */
    /**
     * ****************** getIdAb : recuperer l id du client pour le saisir
     * automatiquement *******************************************************
     */
    private int getIdAb(Statement st, int codeSecret) throws SQLException {// pour les abonnes : retour de loc et res
        int idAbonne = 0;
        ResultSet rs1 = st.executeQuery("select idAbonne from Abonnes where codeSecret =" + codeSecret);// recuper lid de l abonnee pour l inserer directement dans la table reservation
        while (rs1.next()) {
            idAbonne = rs1.getInt(1);//
        }
        return idAbonne;
    }

    /**
     * *******************************************************************************************************************************************
     */

    /**
     * ******************* getVeloDispo : recuperer les velo quand dispo = oui *******************************************************************
     */
    private void getVeloDispo(Statement st, int idstat) throws SQLException {// recuperer tous les velo disponible avec leur modele

        ResultSet rs1 = st.executeQuery("select idVelo ,type from bornette natural join station natural join velo natural join ModeleVelo where velo.dispo='oui' and etat='OK' and etat='OK' and idStation=" + idstat);// recuper lid des velo disponuble avec leur medele
        int i = 0;
        while (rs1.next()) {
            String velo = Long.toString(rs1.getInt(1));
            veloDispo.add(i, velo);// stocker l 'id velo dans la case i et son modele dans la case i+1
            veloDispo.add(i + 1, rs1.getString(2));// stocker le modele du velo qui existe dans la case d avant
            i = i + 2;
        }

    }

    /**
     * *******************************************************************************************************************************************
     */

    /**
     * ***************** printVeloDispo : imprimer les velo disponoble qu on
     * stocker dans veloDispo ***********************************************
     */
    private void printVeloDispo() {
        String aff = "Voici les velos disponibles \n";
        int i = 0;

        System.out.println(veloDispo);
        while (i < veloDispo.size() - 1) {
            aff += "Velo num :" + veloDispo.get(i) + " ----- " + veloDispo.get(i + 1) + " \n";
            i = i + 2;
        }
        System.out.println(aff);
    }

    /**
     * ********************************************************************************************************************************************
     */
    /**
     * *************** printInfoModeleVelo afficher tt les modeles qu on dispose
     * avec leur prix *******************************************************
     */
    private void printInfoModeleVelo(Statement st) throws SQLException {
        String infoModele = "Rensegnement sur les prix des modeles velo \n";
        ResultSet rs1 = st.executeQuery("select type , prix from ModeleVelo");
        while (rs1.next()) {
            infoModele += "Modele :" + rs1.getString(1) + " : " + rs1.getInt(2) + " euro pour 30 minutes \n";// modele du velo + son prix en euro 
        }
        System.out.println(infoModele);

    }

    /**
     * *********************************************************************************************************************************************
     */

    /**
     * ********** checkBikeToRes : verifier le num de velo saisi par le client
     * existe parmi les choix proposes*****************************************
     */
    private int checkBikeToRes(String idVelo) {

        while (!(veloDispo.contains(idVelo))) {
            System.out.println("Ce choix n'existe pas ");
            int d = LectureClavier.lireEntier(" Veillez resaisir un chiffre existant dans les choix !!");
            idVelo = Long.toString(d);
        }
        int veloExiste = Integer.parseInt(idVelo);
        return veloExiste;
    }

    /**
     * ********************************************************************************************************************************************
     */
    /**
     * ************* getStationDispo : stocker les stations disponible pour une reservation********************************************************
     */
    private void getStationDispo(Statement st) throws SQLException {
        ResultSet rs1 = st.executeQuery("select idStation , adresse ,prime from Station natural join PlageHoraire where Station.idplage=PlageHoraire.idPlageHoraire");
        // recuper lid des velo disponuble avec leur medele
        // select idStation , adresse ,prime from Station natural join PlageHoraire where Station.idplage=PlageHoraire.idPlageHoraire;
        int i = 0;
        while (rs1.next()) {
            String stat = Long.toString(rs1.getInt(1));
            stationDispo.add(i, stat);// stocker l 'id velo dans la case i et son modele dans la case i+1
            String fusion = "Adresse : " + rs1.getString(2) + " ---> " + rs1.getString(3);
            //String fusion="Adresse : "+rs1.getString(2)+" ---> ";
            stationDispo.add(i + 1, fusion);// stocker le modele du velo qui existe dans la case d avant
            i = i + 2;
        }
    }

    /**
     * ********************************************************************************************************************************************
     */

    /**
     * ************** printStationsDispo***********************************************************************************************************
     */
    public void printStationDispo() {
        String aff = "Les stations disponible pour votre reservation \n";
        int i = 0;
        while (i < stationDispo.size() - 1) {
            aff += "Station num : " + stationDispo.get(i) + " , " + stationDispo.get(+1) + " \n";
            i = i + 2;
        }
        System.out.println(aff);
    }

    /**
     * ********************************************************************************************************************************************
     */
    /**
     * *************checkStationToRes : verifier le numero de station saisie par
     * le client *******************************************************
     */
    private int checkStationToReserve(String idStation) {
        while (!(stationDispo.contains(idStation))) {
            System.out.println("Ce choix n'existe pas");
            int d = LectureClavier.lireEntier(" Veillez resaisir un chiffre existant dans les choix !!");
            idStation = Long.toString(d);
        }
        int stationExiste = Integer.parseInt(idStation);
        return stationExiste;
    }

    /**
     * *******************************************************************************************************************************************
     */

    /**
     * ***** getLAstIdRes : recuperer le dernier id qui existe dans la table
     * reservation puis l incrementer***************************************
     */
    private int getLastIdRes(Statement st) throws SQLException {
        int idres = 0;
        ResultSet rs1 = st.executeQuery("select MAX(idReservation) from Reservations");// recuper lid des velo disponuble avec leur medele
        while (rs1.next()) {
            idres = rs1.getInt(1);
        }

        idres = idres + 1;
        return idres;

    }

    /**
     * ******************************************************************************************************************************************
     */
    /**
     * **********verifFormDate : verifier la forme de la date****************************************************************************************
     */
    private boolean verifFormDate(String date) {
        String jourStr = date.charAt(0) + "" + date.charAt(1);
        int jourInt = Integer.parseInt(jourStr);

        String moisStr = date.charAt(3) + "" + date.charAt(4);
        int moisInt = Integer.parseInt(moisStr);

        String anneStr = date.charAt(6) + "" + date.charAt(7) + "" + date.charAt(8) + "" + date.charAt(9);
        int anneeInt = Integer.parseInt(anneStr);

        return 1 <= jourInt && jourInt <= 31 && 1 <= moisInt && moisInt <= 12 && anneeInt > 2018;

    }

    /**
     * ** verif heure ********************************************************************************
     */
    private boolean verifHeure(String heure) {
        int heureInt = Integer.parseInt(heure.charAt(0) + "" + heure.charAt(1));
        int minute = Integer.parseInt(heure.charAt(3) + "" + heure.charAt(4));

        return 0 <= heureInt && heureInt <= 23 && 0 <= minute && minute <= 59;
    }
}
