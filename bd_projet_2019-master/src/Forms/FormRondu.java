/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Forms;

import static applications.AppliClient.st;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import lectureClavier.LectureClavier;
import renduVelo.*;

/* idclient , idres , id station , id bornette , idvelo , demande etat du velo **/
/**
 *
 * @author ghammaza
 */
public class FormRondu {

    private ArrayList<Integer> bornette;
    private ArrayList<String> stationDispo;

    public FormRondu(Connection conn, int codeSecret, String abNon) throws SQLException, ParseException {

        Statement st = conn.createStatement();
        System.out.println("-----------------rendu du velo-----------------");
        int idAbN = 0;
        if (abNon.equals("nonAbn")) {
            idAbN = getIdAbNon(st, codeSecret, abNon);// id  abonne
        } else {
            idAbN = getIdAbNon(st, codeSecret, abNon);// id non abonne
        }
        /**
         * ******************* choix de station**********************
         */
        stationDispo = new ArrayList<String>();
        getStationDispo(st);
        printStationDispo();
        System.out.println("Veuillez saisire le numero de station !!");
        int idStat = LectureClavier.lireEntier("Veillez entrer le numero de station de votre choix ");
        String stat = Long.toString(idStat);
        idStat = checkStationToReserve(stat);// id final de la station 

        /**
         * ******************* choix de station fin *****************
         */
        /**
         * ****** choix de bornette*********************************
         */
        bornette = new ArrayList<Integer>();
        getBornetteDispo(st, idStat);
        printBornetteDispo();
        System.out.println("Veuillez saisire le numero de bornette !!");
        int idBor = LectureClavier.lireEntier("Veillez entrer le numero de station de votre choix ");
        idBor = checkBornetteToReserve(idBor);// id final de la station */

        /**
         * *******************************************************
         */
        if (abNon.equals("nonAbn")) {
            if (getIdLocNonAb(st, idAbN) != 0) {
                rendu r = new rendu(conn, getIdLocNonAb(st, idAbN), idBor, idAbN,false);
                
                //System.out.println(getIdLocNonAb(st, idAbN)+"------"+idAbN+"/////"+false+"#####"+getPrime(st, idStat));
                
                Facturation f = new Facturation(conn, getIdLocNonAb(st, idAbN), idAbN, false, getPrime(st, idStat));
            } else {
                System.out.println("Vous n avez aucun velo a rendre ");
            }
            ///
        } else {
            if (getInfoAction(st, idAbN) != 0) {
                rendu r = new rendu(conn, getInfoAction(st, idAbN), idBor, idAbN,true);
                
                //System.out.println(getIdLocNonAb(st, idAbN)+"------"+idAbN+"/////"+false+"#####"+getPrime(st, idStat));
                
                Facturation f = new Facturation(conn, getInfoAction(st, idAbN), idAbN, true, getPrime(st, idStat));
            } else {
                System.out.println("Vous n avez aucun velo a rendre ");
            }
        }
    }

    /**
     * *************getIdNonAb : recuperer le client non abonne qui corepend au
     * code
     * secret************************************************************************
     */
    private int getIdAbNon(Statement st, int codeSecret, String abNon) throws SQLException {
        int idNonAbonne = 0;
        if (abNon.equals("nonAbn")) {
            ResultSet rs1 = st.executeQuery("select idNonAbonne from NonAbonnes where codeSecret =" + codeSecret);// recuper lid de l abonnee pour l inserer directement dans la table reservation
            while (rs1.next()) {
                idNonAbonne = rs1.getInt(1);//
            }
        } else {
            ResultSet rs1 = st.executeQuery("select idAbonne from Abonnes where codeSecret =" + codeSecret);// recuper lid de l abonnee pour l inserer directement dans la table reservation
            while (rs1.next()) {
                idNonAbonne = rs1.getInt(1);//
            }

        }
        return idNonAbonne;

    }

    /**
     * ************** fin methode
     * getIdNonAb*************************************************************************************************************************
     */
//     update reservations set datedebut=TO_DATE('15:11:49','HH24:MI:SS') where idreservation=2;
    /**
     * ************* getStationDispo : stocker les stations disponible pour
     * rendre un velo : avec des bornettes
     * libre********************************************************
     */
    private void getStationDispo(Statement st) throws SQLException {
        ResultSet rs1 = st.executeQuery("Select distinct(idStation) ,adresse ,prime from Station natural join Bornette natural join PlageHoraire where (idVelo is null) and Station.idplage=PlageHoraire.idPlageHoraire ");
        // recuper lid des velo disponuble avec leur medele
        // select idStation , adresse ,prime from Station natural join PlageHoraire where Station.idplage=PlageHoraire.idPlageHoraire;
        int i = 0;
        while (rs1.next()) {
            String stat = Long.toString(rs1.getInt(1));
            stationDispo.add(i, stat);// stocker l 'id velo dans la case i et son modele dans la case i+1
            String fusion = "Adresse : " + rs1.getString(2) + " ---> " + rs1.getString(3);
            //String fusion="Adresse : "+rs1.getString(2)+" ---> ";
            stationDispo.add(i + 1, fusion);// stocker le modele du velo qui existe dans la case d avant
            i = i + 2;
        }
    }

    /**
     * ********************************************************************************************************************************************
     */
    /**
     * **************
     * printStationsDispo***********************************************************************************************************
     */
    public void printStationDispo() {
        String aff = "Les stations disponible pour votre reservation \n";
        int i = 0;
        while (i < stationDispo.size() - 1) {
            aff += "Station num : " + stationDispo.get(i) + " , " + stationDispo.get(+1) + " \n";
            i = i + 2;
        }
        System.out.println(" ");
        System.out.println(aff);
        System.out.println(" ");

    }

    /**
     * ********************************************************************************************************************************************
     */
    /**
     * *************checkStationToRes : verifier le numero de station saisie
     * par le client *******************************************************
     */
    private int checkStationToReserve(String idStation) {
        while (!(stationDispo.contains(idStation))) {
            System.out.println("Ce choix n'existe pas");
            int d = LectureClavier.lireEntier(" Veillez resaisir un chiffre existant dans les choix !!");
            idStation = Long.toString(d);
        }
        int stationExiste = Integer.parseInt(idStation);
        return stationExiste;
    }

    /**
     * *******************************************************************************************************************************************
     */
    /**
     * *********** getBornetteDispo : recupere les bornette libre de la station
     * choisie par le
     * client********************************************************************
     */
    public void getBornetteDispo(Statement st, int station) throws SQLException {
        //select idBornette from bornette where idStation = 4 and idVelo is null ;

        ResultSet rs1 = st.executeQuery("select idBornette from bornette where (idVelo is null) and idStation = 4");

        while (rs1.next()) {
            int k = rs1.getInt(1);
            bornette.add(k);

        }

    }

    /**
     * ******************************************************************************************************************************************************************
     */
    /**
     * **************
     * printBornetteDispo : imprimer les bornette
     * dispo***********************************************************************************************************
     */
    public void printBornetteDispo() {
        String aff = "Les bornette disponible pour rendre votre velo \n";
        int i = 0;
        while (i < bornette.size()) {
            aff += "bornette num : " + bornette.get(i) + " est libre \n";
            i++;
        }
        System.out.println(" ");
        System.out.println(aff);
        System.out.println(" ");

    }

    /**
     * ********************************************************************************************************************************************
     */
    /**
     * *************checkStationToRes : verifier le numero de station saisie
     * par le client *******************************************************
     */
    private int checkBornetteToReserve(int idBornette) {
        while (!(bornette.contains(idBornette))) {
            System.out.println("Ce choix n'existe pas");
            idBornette = LectureClavier.lireEntier(" Veillez resaisir un chiffre existant dans les choix !!");

        }

        return idBornette;
    }

    /**
     * *******************************************************************************************************************************************
     */
    private int getInfoAction(Statement st, int idAbonne) throws SQLException {// pour les abonnes : retour de loc et res

        String last = "";
        int IdresLoc = 0;
        ResultSet rs1 = st.executeQuery("select lastAction from Abonnes where idAbonne =" + idAbonne);// voir la derniere action du client : si il a fait une location ou une reservation
        while (rs1.next()) {
            last = rs1.getString(1);

        }
        if (last.equals("loc")) {
            ResultSet rs2 = st.executeQuery("select count(*) from location  where clientAbonnes =" + idAbonne + " and renduVelo = 'non'");// voir la derniere action du client : si il a fait une location ou une reservation

            while (rs2.next()) {

                IdresLoc = rs2.getInt(1);
            }
            if (IdresLoc != 0) {
                ResultSet rs5 = st.executeQuery("select idLocation from location  where clientAbonnes =" + idAbonne + " and renduVelo = 'non'");// voir la derniere action du client : si il a fait une location ou une reservation
                while (rs5.next()) {
                    IdresLoc = rs5.getInt(1);

                }

            }
        } else {
            ResultSet rs3 = st.executeQuery("select count(*) from Reservations  where idClient  =" + idAbonne + " and renduVelo = 'non'");// voir la derniere action du client : si il a fait une location ou une reservation

            while (rs3.next()) {
                IdresLoc = rs3.getInt(1);

            }
            if (IdresLoc != 0) {
                ResultSet rs4 = st.executeQuery("select IDRESERVATION from Reservations  where idClient  =" + idAbonne + " and renduVelo = 'non'");// voir la derniere action du client : si il a fait une location ou une reservation
                while (rs4.next()) {
                    IdresLoc = rs4.getInt(1);

                }

            }
        }
        return IdresLoc;
    }

    /**
     * *************** getPrime : retourner la prime de la station
     * choisi******************************************************************************************************************
     */
    private String getPrime(Statement st, int idStation) throws SQLException {
        String prime = "";
        ResultSet rs1 = st.executeQuery("Select prime from Station natural join Bornette natural join PlageHoraire where (idVelo is null) and Station.idplage=PlageHoraire.idPlageHoraire ");
        while (rs1.next()) {
            prime = rs1.getString(1);
        }
        return prime;
    }
    /**
     * **************************************************************************************************************************************************************
     */
    /****** getIdLocNonAnb: recuperer l id de la derniere location du non ab*********************************************************************/
    
    private int getIdLocNonAb(Statement st , int idNonAb) throws SQLException{
    ResultSet rs5 = st.executeQuery("select IDLOCATION from location  where CLIENTNONABONNES  =" + idNonAb + " and renduVelo = 'non'");// voir la derniere action du client : si il a fait une location ou une reservation
                while (rs5.next()) {
                    idNonAb = rs5.getInt(1);

                }
    return idNonAb;
    }
    
    
    
    
    
    /*******************************************************************************************************************************************/
}
