/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Forms;

import static applications.AppliClient.conn;
import client.Abonnee;
import enumerations.Sexe;
import java.sql.SQLException;
import lectureClavier.LectureClavier;

/**
 *
 * @author ghammaza
 */
public class FormAddAb {
    private int code;
    public FormAddAb() throws SQLException {
        System.out.println("veuillez saisir votre nom");
        String non = LectureClavier.lireChaine();// saisi du nom 

        System.out.println("veuillez saisir votre prenom");
        String prenom = LectureClavier.lireChaine();

        System.out.println("veuillez saisir votre date naissance (JJ/MM/AAAA)");
        String dateNaissance = LectureClavier.lireChaine();

        Sexe s;
        System.out.println("vous etes : ");
        System.out.println("1) homme               2) femme");
        int ss = LectureClavier.lireEntier("votre sexe :");// gerer cas de ne saisir ni 1 ni 2
        if (ss == 1) {
            s = Sexe.M;
        } else {
            s = Sexe.F;// faire un switch apres
        }

        System.out.println("Entrez votre adresse");
        String adr = LectureClavier.lireChaine();

        // long cb =(long) LectureClavier.lireEntier("Entrez le numero de votre carte boncaire (16 chiffres)");
        System.out.println("Entrez le numero de votre carte boncaire (16 chiffres)");
        String cbVerif = LectureClavier.lireChaine();
        if (cbVerif.length() != 16) {// verifier si la cb a 16 chiffres
            do {
                /*  cb = LectureClavier.lireEntier("votre carte doit contenir 16 chiffres");
                                    cbVerif = Long.toString(cb);*/
                System.out.println("votre carte doit contenir 16 chiffres");
                cbVerif = LectureClavier.lireChaine();
            } while (cbVerif.length() != 16);
        }
        // creation du client
        long cb = Long.parseLong(cbVerif);
        Abonnee nB = new Abonnee(conn, non, prenom, dateNaissance, s, adr, cb);
        // nouvelle meyhode getCode
        System.out.println("Bonjour "+prenom+"votre code secret est " + nB.getCodeSecret());
        code=nB.getCodeSecret();

    }

    public int getCode() {
        return code;
    }

}
