/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Forms;

import static applications.AppliClient.conn;
import client.Abonnee;
import client.NonAbonnee;
import java.sql.SQLException;
import lectureClavier.LectureClavier;

/**
 *
 * @author ghammaza
 */
public class FormNonAb {

    private int code = 0;

    public FormNonAb() throws SQLException {
        System.out.println("Veillez saisir le numero de votre carte bancaire (16 chiffres)");
        String cbVerif = LectureClavier.lireChaine();
        if (cbVerif.length() != 16) {// verifier si la cb a 16 chiffres
            do {
                /*  cb = LectureClavier.lireEntier("votre carte doit contenir 16 chiffres");
                                    cbVerif = Long.toString(cb);*/
                System.out.println("votre carte doit contenir 16 chiffres");
                cbVerif = LectureClavier.lireChaine();
            } while (cbVerif.length() != 16);
        }
       long cb = Long.parseLong(cbVerif);
        NonAbonnee nonAb = new NonAbonnee(conn, cb);
        System.out.println("votre code secret est " + nonAb.getCodeSecret());
        code=code=nonAb.getCodeSecret();
    }

    public int getCode() {
        return code;
    }

}


  
