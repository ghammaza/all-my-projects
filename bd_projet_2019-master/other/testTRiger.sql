Create trigger TestAbonne BEFORE insert on Abonnes
FOR  each row
Declare
abnum NUMBER:=0;
Begin
select idAbonne into abnum from Abonnes where nom=:new.nom;
if abnum<>0 then raise_application_error(-20100,' vous etes deja abonne!');
end if;
Exception when NO_DATA_FOUND then DBMS_OUTPUT.PUT_LINE ('OK');
End;
/


create or replace procedure addClientAb (nomC varchar,prenomC  varchar,dateNaissaance  date,adresse  varchar , sexe  varchar , cb  Integer ,codeSecret  Integer ) 
		is
		client_exist NUMBER;
		BEGIN
		SELECT count(*) 
		    INTO client_exist
		    FROM Abonnes
			WHERE nom=nomC and prenom=prenomC;
		IF client_exist=0 then INSERT INTO Abonnes(nom ,prenom ,dateNais ,adresse ,sexe,codeSecret,cb ) VALUES (nomC ,prenomC ,TO_DATE(dateNaissaance, 'DD/MM/YYYY') ,adresse  , sexe  ,codeSecret , cb ) ;
		END IF;
		END;
/