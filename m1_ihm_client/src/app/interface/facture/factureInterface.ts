export interface FactureInterface {

    idCommande: string;
    idClient: string;
    films: string[];
    plats: string[];
    reduction: number;
    sousTotal: number;
    prixTotal: number;
    date: Date;
    points: number;
}


