import { SuggestionService } from 'src/app/services/suggestionService/suggestion.service';
import { PanierService } from 'src/app/services/PanierService/panier.service';
import { MenuService } from './../../services/menuService/menu.service';
import { CommandeService } from 'src/app/services/commandeService/commande.service';
import { PlatInterface } from 'src/app/interface/commande-data/plat';
import { environment } from 'src/environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { SearchMovieResponse } from './../../interface/tmdb-data/searchMovie';
import { TmdbService } from '../../services/tmdbService/tmdb.service';
import { Component, OnInit } from '@angular/core';
import { MovieResponse, MovieQuery } from '../../interface/tmdb-data/Movie';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  listSeach: SearchMovieResponse;
  isLoading: boolean;
  searched: boolean;
  affichage: string;
  listPopular: PlatInterface[];
  listAllPlats: PlatInterface[];
  listToShow: PlatInterface[];

  bestVente: PlatInterface[] = [];
  listPopularMovies: MovieResponse;
  page: number = 1;
  display: boolean = false;
  responsiveOptions :any;
  constructor(private spinner: NgxSpinnerService, private tmdb: TmdbService, private snackBar: MatSnackBar, private router: Router, private route: ActivatedRoute,
    private commande: CommandeService, private menuService: MenuService, private panier: PanierService,
    private suggestion: SuggestionService) {

      this.responsiveOptions = [
            {
                breakpoint: '1024px',
                numVisible: 3,
                numScroll: 3
            },
            {
                breakpoint: '768px',
                numVisible: 2,
                numScroll: 2
            },
            {
                breakpoint: '560px',
                numVisible: 1,
                numScroll: 1
            }
        ];
        
    }

  async  ngOnInit() {
    await this.menuService.getData().then(res => {
      this.listAllPlats = res;
      this.listToShow = this.listAllPlats;
      //console.log("la liste des plats: " + res);
    }, err => {
      this.snackBar.open('Une erreur est survenue lors de la récuperation des plats : ' + err, 'OK', {
        duration: 3000,
        panelClass: ['error-snackbar']
      });
    });
    this.isLoading = true;
    //récupération du numéro de page
    this.init();
  }
  public async init() {
    this.listAllPlats.length = 8;
    this.tmdb.init(environment.tmdbKey);
    this.getBestSellers();
    //affectation de la liste des films populaires
    await this.pullPopularMovies(this.page);


  }
  showDialog() {
    this.display = true;
  }

  public async searchMovies(queries: string) {
    this.isLoading = true;
    await this.tmdb.searchMovie({
      language: 'fr-FR',
      query: queries,
      region: 'DE',
    }).then(res => {
      this.isLoading = false;
      this.searched = true;
      this.listSeach = res;
      this.affichage = this.listSeach.total_results + ' resultats pour votre recherche "' + queries + '".';
    }, err => {
      this.isLoading = false;
      this.snackBar.open('Une erreur est survenue lors de la recherche des films', 'OK', {
        duration: 5000,
        panelClass: ['error-snackbar']
      });
    });


  }
  async getBestSellers() {
    const idBestSeller = await this.commande.getBestSellerPlats();
    idBestSeller.forEach(element => {
      const plat = this.listAllPlats.filter(p => {
        return p.id === element.id;
      });
      if (plat.length > 0) {
        this.bestVente.push(plat[0]);
      }
      this.listAllPlats.length = 8;
    });
    //console.log(this.bestVente)
  }

  //récuperation des films populaires
  public async pullPopularMovies(page: number, options?: MovieQuery) {
    await this.tmdb.getPopular(1, options).then(data => {
      this.listPopularMovies = data;
      this.listPopularMovies.results.length = 8;
    }, error => {
      this.snackBar.open('Une erreur est survenue lors de la récuperation des films de la page : ' + error, 'OK', {
        duration: 5000,
        panelClass: ['error-snackbar']
      });
    });
  }

  public pushplatsInPanier(plat: PlatInterface) {
    this.panier.addPlat(plat);
    /* this.snackBar.open('Le plat ' + plat.nom + ' a bien été ajouté dans votre panier', 'OK', {
        duration: 3000,
        panelClass: ['success-snackbar']
    });*/

    //dialog suggestion
    this.suggestion.suggestionPourPlat(plat);

  }
}
