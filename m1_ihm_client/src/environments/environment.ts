// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  tmdbKey: 'd806d1912d8335b500d503b77b5a816e',
  serveurAdd: '/api/',
  firebase: {
    apiKey: "AIzaSyB1OESJw8oGMrGoZcVSVzn16vU1oHqrxDs",
    authDomain: "menucinema-l3-miage-19.firebaseapp.com",
    databaseURL: "https://menucinema-l3-miage-19.firebaseio.com",
    projectId: "menucinema-l3-miage-19",
    storageBucket: "menucinema-l3-miage-19.appspot.com",
    messagingSenderId: "503905995713"
  }
};
