package inf353;
import java.lang.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.System;
import java.lang.System.*;
import java.nio.*;



public class  Principale{

    //attributs
    public  int D_MAX;
    public  int T_MAX;
    public   MatriceIndex mat;
    public   Dictionnaire d;
    public   TableHash th;
    public   IndexeurIntelligent ind;
    public   File[] Doc;
    
    public   final int N_MAX =32693 ; //nbr premiers pour reduire le nbr de collisions
    public   int nbMotH; //nbr t'elements ajoutés a la table
    public   int lgH;
    public   String                    nomDoss;
    public   String           nomMatriceCreuse;
    public   String nomDictionnaireIntelligent;
    public   String               nomTableHash;
    public   String                  LastModif;
    
    
    //Constructeurs
    public  Principale(int nbd, int nbt,String nomD,String LastM) {
        this.LastModif = LastM;
        this.nomDoss = nomD;
        this.T_MAX=nbt;  
        this.D_MAX=nbd;  
        this.mat = new MatriceIndexCreuse(this.T_MAX);
        this.d = new DictionnaireIntelligent(T_MAX); 
        this.th= new TableHash(this.N_MAX);
        File file = new File(nomD);
        this.ind = new  IndexeurIntelligent(nbd,nbt,nomD,file.lastModified()+"");
                  this.nomMatriceCreuse = "./src/test/ressource/SAUVER/matriceCreuse.txt-"+nbd+"_"+nbt+"_"+LastM;
        this.nomDictionnaireIntelligent = "./src/test/ressource/SAUVER/DictionnaireIntelligent.txt-"+nbd+"_"+nbt+"_"+LastM;
                      this.nomTableHash = "./src/test/ressource/SAUVER/table_hash.txt-"+nbd+"_"+nbt+"_"+LastM;
                      
    }


   
   
     public static void main (String []args) {
       try{
              String Rep = "./src/test/ressource/corpus";
              int nbd = Integer.parseInt(args[0]);
              int nbt = Integer.parseInt(args[1]);
                   if(args[2].equals("c" )){ Rep = "./src/test/ressource/corpus";}
              else if(args[2].equals("ce")){ Rep = "/ext/INF/354_projet/corpus/";}
              else if(args[2].equals("ff")){ Rep = "/ext/INF/354_projet/french_flat";}
              else if(args[2].equals("ffo")){ Rep = "/home/orandr/Bureau/FRENCH";}
              else if(args[2].equals("lmf")){ Rep = "/home/orandr/Bureau/Repertoire/lemonde_flat";}
              else if(args[2].equals("f" )){ Rep = "/ext/INF/354_projet/french";}
              else if(args[2].equals("e5")){ Rep = "/ext/INF/354_projet/echantillon_5000";}
              else if(args[2].equals("e1")){ Rep = "/ext/INF/354_projet/echantillon_100";}
              else  Rep = "./src/test/ressource/corpus";
               
               File file = new File(Rep);
               Principale pr = new  Principale(nbd,nbt,Rep,file.lastModified()+"");
               long date_debut =  System.currentTimeMillis();
               
               
               
               
               
               
               pr.ind.DATE("Debut de l'Indexation : ",date_debut);
               
               File FileDico = new File(pr.nomDictionnaireIntelligent);
               System.out.println(pr.nomDictionnaireIntelligent);
               if(!FileDico.exists() ){
                System.out.println("IndexeurIntelligent");
                pr.ind.Indexation();
               }
               else{//Mise a jour de matricecreuse, dico, tablehash, date
                 System.out.println("");
                 System.out.println("Remplissage du Dictionnaire");
                 pr.ind.Remplir_DICO();
                 pr.ind.DATE("",date_debut);
                 
                 System.out.println("");
                 System.out.println("Remplissage de la Table de Hachage");
                 pr.ind.Remplir_TableHash();
                 pr.ind.DATE("",date_debut);
                 
                 System.out.println("");
                 System.out.println("Remplissage de la MatriceCreuse");
                 pr.ind.Remplir_MatriceCreuse();
                 pr.ind.DATE("",date_debut);
                 
                 
                 
               System.out.println(" Taille de la tableHash : "+pr.ind.th.getsize());
               System.out.println(" Taille du dictionaire  : "+pr.ind.d.nbMots());
                 
               }
               
               pr.ind.DATE("Fin   de l'Indexation : ",date_debut);
               
               System.out.println("Principale Fin");
               System.out.println("");
               
               
                       
        }catch (IOException io){ 
            throw new RuntimeException("Erreur: main existance du fichier() Principale"+io);
        }
    }
}

/*


touch LOL.txt 
stat -c %y LOL.txt > Loldate.txt

temps arriver a  5 000 doc  1'29"30
temps arriver a 10 000 doc  3'23"20
temps arriver a 15 000 doc  5'21"41
temps arriver a 20 000 doc  7'28"26
temps arriver a 25 000 doc  9'32"01
temps arriver a 30 000 doc 11'43"52
temps arriver a 35 000 doc 13'54"30
temps arriver a 40 000 doc 16'11"83
temps arriver a 45 000 doc 18'26"30
temps arriver a 50 000 doc 20'41"00
temps arriver a 55 000 doc 23'58"00
temps arriver a 60 000 doc 25'16"10
temps arriver a 65 000 doc 27'32"00
temps arriver a 70 000 doc 29'56"10
temps arriver a 75 000 doc 32'46"54
temps arriver a 80 000 doc 35'40"96
temps arriver a 85 000 doc 38'32"80
temps arriver a 87 191 doc 39'40"10

50 seconde pour echantillon_5000

doc 87 191

Term 291 474   soit 320 000 avec -


ls | wc -l                 Pour savoir le nombre de ficheirs
87191
cat * | wc -w              Pour savoir le nombre de mots
32 millions de mot

*/
