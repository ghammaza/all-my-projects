package inf353;
public class CelluleM{
 
		public int nbocc;
		public int nbdoc;
		public  CelluleM succ;
		
		public  CelluleM(int occ,int doc,  CelluleM suc) {
			this.nbocc = occ;
			this.nbdoc = doc;
			this.succ  = suc;
		}
		
		public  CelluleM(int doc,  CelluleM suc) {
			this.nbocc ++;
			this.nbdoc = doc;
			this.succ  = suc;
		}
		
		public  CelluleM(CelluleM suc) {
			this.succ=suc;
		}
		
		public void CelluleM() {
		   this.nbocc = -1;
			this.nbdoc = -1;
			this.succ  = null;
		
		}
		
		public void setSucc(CelluleM c){
		    this.succ = c;
		}
		
		public CelluleM getSucc(){
		    return this.succ;
		}
		
		public void setnbdoc(int doc){
		    this.nbdoc = doc;
		}
		
		public void setnbocc(int occ){
		    this.nbocc = occ;
		}
		
		public void plusnbocc(CelluleM c){
		    c.nbocc ++;
		}
		
		public int getnbdoc(){
		    return this.nbdoc;
		}
		
		public int getnbocc(){
		    return this.nbocc;
		}
	}
