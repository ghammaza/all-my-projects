package inf353;
import java.io.*;

/**
 * Interface générique d'un accès séquenciel de modèle 1 d'élemnts de type T
 */
public class LecteurDocumentNaif implements AccesSequentielModele1<String> {
    private String mot; // texte parcouru.
    private String nomFic;
    private int cc;
    private InputStream ips;
    private InputStreamReader isr; 
    
    
    
    public LecteurDocumentNaif(String nom){
    		this.mot ="";
    		this.nomFic = nom;
    }

    /*
     * Initialisation du parcours.
     */
    public void demarrer(){
        
     try{
            this.ips=new FileInputStream(this.nomFic);
	       this.isr = new InputStreamReader(this.ips);
	 	  this.mot ="";
	 	  this.cc =' ';
	 	  avancer();
     }catch (IOException io) {
            throw new RuntimeException("Erreur: demmarer() LecteurDocumentNaif");
        
     }
      
    }    
    
    
    public boolean FinLigne(){
        return (this.cc == '\n');
    }
    
    public void avancer(){  
        this.mot = "";
        
        try{
            if(!finDeSequence()){
		         this.cc = this.isr.read();
		  
		         while( !finDeSequence() && estSep((char)cc) ){
		     	      this.cc = this.isr.read();
		         }
		     
			      while( !finDeSequence() &&  !estSep((char)cc)){
			 	 	this.mot = this.mot+(char)this.cc;
			 	 	this.cc = this.isr.read();
			      }
		      }
		  }catch (IOException io) {
            throw new RuntimeException("Erreur: avancer() LecteurDocumentNaif");
        
        }
    
    } 
    
    
    
     public void avancer(String[] tab, String ind){  
        try{
            int cpt=0;
            String indice = "";
            String MOT = "";
            if(!finDeSequence()){
		         this.cc = this.isr.read();
		  
		         while( !finDeSequence() && estSep((char)cc) ){
		     	      this.cc = this.isr.read();
		         }
			      if( !finDeSequence() && this.cc=='[' ){
			          while( !finDeSequence() &&  !estSep((char)cc) && this.cc!=']'   ){
			     	 	  this.cc = this.isr.read();
			          }
			          if(!finDeSequence()) this.cc = this.isr.read();  //Pour ne pas prendre ']' et me positionner correctement a la position d'apres
			      }
			      
			      else if(!finDeSequence() && this.cc==']' ){
			          this.cc = this.isr.read();  //Pour ne pas prendre ']' et me positionner correctement a la position d'apres
			      }
			      //Cette fois ci, on ne recupere pas l'indice dans la tableHash car elle est la meme grace au Hashcode
			      while( !finDeSequence() && estSep((char)cc) ){
		     	      this.cc = this.isr.read();
		         }
		         if(!((char)cc=='[')){
			                  while( !finDeSequence() &&  !estSep((char)cc) && !((char)cc=='[') &&  !((char)cc==']') ){
			             	 	MOT = MOT+(char)this.cc;
			             	 	this.cc = this.isr.read();
			                  }
		                     tab[0]=MOT;
			                  while( !finDeSequence() &&   estSep((char)cc) && !((char)cc=='[')  ){
		                 	      this.cc = this.isr.read();
		                     }
		                     
		                     if( this.cc=='[' ){
			                      while( !finDeSequence() &&  !estSep((char)cc) && this.cc!=']'   ){
			                 	 	  this.cc = this.isr.read();
			                      }
			                      if(!finDeSequence()) this.cc = this.isr.read();  //Pour ne pas prendre ']' et me positionner correctement a la position d'apres
			                  }
			                  
			                  //Cette fois ci, on ne recupere pas l'indice dans la tableHash car elle est la meme grace au Hashcode
			                  while( !finDeSequence() && estSep((char)cc) ){
		                 	      this.cc = this.isr.read();
		                     }
		                     
			                  while( !finDeSequence() &&  !estSep((char)cc) && !((char)cc=='[')  &&  !((char)cc==']')){
			             	 	indice = indice+(char)this.cc;
			             	 	this.cc = this.isr.read();
			                  }
		                     tab[1]=indice;
		         }
	              else{ 
	                  if( !finDeSequence() && this.cc=='[' ){
			              while( !finDeSequence() &&  !estSep((char)cc) && this.cc!=']'   ){
			         	 	  this.cc = this.isr.read();
			              }
			              if(!finDeSequence()) this.cc = this.isr.read();  //Pour ne pas prendre ']' et me positionner correctement a la position d'apres
			          }
			          
			          else if(!finDeSequence() && this.cc==']' ){
			              this.cc = this.isr.read();  //Pour ne pas prendre ']' et me positionner correctement a la position d'apres
			          }tab[0]=""; tab[1]="";
	             }
	      }
	   
		      
		      
		      
		      
		  }catch (IOException io) {
            throw new RuntimeException("Erreur: avancer(String indice, String occ,String doc) LecteurDocumentTableHash");
        
        }
    
    } 
    
    public boolean finDeSequence(){
         return (this.cc == -1);
     }

    public String elementCourant(){
     		return this.mot;
    }
                                      
    public boolean estSep(char c){
        return (c=='\n')||(c==' ')||(c=='\'')||(c=='.')||(c==',')||(c=='-')||(c=='_')||(c=='&')||(c=='(')||(c==')')||(c=='!')||(c=='"')||(c=='%')||(c=='\t')||(c==';')||(c==':')||(c=='?')||(c=='*')||(c=='/')||(c=='+')||(c=='=')||(c=='|')||(c=='{')||(c=='}')||(c=='#')||(c=='`')||(c=='^')||(c=='[')||(c==']')||(c=='<')||(c=='>')||(c=='~')||(c=='²')||(c=='°')||(c=='§')||(c=='¤')||(c=='@')||(c=='µ')||(c=='\\');
    
     } 
     
}
