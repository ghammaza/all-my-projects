package inf353;
import java.io.*;
import java.lang.Comparable;
public class Recherche{

    //attributs
    private int D_MAX;//ndoc max
    private int T_MAX;//nterm max
    private MatriceIndex mat;
    private AccesSequentielModele1<String> l;
    private Dictionnaire d;
    private MatriceIndex M;
    private Indexeur ind;
    private String Rep="./src/test/ressource/corpus";
    
    //constructeurs
    public void Rechercher(int nb_doc,int nb_term, String  requete){
        int i_mot=-1;
        boolean b=true;
        int nd_R=1;
        int nt_T=100;
        String mot="";
        l = new LecteurDocumentNaif(requete);
		l.demarrer();
		while(!l.finDeSequence())
		{
		    l.avancer();
		    if(!l.finDeSequence()){
		       mot=l.elementCourant();
		       d.ajouterMot(mot);
	        }
		}
		
		System.out.println("nbMots d :"+d.nbMots());
		//d.afficher();
		System.out.println("-----------------------------------------------------------------------------------------------");
        apparait_nb_fois(nb_doc);
        System.out.println("-----------------------------------------------------------------------------------------------");
        nb_mot_par_doc(nb_doc,Rep);
        System.out.println("-----------------------------------------------------------------------------------------------");

    }
	
	public void apparait_nb_fois(int nb_doc){
            int i=0;
            int i_mot=0;
		    while(i!=d.nbMots())
		    {
		            
		            int n_d=0;
		            int n_t=0;
		            int somme=0;
		            i_mot=d.indiceMot(d.motIndice(i));
	                if(i_mot!=-1){
		                while(n_d!=nb_doc){
	                        somme = somme + M.val(n_d, i_mot);
	                        n_d++;
		                }
		            }
		            System.out.println("Il apparait "+somme+" fois le mot "+d.motIndice(i));
		            i++;
		    }
        }

	public void nb_mot_par_doc(int nb_doc, String rep){
        int  j=0;
        String [][] tab;
        tab = new String[nb_doc][2];
        int i_mot=0;
        File repertoire = new File(rep);
        File[] files=repertoire.listFiles();
        String doc="";
        File f;
        for(int cpt=0;cpt<nb_doc;cpt++){
            f= files[cpt];
            doc = f.toString();
            tab[cpt][0]=doc;
        }
        while(j!=nb_doc){
                int k=0;
                int somme=0;
                while(k!=d.nbMots())
                {
                i_mot=d.indiceMot(d.motIndice(k));
                if( i_mot!=-1 && M.val(j, i_mot)!=0) somme ++;
                    k++;
                }
                tab_insertion(tab,somme+"",j);
                f= files[j];
                doc = f.toString();
                System.out.println("Il y a "+somme+" mots de la requete sur "+d.nbMots()+" qui apparait dans le Fichier "+doc);
                j++;
        }
        System.out.println("-----------------------------------------------------------------------------------------------");
        affiche_pertinence(tab, nb_doc);
    }

    public void tab_insertion(String tab[][],String ec, int n){
                int v=n;
            while(v!=0 && ec.compareTo(tab[v-1][1])<0   ){
                tab[v][1]=tab[v-1][1];
                v=v-1;
            }
            tab[v][1]=ec;
    }

    public void affiche_pertinence(String tab[][], int nb_doc){
        for(int cpt=nb_doc-1;0<=cpt;cpt--)  System.out.println(tab[cpt][0] +" "+tab[cpt][1]);
    }
        
    public static void main (String []args) {
            int nb_doc=10;
            int nb_term=900;
            Recherche Rech = new Recherche();
            /*
            ind =        new Indexeur(nb_doc,nb_term,Rep);
            m = new MatriceIndexNaive(nb_doc,nb_term);
            m =   ind.Remplie_matrice(nb_doc,nb_term,Rep);
            */
            String Req="./src/main/java/inf353/Requete";
            Rech.Rechercher(nb_doc,nb_term,Req);
    }
}
/*
    private int D_MAX;//ndoc max
    private int T_MAX;//nterm max
    private MatriceIndexNaive mat;
    private LecteurDocumentNaif l;
    private DictionnaireNaif d;
    private MatriceIndex m;
    private Indexeur ind;
    private String Rep="./src/test/ressource/corpus";
    private String Req="./src/main/java/inf353/Requete";
    
    */
