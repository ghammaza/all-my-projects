package inf353;
import java.lang.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.System;
import java.lang.System.*;
import java.nio.*;

//package com.mkyong.file;


public class  IndexeurIntelligent{

    //attributs
    public  int D_MAX;
    public  int T_MAX;
    public   MatriceIndex mat;
    public   Dictionnaire d;
    public   File[] Doc;
    
    public   final int N_MAX =51599 ; //nbr premiers pour reduire le nbr de collisions
    public   int nbMotH; //nbr t'elements ajoutés a la table
    public   int lgH;
    public   String                    nomDoss;
    public   String           nomMatriceCreuse;
    public   String nomDictionnaireIntelligent;
    public   String                  LastModif;
    public   String                datefichier;
    
    
    //Constructeurs
    public  IndexeurIntelligent(int nbd, int nbt,String nomD,String LastM) {
        this.LastModif = LastM;
        this.nomDoss = nomD;
        this.T_MAX=nbt;  
        this.D_MAX=nbd;  
        this.mat = new MatriceIndexCreuse(this.T_MAX);
        this.d = new DictionnaireIntelligent(T_MAX); 
                  this.nomMatriceCreuse = "./src/test/ressource/SAUVER/matriceCreuse.txt-"+nbd+"_"+nbt+"_"+LastM;
        this.nomDictionnaireIntelligent = "./src/test/ressource/SAUVER/DictionnaireIntelligent.txt-"+nbd+"_"+nbt+"_"+LastM;
                       this.datefichier = "./src/test/ressource/SAUVER/DATE.txt-"+nbd+"_"+nbt+"_"+LastM;
    }

    public void Indexation()   throws FileNotFoundException    {
      AccesSequentielModele1<String> l;
            try{ 
                    int nbM=0,num_doc=0,i_mot=-1;
                    File repertoire = new File(this.nomDoss);
                    this.Doc=repertoire.listFiles();
                    //Doc = new File[90000];
                    int nbD=Doc.length;
                    String doc="";
                    File f;
                    while(num_doc<nbD){
                            f=  Doc[num_doc];
                            doc = f.toString();
                            String mot="";
                            i_mot=-1;
                            l = new LecteurDocumentNaif(doc);
                       		l.demarrer();
                       		while(!l.finDeSequence())
                       		{
                       		      mot=l.elementCourant();   
	                              i_mot=d.indiceMot(mot);
	                              if(i_mot==-1) {
	                                    d.ajouterMot(mot);
	                                    i_mot=d.indiceMot(mot);
	                              }
	                              mat.incremente(num_doc,i_mot);
		                          l.avancer();
                       		}
                       		num_doc ++;
                       		if((num_doc%5000)==0){
                       		   System.out.println("num_doc  "+num_doc +"   nbMots()  "+d.nbMots() );
                       		}
                    }
                    
                    System.out.println("Sauvegarde de la Table Hash ");
                    this.th.afficher(this.nomTableHash); 
                    
                    System.out.println("Sauvegarde de la Matrice Creuse ");
                    mat.sauver(this.nomMatriceCreuse);
                    
                    System.out.println("Sauvegarde du Dictionnaire Intelligent");
                    this.d.afficherfichier(this.nomDictionnaireIntelligent);
                    
                    
                    System.out.println("d.nbMots()  "+d.nbMots());
         }catch (IOException io){
            throw new RuntimeException("Erreur: Indexation()  IndexeurIntelligent");
        }
    
    }
   
   
   public String DATE(String m,long date_deb){
      long t =  System.currentTimeMillis() -  date_deb;
      Date mainteant = new Date();
      DateFormat mediumDateFormat = DateFormat.getDateTimeInstance(DateFormat.MEDIUM,DateFormat.MEDIUM);
      
      long heure    =     (t/3600000);
      long min      =  ( (t%3600000)/60000);
      long sec      =  ( (t%3600000)%60000)/1000;
      long Millisec =  ( (t%3600000)%60000)%1000;
      System.out.println(m+"  "+mediumDateFormat.format(mainteant)+" soit "+t+" Milliseconde Soit  "+heure+" Heures "+min+" Minutes "+sec+" Secondes "+Millisec+" Millisecondes");
      return t+" ";
   } 
   
     public int Nb_doc_rep(){
        File repertoire = new File(this.nomDoss);
        Doc=repertoire.listFiles();
        System.out.println("Doc.length   "+Doc.length);
    	return  Doc.length;
    }
   /*
     public void Remplir_DICO()   throws FileNotFoundException    {
      AccesSequentielModele1<String> l;
            try{ 
                    String mot="";
                    l = new LecteurDocumentNaif(nomDictionnaireIntelligent);
               		l.demarrer();
               		while(!l.finDeSequence())
               		{
               		      mot=l.elementCourant();   
                          d.ajouterMot(mot);
                          l.avancer();
               		}
               		System.out.println("Recuperation du Dictionnaire");
                    this.d.afficherfichier(nomDictionnaireIntelligent+"Recup");
         }catch (IOException io){
            throw new RuntimeException("Erreur: Remplir_DICO()  IndexeurIntelligent "+io);
        }
     }
   */
   public void Remplir_MatriceCreuse()   throws FileNotFoundException    {
            AccesSequentielModele1<String> l;
            try{ 
                    int cpt=0;
                    String anc_ind="";
                    String[] tab = new String[3];
                    l = new LecteurDocumentMatrice(nomMatriceCreuse);
               		l.demarrer();
               		tab[0]="";tab[1]="";tab[2]="";
               		while(!l.finDeSequence() )  {
                                l.avancer(tab,tab[0]);
                                if( !tab[2].equals("")){
                                    //ind(0) occ(1) doc(2)
                                    mat.affecte(Integer.parseInt(tab[2]),Integer.parseInt(tab[0]),Integer.parseInt(tab[1]));
                                    //((MatriceIndexCreuse)MAT).tab[Integer.parseInt(tab[0])].ajoutQueue(Integer.parseInt(tab[1]),Integer.parseInt(tab[2]));
                                }
                                if(!anc_ind.equals(tab[0])){ anc_ind=tab[0]; cpt++; System.out.println("n°"+cpt+" et cpt<TMAX "+(cpt<this.T_MAX) ); }
                                if((cpt%1000)==0){
                                     cpt++;
                       		         System.out.println("n°"+cpt+" et cpt<TMAX "+(cpt<this.T_MAX) );
                       		    }
               		}
                    
                    System.out.println("Recuperation de la MatriceCreuse");
                    mat.sauver(nomMatriceCreuse+"Recup");
         }catch (IOException io){
            throw new RuntimeException("Erreur: Remplir()  IndexeurIntelligent "+io);
        }
     }
   
   
   

     public void Remplir_DICO()   throws FileNotFoundException    {
                    int nbM=0,num_doc=0,i_mot=-1,i=0;
                    String indice ="";
                    String[] tab = new String[3];
                    l = new LecteurDocumentTableHash(nomDictionnaireIntelligent);
               		l.demarrer();
               		while(!l.finDeSequence() && i<=d.nbMots())
               		{
                        l.avancer(tab,indice);
                        if( !tab[0].equals("")){
                            //MOT(0) indice(1)
                            //th.put(tab[0],Integer.parseInt(tab[1]));
                            d.ajouterMot(tab[0]);
                        }
               		}
               		System.out.println("Recuperation de la Table de Hachage");
                    d.afficher(nomDictionnaireIntelligent+"Recup");
                    
                    
                    AccesSequentielModele1<String> l;
            try{ 
                    String mot="";
                    l = new LecteurDocumentNaif(nomDictionnaireIntelligent);
               		l.demarrer();
               		while(!l.finDeSequence())
               		{
               		      mot=l.elementCourant();   
                          d.ajouterMot(mot);
                          l.avancer();
               		}
               		System.out.println("Recuperation du Dictionnaire");
                    this.d.afficherfichier(nomDictionnaireIntelligent+"Recup");
         }catch (IOException io){
            throw new RuntimeException("Erreur: Remplir_DICO()  IndexeurIntelligent "+io);
        }
     }
     
     
     public static void main (String []args) {
       try{
              String Rep = "./src/test/ressource/corpus";
              int nbd = Integer.parseInt(args[0]);
              int nbt = Integer.parseInt(args[1]);
                   if(args[2].equals("c" )){ Rep = "./src/test/ressource/corpus";}
              else if(args[2].equals("ce")){ Rep = "/ext/INF/354_projet/corpus/";}
              else if(args[2].equals("ff")){ Rep = "/ext/INF/354_projet/french_flat";}
              else if(args[2].equals("ffo")){ Rep = "/home/orandr/Bureau/Repertoire/french_flat";}
              else if(args[2].equals("f" )){ Rep = "/ext/INF/354_projet/french";}
              else if(args[2].equals("e5")){ Rep = "/ext/INF/354_projet/echantillon_5000";}
              else if(args[2].equals("e1")){ Rep = "/ext/INF/354_projet/echantillon_100";}
              else  Rep = "./src/test/ressource/corpus";
               
               File file = new File(Rep);
               IndexeurIntelligent ind = new  IndexeurIntelligent(nbd,nbt,Rep,file.lastModified()+"");
               long date_debut =  System.currentTimeMillis();
               ind.DATE("Debut de l'Indexation : ",date_debut);
               ind.Indexation();
               ind.DATE("Fin   de l'Indexation : ",date_debut);
               
               System.out.println("IndexeurIntelligent Fin");
               System.out.println("");
               
               
                       
        }catch (IOException io){ 
            throw new RuntimeException("Erreur: main existance du fichier() IndexeurIntelligent");
        }
    }
}
