package inf353;
import java.io.*;
import java.lang.*;
import java.util.*; 

public class TableHash{

//ATTRIBUTS:

     public  CelluleH[]  tableHash;
     public final int N_MAX =32693 ; //nbr premiers pour reduire le nbr de collisions
     public int nbMotH; //nbr t'elements ajoutés a la table
     public int lgH;
     
//CONSTRUCTEUR:

     public TableHash(){
          this.tableHash = new  CelluleH[N_MAX];  //Pour le gros corpus ( taille constante )
          this.lgH = 0;
          this.nbMotH = 0;
     }  
     
     public TableHash(int size){
          this.tableHash = new  CelluleH[size];  // pour les jeux de test ( size a definir )
          this.lgH = 0;
          this.nbMotH = 0;
     }  
     
//METHODES :


     
     public int val(String s){    
            CelluleH cc =  tableHash[hash(s)];
            while ( cc != null && !MemeMot(s,cc.motH) ){
                cc = cc.succ; 
            }
            if (cc != null) return cc.indiceH;
            else return -1; 
     }
     


     //Fonction de hachage et compression : renvoie l'indice associé a la chaine S sur [0..N_MAX]
     public int hash(String s){
          return Math.abs(s.toLowerCase().hashCode() %  tableHash.length);   
     }
     
     
     //Ajoute une cellule contenant la chaine S si elle n'existe pas deja
     public void put(String s, int i) {   
          //if (!containH(s)){
                CelluleH c =  tableHash[hash(s)];   // on recupere la sequence chainée 
                CelluleH cS = new   CelluleH(s,i,c);    // on ajoute en tete une cellule pour s
                tableHash[hash(s)] = cS;          // on remet a jour la tete de la sequence 
                nbMotH++;
         /* }
          else {
               System.out.println(" Put : Argument ("+s+") existe deja a l'indice "+i+" !");
          }*/
     }  


     //renvoie la cellule contenant la chaine S
     public  CelluleH get(String s) throws RuntimeException {
        if (!containH(s)){   
             throw new RuntimeException ("Get : Argument (String s) introuvable ");
        }
        else {  
              CelluleH cc =  tableHash[hash(s)];
              CelluleH cp = null;
              CelluleH n = null;
             while ( cc != null && !(s.equals(cc.motH))) {
                 cp = cc;
                 cc = cc.succ;  
             }
             if (cc!= null){
               if (cp != null){
                    n = cp;
               }
               else {
                    n = cc;
               }
             }
             return n;
        }
     }


     //supprimer la cellule contenant la chaine S
     public void remove(String s) { 
          if (containH(s)){  
              CelluleH cc =  tableHash[hash(s)];
              CelluleH cp = null;
             while ( cc != null && !MemeMot(s,cc.motH)   ){
                 cp = cc;
                 cc = cc.succ;  
             }
             if (cc != null){ // S trouvé
                if (cp != null){
                    cp.succ = cc.succ;
                }
                else {  //suppression en tete
                     tableHash[hash(s)] = cc.succ;
                }
             }
             nbMotH--;
         }
         else {
             System.out.println(" Remove : Argument ("+s+") introuvable !");
         }
           
     }
     
    public boolean MemeMot(String m1,String m2){
            /*int i = 0;
            if(m1.length() == m2.length() ){
                while(  i<m1.length()  && java.lang.Character.toLowerCase(m1.charAt(i))==java.lang.Character.toLowerCase(m2.charAt(i))  ){
                    i++;
                }
                return ( i>=m1.length() ) ;
            }
            return false;*/
            return m1.toLowerCase().equals(m2.toLowerCase());
    } 
    
    
    

    // Vrai si la tableH contient la chaine S
    public boolean containH(String s){  
        CelluleH cc =  tableHash[hash(s)];
        while ( cc != null && !MemeMot(s,cc.motH)){
            cc = cc.succ; 
        }
        return (cc != null);
    }
    
     //Afficher les mots contenu dans la tableH : chaque sequence chainée dans une ligne
     public void afficher(){
          int i =0;
          while (i !=  tableHash.length){
               CelluleH cc =  tableHash[i];
              System.out.print(" ["+i+"] ");
              while(cc != null){
                  System.out.print(" --> ("+cc.motH+" | "+cc.indiceH+") ");
                  cc = cc.succ;
              }
              System.out.println("");
              i++;
          }
     }
     
     //Afficher les mots contenu dans la tableH : chaque sequence chainée dans une ligne
     public void afficher(String nomTabH){
        try{
          PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(nomTabH))));
          int i =0;
          while (i !=  tableHash.length){
               CelluleH cc =  tableHash[i];
              pw.print(" ["+i+"] ");
              while(cc != null){
                  pw.print(" --> ("+cc.motH+" | "+cc.indiceH+") ");
                  cc = cc.succ;
              }
              pw.println("");
              i++;
          }
          pw.close();
          
        }catch (IOException io){
            throw new RuntimeException("Erreur: Indexation_2()  IndexeurIntelligent"+io);
        }
     }
     
     
     //renvoie la taille de la tableH : c.a.d le nombre de mots contenus 
     public int getsize(){
          
          return this.nbMotH;
          
          
     }
    
    
     public static void main (String [] args){
           TableHash th= new TableHash (13);
           th.put("Vald",0);
           th.put("Kopp",1);
           th.put("Hayce",2);
           th.put("Rohff",3);
           th.put("Squadra",4);
           th.put("VoltFace",5);
           th.put("DizzyDros",6);
           th.put("Niro",7);
           th.put("Damso",8);
           th.put("LilPump",9);
           th.put("Kodak",10);
           th.put("gucciMane",11);
           System.out.println("nb mot :"+th.getsize());
           th.afficher();
           System.out.println("");
           System.out.println("contient Niro :"+th.containH("Niro"));
         
           System.out.println("TableHash Fin");
           System.out.println("");
           
           
           
     }
 } 

