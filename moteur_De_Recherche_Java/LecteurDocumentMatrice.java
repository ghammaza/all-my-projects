package inf353;
import java.io.*;

/**
 * Interface générique d'un accès séquenciel de modèle 1 d'élemnts de type T
 */
public class LecteurDocumentMatrice implements AccesSequentielModele1<String> {
    private String mot; // texte parcouru.
    private String nomFic;
    private int cc;
    private InputStream ips;
    private InputStreamReader isr; 
    
    
    
    public LecteurDocumentMatrice(String nom){
    		this.mot ="";
    		this.nomFic = nom;
    }

    /*
     * Initialisation du parcours.
     */
    public void demarrer(){
        
     try{
          this.ips=new FileInputStream(this.nomFic);
	      this.isr = new InputStreamReader(this.ips);
	 	  this.mot ="";
	 	  this.cc =' ';
	 	  //avancer();
     }catch (IOException io) {
            throw new RuntimeException("Erreur: demmarer() LecteurDocumentMatrice");
        
     }
      
    }    
    
    /*
    
    public void avancer(String[] tab, String ind){  
        try{
            int cpt=0;
            String indice = "";
            String MOT = "";
            String occ = "";
            String doc = "";
            
              //if(!finDeSequence()){
              
		         this.cc = this.isr.read();
		  
		         while( !finDeSequence() && this.cc!='(' ){
		     	      this.cc = this.isr.read();
		         }
			      if( !finDeSequence() && this.cc=='(' ){
			          this.cc = this.isr.read();
			          while( !finDeSequence() &&  this.cc!=' ' && this.cc!='|' && this.cc!=')'){
			              occ = occ + (char)this.cc;
			              //System.out.println("occurence     "+(char)this.cc);
			              //System.out.println("nbocc    "+occ);
			     	 	  this.cc = this.isr.read();
			     	 	  System.out.println("-occ "+occ);
			          }
			      }
			      
			      while( !finDeSequence() && estSep((char)cc) ){
		     	      this.cc = this.isr.read();
		     	      //System.out.println("sep milieu :      "+(char)this.cc);
		         }
			      while( !finDeSequence() &&  !estSep((char)cc) && this.cc!=')'){
			              doc = doc + (char)this.cc;
			              System.out.println("-doc"+doc);
			              //System.out.println("document     "+(char)this.cc);
			              //System.out.println("ndoc    "+doc);
			     	 	  this.cc = this.isr.read();
			      }
			      System.out.println("DEDANS  ");
			 // }
			  System.out.println("-------occ--------"+occ);
			  System.out.println("-------doc--------"+doc);
			  tab[0]=occ; tab[1]=doc;
		  }catch (IOException io) {
            throw new RuntimeException("Erreur: avancer(String indice, String occ,String doc) Lecteurmatrice");
        
        }
    
    } */
    
    
    public boolean FinLigne(){
     System.out.println("(this.cc == n) "+(this.cc == '\n') );
        return (this.cc == '\n');
    }
    
    
    public void avancer(){  
        this.mot = "";
        
        try{
            if(!finDeSequence()){
		         this.cc = this.isr.read();
		  
		         while( !finDeSequence() && estSep((char)cc) ){
		     	      this.cc = this.isr.read();
		         }
		     
			      while( !finDeSequence() &&  !estSep((char)cc)){
			 	 	this.mot = this.mot+(char)this.cc;
			 	 	this.cc = this.isr.read();
			      }
		      }
		  }catch (IOException io) {
            throw new RuntimeException("Erreur: avancer() LecteurDocumentMatrice");
        
        }
    
    } 
    
    
    
    
    /*
    public void avancer(String[] tab, String ind, int nbM){  
        //this.mot = "";
        String indice = "";
        String occ = "";
        String doc = "";
        try{
            if(!finDeSequence()){
		         this.cc = this.isr.read();
		  
		         while( !finDeSequence() && estSep((char)cc) ){
		     	      this.cc = this.isr.read();
		         }
			      if( this.cc=='[' ){
			          if(!finDeSequence()) this.cc = this.isr.read();  //Pour ne pas prendre '[' et me positionner correctement a la position d'apres
			          while( !finDeSequence() &&  !estSep((char)cc) && this.cc!=']'   ){
			              indice = indice+(char)this.cc;
			     	 	  this.cc = this.isr.read();
			          }
			          if(!finDeSequence()) this.cc = this.isr.read();  //Pour ne pas prendre ']' et me positionner correctement a la position d'apres
			      }
                 if( indice.equals(ind)) tab[1]=ind; 
                 else tab[1]=indice;
			      System.out.println("   tab[1]  "+tab[1]);
			      
		      }
		  }catch (IOException io) {
            throw new RuntimeException("Erreur: avancer() LecteurDocumentNaif");
        
        }
    
    } */
    
    
    
   public void avancer(String[] tab, String ind){  
        try{
            int cpt=0;
            String indice = "";
            String MOT = "";
            String occ = "";
            String doc = "";
            
            
            if(!finDeSequence()){
		         this.cc = this.isr.read();
		         while( !finDeSequence() && estSep((char)cc) ){
		     	      this.cc = this.isr.read();
		         }
			      if( !finDeSequence() && this.cc=='[' ){
			      
			     	  this.cc = this.isr.read(); //Pour ne pas prendre '[' et me positionner correctement a la position d'apres
			          while( !finDeSequence() &&  !estSep((char)cc) && this.cc!=']'   ){
			              indice = indice + (char)this.cc;
			     	 	  this.cc = this.isr.read();
			          }
			          if(!finDeSequence()  && this.cc!=']') this.cc = this.isr.read();  //Pour ne pas prendre ']' et me positionner correctement a la position d'apres
			      }
			      
			      else if(!finDeSequence() && this.cc==']' ){
			          this.cc = this.isr.read();  //Pour ne pas prendre ']' et me positionner correctement a la position d'apres
			      }
                  if( !indice.equals("")){ tab[0] = indice ;}
                  else { tab[0]=ind;}
			      //Cette fois ci, on ne recupere pas l'indice dans la matrice car elle est la meme
			      while( !finDeSequence() && estSep((char)cc) ){
		     	      this.cc = this.isr.read();
		         }
		         
			     if(!finDeSequence() && this.cc==']' ){
			              this.cc = this.isr.read();  //Pour ne pas prendre ']' et me positionner correctement a la position d'apres
			     }
		         if(!((char)cc=='[')){
		                      
		                      while( !finDeSequence() && estSep((char)cc) ){
		                 	      this.cc = this.isr.read();
		                     }
			                  while( !finDeSequence() &&  !estSep((char)cc) &&  !((char)cc==']') ){
			             	 	occ = occ+(char)this.cc;
			             	 	this.cc = this.isr.read();
			                  }
		                     tab[1]=occ;
		                     
			                  while( !finDeSequence() &&   estSep((char)cc) && !((char)cc=='[')  ){
		                 	      this.cc = this.isr.read();
		                     }
		                     
		                     if( this.cc=='[' ){
			                      while( !finDeSequence() &&  !estSep((char)cc) && this.cc!=']'   ){
			                 	 	  this.cc = this.isr.read();
			                      }
			                      if(!finDeSequence()) this.cc = this.isr.read();  //Pour ne pas prendre ']' et me positionner correctement a la position d'apres
			                  }
			                  
			                  //Cette fois ci, on ne recupere pas l'indice dans la tableHash car elle est la meme grace au Hashcode
			                  while( !finDeSequence() && estSep((char)cc) ){
		                 	      this.cc = this.isr.read();
		                     }
		                     
			                  while( !finDeSequence() &&  !estSep((char)cc) && !((char)cc=='[')  &&  !((char)cc==']')){
			             	 	doc = doc+(char)this.cc;
			             	 	this.cc = this.isr.read();
			                  }
		                     tab[2]=doc;
		         }
	              else{ 
	                  if( !finDeSequence() && this.cc=='[' ){
			              while( !finDeSequence() &&  !estSep((char)cc) && this.cc!=']'   ){
			         	 	  this.cc = this.isr.read();
			              }
			              if(!finDeSequence()) this.cc = this.isr.read();  //Pour ne pas prendre ']' et me positionner correctement a la position d'apres
			          }
			          
			          else if(!finDeSequence() && this.cc==']' ){
			              this.cc = this.isr.read();  //Pour ne pas prendre ']' et me positionner correctement a la position d'apres
			          }
			          tab[0]=""; tab[1]="";
	             }
	          }
	      }catch (IOException io) {
            throw new RuntimeException("Erreur: avancer(String indice, String occ,String doc) Lecteurmatrice");
        
        }
    
    } 
    
   
    public boolean finDeSequence(){
         return (this.cc == -1);
     }

    public String elementCourant(){
     		return this.mot;
    }
                                      
    public boolean estSep(char c){
        return (c==' ')||(c=='(')||(c==')')||(c=='\n')||(c=='-')||(c=='>')||(c=='|');
    
     } 
     
}
