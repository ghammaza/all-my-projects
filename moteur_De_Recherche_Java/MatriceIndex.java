package inf353;
import java.io.File;
import java.io.*;
import java.lang.Object;
import java.io.FileNotFoundException;
public interface MatriceIndex{


    public abstract void sauver2(PrintWriter pw, int i);
    
    
     //attention abstract n'y etait pas pour sauver
    public abstract void sauver(String nomDeFichier) throws FileNotFoundException;

    /**
     * retourne le nombre d'occurences du terme numéro nterm dans le document numéro ndoc.
     * @param  ndoc  le numéro du document
     * @param  nterm le numéro du terme
     * @return       le nombre d'occurences du terme dans le document
     */
    public abstract int val(int ndoc, int nterm);

    /**
     * Ajoute 1 au nombre d'occurences du terme numéro nterm dans le document numéro ndoc.
     * @param  ndoc  le numéro du document
     * @param  nterm le numéro du terme
     */
    public abstract void incremente(int ndoc, int nterm);

    /**
     * affecte à val le numéro d'occurences du terme numéro nterm dans le document numéro ndoc.
     * @param  ndoc  le numéro du document
     * @param  nterm le numéro du terme
     * @param val    la nouvelle valeur du nombre d'occurence
     */
    public abstract void affecte(int ndoc, int nterm, int val);
}
