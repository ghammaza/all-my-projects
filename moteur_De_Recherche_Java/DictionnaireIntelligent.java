package inf353;
import java.lang.*;
import java.io.*;

public class DictionnaireIntelligent implements Dictionnaire {
//public class DictionnaireIntelligent{
    private CelluleD[] tab;//tableau de char
    private int Nb_MAX;//nbmax du tableau
    private  TableHash th;
    private int lg;//nb mot dans le tabelau
    
    public DictionnaireIntelligent(int TAILLE_MAX){
        this.Nb_MAX=TAILLE_MAX;
        this.tab= new CelluleD[TAILLE_MAX];
        this.lg =0;
        th= new TableHash(TAILLE_MAX);
    }
     

     
     
      //mot par ligne
    public void afficherfichier(String nomDeFichier) throws FileNotFoundException {
        int i = 0;
        PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(nomDeFichier))));
        while(i < this.lg ){
             pw.println(motIndice(i));
             i++;
        }
        pw.close() ;
   }
      
      
     //mot par ligne
    public void afficher2(){
        int i = 0;
        System.out.println("DictionnaireIntelligent : ");
        while(i < this.lg )
        {
             System.out.println(motAffich(i));
             //System.out.println(motIndice(i));
             i++;
        }
        System.out.println(""); 
        System.out.println("Affichage de la table de Hachage : "); 
        th.afficher(); 
     }


      //mot par mot
      public void afficher(){
        int i = 0;
        System.out.println("DictionnaireIntelligent : ");
        while(i < this.lg )
        {
             System.out.println(motAffich(i));
             //System.out.print(motIndice(i)+" ");
             i++;
             if(i%10==0)  System.out.println(""); 
        }
        System.out.println("");  
        System.out.println("Affichage de la table de Hachage : "); 
        th.afficher(); 
     }
    
    
    /**
     * é.i: qcq
     * é.f.: le dictionnaire est vide.
     //nb mot du dictionnaire est vide
     */
     public void vider(){
          this.lg =0;
     }
    
    
    /**
     * renvoie vrai ssi m est dans le dictionnaire.
     * @param m
     * @return
     */
    public boolean contient(String m){
        return th.containH(m);
    }
    
     /**
     * é.i.: le dictionnaire contient D0 (un ensemble de this.Nb_MAX mots).
     * é.f.: si m appartient à D0; le dictionnaire est inchangé
     *       sinon, le dictionnaire contient D0 U {m}
     * 
     * @param m
     */
    public void ajouterMot(String m){
       if(this.lg<this.Nb_MAX){
            if(!th.containH(m)) th.put(m,this.lg);
          //CelluleD c =  tab[th.hash(m)];   // on recupere la sequence chainée   val(String s)
            CelluleD c =  tab[th.val(m)];   // on recupere la sequence chainée
            this.tab[th.val(m)] = new CelluleD(m,c);
            this.lg++;
        }
    }
 
  
    
        /**
     * é.i: Dictionnaire contient D0
     * é.f.: si m est dans D0, le Dictionnaire contient D0 | {m}, sinon le dictionnaire est inchangé.
     * @param m
     */
     public void supprimer(String m){
         if(0<=this.lg && contient(m)){
             CelluleD cc =  tab[th.val(m)];
             CelluleD cp = null;
             while ( cc != null && !(m.equals(cc.mot))){
                 cp = cc;
                 cc = cc.succ;  
             }
             if (cc != null){ // S trouvé
                if (cp != null){
                    cp.succ = cc.succ;}
                else {  //suppression en tete
                     tab[th.val(m)] = cc.succ;
                }
             }
             this.lg--;
        }     
             
     }

     /**
     * renvoie le nombre de mots de m.
     * @return
     //this.lg etant le nb mot du dictionnaire alors je le retourne directement)
     */
     public int nbMots(){
            return this.lg;
     }
    
    
     /**
     * vrai ssi il existe m dans D0 tel que il exist u tq m = p.u
     * 
     * (vrai si un mot de D0 commence par p)
     * @param p le préfixe recherché
     * @return
     */
     public boolean contientPrefixe(String p){
                int i=0;
                CelluleD cc;
                int j=0;
                String Mot = "";
                while(i<this.lg && j<p.length() && p.length()<40 && !(p.isEmpty()) )
                {
                   cc = this.tab[i];
                   Mot = cc.mot;
                   while ( cc != null && j<p.length() && j<Mot.length()){
                       if(java.lang.Character.toLowerCase(Mot.charAt(j))== java.lang.Character.toLowerCase(p.charAt(j))  )
                        j++;
                       else {
                          j=0;
                          if(cc!=null) cc = cc.succ; 
                       }
                   }
                   i++;
               }
                if(p.isEmpty()) return false; 
                return (j==p.length() );
       
       }
    
     /**
     * renvoie la chaîne de caractères s telle que 
     *  s est dans D0 
     *  et m commence par s
     *  et il n'existe pas de chaîne s' ds D plus longue que s tq m commence par s.
     *  
     * @param mot
     * @return
     */
    public String plusLongPrefixeDe(String mot){
       int i=0,j=0,k=0,k_new=0;
        CelluleD cc;
        String Mot = "";
        while(i<this.lg && j<mot.length() && mot.length()<40 && !(mot.isEmpty()) )
        {
           cc = this.tab[i];
           Mot = cc.mot;
           while ( cc != null && j<mot.length() && j<mot.length() && j<Mot.length() && j<Mot.length()){
               if(java.lang.Character.toLowerCase(Mot.charAt(j))== java.lang.Character.toLowerCase(mot.charAt(j))  ){
                   j++;
                   k_new++;
                   if(k<k_new) k=k_new;
               }
               else {
                  if(k<k_new) k=k_new;
                  k_new=0;
                  j=0;
                  if(cc!=null) cc = cc.succ; 
               }
           }
           i++;
       }
         //static String copyValueOf(char[] data, int offset  , int count   )
         //-------------------------(tableau  s , int decalage, int compteur)
         //Construit une chaîne de caractères à partir d’une partie de tableau de caractères 
         
        if(k>0){
            String MOT="";
            for(int L=0;L<k;L++) MOT += mot.charAt(L);
            return MOT;
        }
        return "";
    }
   
     
      public String motAffich(int i){
         if(0<=i && i< this.Nb_MAX){
                String Mot = "";
                CelluleD cc =  this.tab[i];
                System.out.print(" ["+i+"] ");
                while ( cc != null ){
                    Mot = Mot +"--> "+ cc.mot;
                    cc = cc.succ; 
                }
                return Mot;
        }else{
            return "";
       }
      
      }
      
        /**
     * renvoie le mot associé à l'entier i;
     * @param i l'indice du mot à renvoyer
     * @return
     */
      public String motIndice(int i){
         String Mot = "";
         if(0<=i && i< this.Nb_MAX){
                CelluleD cc =  this.tab[i];
                while ( cc != null ){
                    Mot = cc.mot;
                    cc  = cc.succ; 
                }
                return Mot;
        }
        return Mot;
      }
    
    
       /*
    
     /**
     * renvoie l'entier associé à m;
     * @param m
     * @return
     */
     //renvoie -1 si incorrecte
     public int indiceMot(String m){
            return th.val(m);
      }
    
       
            
    
    
   public static void main(String[] arg){
            Dictionnaire d = new DictionnaireIntelligent(10); 
            System.out.println("");
            System.out.println("");
            d.ajouterMot("chouaib");
            d.ajouterMot("regis");
            d.ajouterMot("thang");
            d.ajouterMot("deLES");
            d.ajouterMot("arthur");
            d.ajouterMot("ayoub");
            d.ajouterMot("dele");
            d.ajouterMot("de");
            d.ajouterMot("de ");
            System.out.println("");
            System.out.println("");
            System.out.println("");
            System.out.println("");
            
            System.out.println("indice du mot deLE  est a la position "+(d.indiceMot("deLE")));
            System.out.println("indice du mot deLES est a la position "+(d.indiceMot("deLES")));
            d.afficher();
            //d.afficher2();
            System.out.println("");
            System.out.println("");
            System.out.println("");
            String mot = "deleguer";
            String plp =d.plusLongPrefixeDe(mot);
            System.out.println("Plus Long Prefixe de "+mot+" est  "+plp);
            System.out.println("");
            System.out.println("");
            System.out.println("");
            System.out.println("");
            String c = "DEles";
            System.out.println("Contient bien le mot "+c+"  dans le dictionnaireInt ?  "+d.contient(c));
            
            System.out.println("");
           
            System.out.println("DictionnaireIntelligent Fin");
            System.out.println("");
    }
         
}

