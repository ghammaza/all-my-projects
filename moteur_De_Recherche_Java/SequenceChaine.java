package inf353;
import java.io.*;

public class SequenceChaine {	
	public CelluleM tete;
	public CelluleM queue;
	
	public SequenceChaine() {
       this.tete = null;
       this.queue = null;
	}
	public CelluleM getTete(){
	    return this.tete;
	}
	public void setTete(CelluleM t){
	    this.tete = t;
	}
	
	public void ajoutQueue(int nbo,int nbd){
	    this.queue=new CelluleM(nbo,nbd,null);
	}
	
}
