package inf353;
import java.io.*;

//public class MatriceIndexIntelligent implements MatriceIndex{
public class MatriceIndexIntelligent{
	// attributs :
	private int [][] tab;
	private int nbT ,nbD;
		
    public MatriceIndexIntelligent(int D,int T){
	        if( 0<D  && 0<T ){	    
               this.nbD = D;
	           this.nbT = T;
	           this.tab = new int[nbT+1][nbD+1];		               
       	    }
            else{
		      System.out.println("Erreur MatriceIndexIntelligent constructeur, Rien fait ");
            }
		
    }


   public void sauver(String nomDeFichier) throws FileNotFoundException{
	int i =0;
    	int j;
    	
    	OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(nomDeFichier));
        BufferedWriter bw = new BufferedWriter(writer);
        PrintWriter pw = new PrintWriter(bw);
            
            while (i != this.nbT) {
                j=0;
                while (j != this.nbD){
                     
                             if(this.tab[i][j]<=9){
                                    pw.print(this.tab[i][j]+"     ");
                             }
                             else if(this.tab[i][j]<=99){
                                    pw.print(this.tab[i][j]+"    ");
                             }
                             else if(this.tab[i][j]<=999){
                                    pw.print(this.tab[i][j]+"   ");
                             }
                             else if(this.tab[i][j]<=9999) {
                                    pw.print(this.tab[i][j]+"  ");
                             }
                             else if(this.tab[i][j]<=99999){
                                    pw.print(this.tab[i][j]+" ");
                             }
                             j++;
           	}
   		pw.println("");
   		i++;
        }
        pw.close() ;
   }

   

   public int val(int ndoc, int nterm){
        
        if(  0<=ndoc &&  ndoc<this.nbD && 0<=nterm &&  nterm<this.nbT) {
       	     return this.tab[nterm][ndoc];
       	}
       	
        else if( !(  0<=ndoc  && ndoc < this.nbD)) {
       	     //System.out.println("Erreur MatriceIndexIntelligent  val, vous avez entré un numéro de document qui n'existe pas. ");
       	     return -1;
       }
       
       else if(!( 0<=nterm  && nterm < this.nbT)) {
       		 System.out.println("Erreur MatriceIndexIntelligent  val, vous avez entré un numéro de terme qui n'existe pas. ");
       		 return -1;
       }
       else return -1;
   }

    
    
   public  void incremente(int ndoc, int nterm){
   	if(  0<=ndoc &&  ndoc<this.nbD && 0<=nterm &&  nterm<this.nbT) {
        		  this.tab[nterm][ndoc] = this.tab[nterm][ndoc] +1;
   	}
   	
       else if( !(   0<=ndoc  && ndoc < this.nbD)) {
       	     System.out.println("Erreur MatriceIndexIntelligent incremente, vous avez entré un numéro de document qui n'existe pas. ");
       }
       
       else if( !(  0<=nterm  && nterm < this.nbT)){
       		 System.out.println("Erreur MatriceIndexIntelligent incremente, vous avez entré un numéro de terme qui n'existe pas. ");
       }
   }
    
    
    
   public  void affecte(int ndoc, int nterm, int val){
      if( 0<=ndoc  && ndoc<this.nbD ){
                if( 0<=nterm &&  nterm<this.nbT){
                    this.tab[nterm][ndoc]=val;  //ligne colonne
                }
                else{
                    System.out.println("Erreur MatriceIndexIntelligent  affecte, vous avez entré un numéro de terme qui n'existe pas. ");
                }
       			
      }
      else {
               System.out.println("Erreur MatriceIndexIntelligent  affecte, vous avez entré un numéro de document qui n'existe pas. ");
      }
   }
   
   
   
  public static void main (String [] args)  throws FileNotFoundException {
               
	            MatriceIndexIntelligent mat = new MatriceIndexIntelligent(2,10);
                mat.affecte(1,1,10);
                String doc = "copie.txt";
                mat.sauver(doc);
                System.out.println("MatriceIndexIntelligent, Notre MatriceIndexIntelligent est dans "+ doc);
                System.out.println("MatriceIndexIntelligent Fin");
                 System.out.println("");
    }
    
}

