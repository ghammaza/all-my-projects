package inf353;
/**
 * Classe représentant une cellule dans une liste chaînée.
 * La cellule contient un élément entier et une cellule suivante.
 */
public class CelluleH{
		public String motH;
		public int indiceH;
		public  CelluleH succ;
		
		/**
		 * Construit une cellule.
		 * @param v valeur de l'élément.
		 * @param suc Cellule suivante.
		 */
		public  CelluleH(String m,int indH,  CelluleH suc) {
			this.motH = m;
			this.indiceH = indH;
			this.succ=suc;
		}
		
		/**
		 * Construit une Cellule avec une valeur d'élément égale à 0 et une cellule suivante.
		 * @param suc la cellule suivante.
		 */
		public  CelluleH(CelluleH suc) {
			this.succ=suc;
		}
		
		
		/**
		 * Construit une cellule avec une valeur d'élément et aucune cellule suivante.
		 * @param v valeur entière de l'élément.
		 */
		public  CelluleH(String m,int indH) {
			this.motH = m;
			this.indiceH = indH;
		}
		
		/**
		 * Construit une Cellule avec une valeur d'élément égale à 0 et aucune cellule suivante.
		 */
		public void CelluleH() {
		  
		}
		
		/**
		 * Modifie la cellule suivante de la cellule courante.
		 * @param c Cellule suivante.
		 */
		public void setSucc(CelluleH c){
		    this.succ = c;
		}
		
		/**
		 * Retourne la cellule suivante de la cellule courante.
		 * @return La cellule suivante.
		 */
		public CelluleH getSucc(){
		    return this.succ;
		}
		
		/**
		 * Modifie la valeur de l'élément courant
		 * @param v la valeur de l'élément.
		 */
		public void setElement(String m, int c){
		    this.motH = m;
		    this.indiceH = c;
		}
		
		/**
		 * Renvoi la valeur de l'élément.
		 * @return la valeur de l'élément.
		 */
		/*public int getElement(){
		    return this.val;
		}*/
	}
