package inf353;

import java.lang.*;
import java.io.*;
import java.util.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.lang.System;
import java.lang.System.*;

public class RechercheIntelligente{

//ATTRIBUTS

	 	
		public AccesSequentielModele1<String> l;
		public Dictionnaire d_req;
		public Dictionnaire DD;
		public IndexeurIntelligent ind;
	    public String requete;
        public String           nomMatriceCreuse ;//= "./src/test/ressource/SAUVER/matriceCreuse.txt";
        public String nomDictionnaireIntelligent ;//= "./src/test/ressource/SAUVER/DictionnaireIntelligent.txt";
        public String               nomTableHash ;//= "./src/test/ressource/SAUVER/table_hash.txt";
        public String                    nomDate ;//= "./src/test/ressource/SAUVER/DATE.txt" ;
	    public String Rep = /*"./src/test/ressource/*/"corpus";
	    public int aff_date = 1;
        public   File[] Dossier_Rec;  
	    
	    
	    
	    
		public   RechercheIntelligente( int nd, int nt , String rep){
		    try{

		        long Debtemp = System.currentTimeMillis();
                
                File file = new File(Rep);
			    ind = new IndexeurIntelligent(nd,nt,rep,file.lastModified()+"");
                 
			    //ind.Indexation(INDnomFich);
			    ind.Indexation();
			    //rempl_dico_requete(LectnomFich,nd,d_req);
			    
			    
			    
		    }catch (IOException io) {
                throw new RuntimeException("Erreur: Constructeur  RechercheIntelligente");
            }
		}
		
		
		
		public boolean MemeMot(String m1,String m2){
            return m1.toLowerCase().equals(m2.toLowerCase());
         }
		
		
		public int facFreq(String term,int doc){
			File f;
			f = ind.Doc[doc];
			System.out.println(f.toString());
	                int i=0;
					// on donne à i l'indice du terme dans la matrice du corpus 
					i = ind.d.indiceMot(term);
					System.out.println("le mot = "+term);
			if(i!=-1){
						int j= ind.mat.val(doc,i);
					System.out.println("faqFreq = "+j);
					return j;
		}
		else{
		return 0;
		}
		
		}
	     
	     
		/*public int MaxOcc(){
                        int l=0;
			int k=0;
			int m=0;
			int j=0;
		   //System.out.println(" ");
			while ( j!=d.nbMots()){
			int i=0;
			k=facFreq(d.motIndice(j),i,"corpus");
			i++;
			    while( i != ind.Nb_doc_rep()){
				//System.out.println( "j : "  +j);
						l=facFreq(d.motIndice(j),i,"corpus");
				        if(l<=k){
								if(m<=k) m=k;
								else  m=m;
						}else{
							if(m<=l) m=l;
							else m=m;
						}
						i++;
						//System.out.println( "k est"  +k);
				}
				System.out.println("max occ est "+m);
				//System.out.println(lector.elementCourant()+"est apparait "+k+"fois en d"+i);
				j++;
		}
		return m;
		}*/
	        
	     public void rempl_de_q(int[] tabq){
          	int lgReq = 0,i =0,j = 0,k = 0,cpt = 0;
          	while ( i != d_req.nbMots()){
          			String mot = d_req.motIndice(i);
          			j = i; cpt = 0;
          			if ( !Verif_avant(mot,i)){
  					    while( j != d_req.nbMots() ){
  				 			 if( !MemeMot(mot,d_req.motIndice(j) ) )j++;
							 else{ cpt++; j++; }
						}
						tabq[k] = cpt;
						lgReq ++;
						k++;
          			}
          			i++;
          	}
	     
	     }   
	     
	     
	     public int rempl_de_d(int[] tabd, int nd){
	            int lgDoc = 0,i = 0,j = 0;
              	String mt = d_req.motIndice(i);
              	while( i != d_req.nbMots()){
              			mt = d_req.motIndice(i);
              			if( !Verif_avant(mt,i) ){
              					tabd[j] = facFreq(mt,nd);
              					lgDoc++;
              					j++;
              			}
						    i++;
              	}
              	return lgDoc;
	     }
	        
	       public double NormalisationCosinus(int numDoc){
	        		// remplissade de q
	              	int [] tableauReq = new int[1000];
	              	rempl_de_q(tableauReq);
	              
	               // remplissage de d
	              	int [] tableauDoc = new int[100];
	              	int l = rempl_de_d(tableauDoc,numDoc);
	              	
	              	double a = 0, b = 0, c = 0;
	              	for( int i = 0 ; i < l; i++){
	              			a = a + tableauDoc[i] * tableauReq[i];
	              			b = b + tableauDoc[i] * tableauDoc[i];
	              			c = c + tableauReq[i] * tableauReq[i];
	              	}		
	              	double s = a/(Math.sqrt(b*c));
	              	System.out.println(s +"");
	              	return s;	
	        }
	        
	        
	        public boolean Verif_avant(String m, int i){
	               int j=0;
	               while(j!=i){
	                      String mot1=d_req.motIndice(j);
                          if( MemeMot(m,d_req.motIndice(j))  ) return true;
                          j++;
	               }
	               return false;
	        
	        }
	           
	   public double facLog(String term, int doc){
        double s=0, tF =0.;
   
     
				
			        
						s=facFreq(term,doc);
						  if(s!=0){
						    tF=1.+(Math.log10(s));
						     System.out.println("le log est :"+tF);
						     return tF;
						   
				           }
				           else{  
				           return 0;
			  	   }
	
		}
		
		
		    
		
		public void tab_somme(int l ,double[] t,String m){
		      int i = 0;
		      System.out.println(m);
		      while ( i != l){
		      		System.out.print(t[i]+"|");
		      		i++;
		      }
		      System.out.println("");
		}
		
		public double idf(String term){ 
            int j=0, df=0,k=0,N=0, i=ind.d.indiceMot(term);
            double idf=0;
            while(j!= ind.Nb_doc_rep()){
                k=ind.mat.val(j,i);   
                if(k!=0){
                    df++;
                    j++;
                }else  j++;
            }
            N= ind.Doc.length;
            idf=1+Math.log(N/df);
            System.out.println("idf est :"+idf);	  
            return idf;
	  
	  
	  }
	
	  public void recup_req(String s, String LectF){
	        try{
	            //Recupere la requete
			    Scanner sc = new Scanner(System.in);
			    System.out.println(s);
			    String req = sc.nextLine();
            	PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(LectF))));
            	pw.print(req);
            	pw.close();
		    }catch (IOException io) {
            throw new RuntimeException("Erreur: Constructeur  RechercheIntelligente");
            
            }
	  }
	  
	  public void ARG_recup_req(String LectF, String[] Arg, int l){
	        try{
	            //Recupere la requete sous forme d'argument
	            String mot="";
	            int i=0;
	            while(i!=l){
	                mot = mot + Arg[i] + " ";
	                i++;
	            }
            	PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(LectF))));
            	pw.print(mot);
            	pw.close();
		    }catch (IOException io) {
                throw new RuntimeException("Erreur: Constructeur  RechercheIntelligente");
            }
	  }
	
	   public void rempl_dico_requete(String LectFich, int numd){
		        l = new LecteurDocumentNaif(LectFich);
			    d_req = new DictionnaireIntelligent(numd);
			    l.demarrer();
			    while(!l.finDeSequence()){
				    d_req.ajouterMot(l.elementCourant());
				    l.avancer();
			    }
			    d_req.ajouterMot(l.elementCourant());
			    //d.afficher();
		}
		
		public void evaluation(String [] tabSt, double [] tabDo ){
		        int lgSt = 0,lgDo = 0,j = 0,k = lgDo -1,p = lgSt -1;
		        double s = 0;
		        while ( j != ind.Doc.length ){		             
		                s = 0;
		                String nomDeDoc = ind.Doc[j].toString();
		                        s = s + combinaison(j);
		                       
		                
		                k = lgDo -1;
		                p = lgSt -1;
		                //System.out.println ( "s = " + s);
		                while ( k != -1 && tabDo[k] <= s ){
		                       tabDo[k+1] = tabDo[k];
		                       tabSt[p+1] = tabSt[p];
		                       k--;
		                       p--;
		                }
		                tabDo[k+1] = s;
		                tabSt[p+1] = nomDeDoc;
		                lgSt++;
		                lgDo++;
		                j++;
		        }
		        int o = 0;
		        while( o != lgSt){
		                if(tabDo[o]!=0)
		                System.out.println("Le document : " + tabSt[o] + " a un poids de : " + tabDo[o]);
		                o++;
		        }
		
		}
		public double combinaison(int doc){
		
		 int i=0;
		 double a=0 , s=0;
		  for(i=0;i<d_req.nbMots();i++){
		
		a=a+(facLog(d_req.motIndice(i),doc)*facLog(d_req.motIndice(i),doc)*idf(d_req.motIndice(i))*idf(d_req.motIndice(i)));
		
		}
		double k=(NormalisationCosinus(doc))*(NormalisationCosinus(doc));
		s=(a/k);
		System.out.println("la combinaison est :"+s);
		
		return s;
		}
		
		//src/test/ressource/inf234-tests/
		/*public void recuperer_req(int i, File[] Dos)    throws FileNotFoundException    {
      AccesSequentielModele1<String> l;
            try{ 
                    int nbM=0,num_doc=0,i_mot=-1;
                    //File repertoire = new File(Dossier_requete);
                    //Dossier_Rec=repertoire.listFiles();
                    String doc= Dos[i].toString();
                    if(0<=i  && i<nbD){
                            String mot="";
                            i_mot=-1;
                            l = new LecteurDocumentNaif(doc);
                       		l.demarrer();
                       		while(!l.finDeSequence())
                       		{
                       		      mot=l.elementCourant();   
	                              i_mot=DD.indiceMot(mot);
	                              if(i_mot==-1) {
	                                    DD.ajouterMot(mot);
	                                    i_mot=DD.indiceMot(mot);
	                              }
		                          l.avancer();
                       		}
                    }
         }catch (IOException io){
            throw new RuntimeException("Erreur: Indexation()  IndexeurIntelligent");
        }
    
    }*/
		/*public void fichier_colonnesTests(String Dossier_requete){
		    File repertoire = new File(Dossier_requete);
		    
		}*/
		
		
		
		public static void main (String []args) {
		        
		                
		        int nbd=  100;
		        int nbt=100000;
		        String rep   = "/ext/INF/354_projet/echantillon_100";
		        //String rep   = "/home/orandr/Bureau/corpus";
		        String InomF = "matriceCreuse.txt";
		        String req   = "./requete";
		        
				RechercheIntelligente r2 = new  RechercheIntelligente(nbd, nbt, rep);
				int lg_arg = args.length;
		        if(args.length == 0 ) r2.recup_req("Veuillez saisir une requete :",req);
		        else r2.ARG_recup_req(req,args,lg_arg);
		        r2.rempl_dico_requete(req,nbd);
               
                double[] Tab = new double[1000];
                String[] Stab = new String[1000];
                 
                r2.evaluation(Stab,Tab);
               
               
                //src/test/ressource/inf234-tests/
                //recuperer_req(String requete);
                
                
                
                /*
                
                String t = "Les";
                int ndoc = 0;
		        //int j = MaxOcc();  System.out.println("MaxOcc  "+j);
                System.out.println("facFreq  "+r2.facFreq(t,ndoc));
	            System.out.println("NormalisationCosinus  "+r2.NormalisationCosinus(ndoc));  
	            System.out.println("facLog  ");  r2.facLog();
		        System.out.println("idf  "+r2.idf(t));
		        
		        */
		        
		        
		        
		        
		        
		        
		        
		        
		        
		        
		        
		        
		        
		        
		}
}		
