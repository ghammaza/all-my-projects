package inf353;
import java.io.File;
import java.io.*;

public class Indexeur{

    //attributs
    private int D_MAX;
    private int T_MAX;
    private MatriceIndex mat;
    private Dictionnaire d;
    private String nomDoss;
    
    //Constructeurs
    public Indexeur(int n1, int n2,String nomD){
        this.nomDoss = nomD;
        this.T_MAX=n2;  
        this.D_MAX=n1;  
        this.mat = new MatriceIndexNaive(this.D_MAX,this.T_MAX);
        this.d= new DictionnaireNaif(T_MAX); 
        //System.out.println(d.lg);
    }

   
    //constructeurs
    public Indexeur( int MOT_MAX, String nomD){
        this.nomDoss = nomD;
        this.T_MAX=Nbmot_fichier(MOT_MAX);
        System.out.println("Indexeur Nbmot_fichier() term = "+T_MAX); 
        this.D_MAX=Nb_doc_rep();
        System.out.println("Indexeur Nb_doc_rep() doc = "+D_MAX);;  
        this.mat = new MatriceIndexNaive(this.D_MAX,this.T_MAX);
        this.d= new DictionnaireNaif(T_MAX); 
    }
            
    public MatriceIndex Remplie_matrice() {
      AccesSequentielModele1<String> l;
            try{ 
                    int num_doc=0;
                    int i_mot=-2;
                    File repertoire = new File(this.nomDoss);
                    File[] files=repertoire.listFiles();
                    int nbD=files.length;
                    int i=0;
                    for(i=0;i<nbD;i++){System.out.println(files[i]);}
                    System.out.println("files.length   "+files.length);
                    String doc="";
                    File f;
                    while(num_doc<nbD)
                    {
                        f= files[num_doc];
                        doc = f.toString();
                        String mot="";
                        i_mot=-1;
                        l = new LecteurDocumentNaif(doc);
                		l.demarrer();
                		while(!l.finDeSequence())
                		{
                		   mot=l.elementCourant();   
		                   i_mot=d.indiceMot(mot);
		                   if(i_mot==-1) 
		                   {
		                         d.ajouterMot(mot);
		                         i_mot=d.indiceMot(mot);
		                         /*if(i_mot==-1){
		                            i_mot=(d.lg)-1;
		                         }
		                         */
		                   }
		                   mat.incremente(num_doc,i_mot);
			               l.avancer();
                		}
                		num_doc ++;
                	
                	}
                	//d.afficher();
                	//d.afficher2();
                	mat.sauver("matrice.txt");
		        System.out.println("d.nbMots()  "+d.nbMots());
                	return mat;
         }catch (IOException io){
          throw new RuntimeException("Erreur: Remplir() Indexeur");
          }
    }
    
    public int Nb_doc_rep(){
        File repertoire = new File(this.nomDoss);
        File[] files=repertoire.listFiles();
        System.out.println("files.length   "+files.length);
    	  return files.length;
    }
    
    
    public int Nbmot_fichier(int LG_MAX){
        AccesSequentielModele1<String> l;
        int num_doc=0;
        Dictionnaire D1= new DictionnaireNaif(LG_MAX); 
        File repertoire = new File(this.nomDoss);
        File[] files=repertoire.listFiles();
        int nbD=files.length;
        String doc="";
        File f;
        while(num_doc<nbD)
        {
            f= files[num_doc];
            doc = f.toString();
            String mot="";
            int i_mot=-1;
            l = new LecteurDocumentNaif(doc);
    		l.demarrer();
    		while(!l.finDeSequence())
    		{
    		   mot=l.elementCourant();   
               i_mot=D1.indiceMot(mot);
               if(i_mot==-1) 
               {
                     D1.ajouterMot(mot);
                     i_mot=D1.indiceMot(mot);
                     /*if(i_mot==-1){
                        i_mot=d.lg-1;
                     }*/
               }
               l.avancer();
    		}
    		num_doc ++;
    	    System.out.println("num_doc  "+num_doc +"   D1.nbMots()  "+D1.nbMots() );
    	}
    	return D1.nbMots();
    }
     
     public static void main (String []args) {
       //this.nomDoss = "/ext/INF/354_projet/french_flat/";
      String Rep = "./src/test/ressource/corpus";
      //String Rep = "/ext/INF/354_projet/corpus"; //ecole
    
       //Indexeur ind_malin = new Indexeur_malin(1100,Rep);
       //ind_malin.Remplie_matrice();
       
       Indexeur ind = new Indexeur(10,900,Rep);
       MatriceIndex mat2 = new MatriceIndexNaive(10,900);
       mat2 = ind.Remplie_matrice();
       int cpt=0;
      
       //L'element les est a l'indice 2.
       //Et manuellement nous les avons cherche dans chacun des documents
       // val n*doc et n*term  
       int id_mot=ind.d.indiceMot("Les");
       /*
       if(mat2.val(0,id_mot)==2 )  cpt++;
       if(mat2.val(1,id_mot)==1 )  cpt++;
       if(mat2.val(2,id_mot)==14)  cpt++;
       if(mat2.val(3,id_mot)==1 )  cpt++;
       if(mat2.val(4,id_mot)==1 )  cpt++;
       if(mat2.val(5,id_mot)==6 )  cpt++;
       if(mat2.val(6,id_mot)==1 )  cpt++;
       if(mat2.val(7,id_mot)==1 )  cpt++;
       if(mat2.val(8,id_mot)==1 )  cpt++;
       if(mat2.val(9,id_mot)==5 )  cpt++;
       */
       if(mat2.val(0,id_mot)==1 ) { cpt++;}
       if(mat2.val(1,id_mot)==1 ) { cpt++;}
       if(mat2.val(2,id_mot)==1 ) { cpt++;}
       if(mat2.val(3,id_mot)==5 ) { cpt++;}
       if(mat2.val(4,id_mot)==6 ) { cpt++;}
       if(mat2.val(5,id_mot)==2 ) { cpt++;}
       if(mat2.val(6,id_mot)==1 ) { cpt++;}
       if(mat2.val(7,id_mot)==14 ){ cpt++;}
       if(mat2.val(8,id_mot)==1 ) { cpt++;}
       if(mat2.val(9,id_mot)==1 ) { cpt++;}
       
       
       System.out.println("Il y a combien de les sur 10 ?  "+cpt);
       System.out.println("Indexeur Fin");
       System.out.println("");

    }
    
   
}

//doc 87 191

//Term 254 717


// ls | wc -l  //Pour savoir le nombre de ficheirs
//87191
// cat * | wc -w //Pour savoir le nombre de mots
//32 millions de mot

