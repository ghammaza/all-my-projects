package inf353;
import java.lang.Character;
import java.lang.String;
import java.io.*;

public class DictionnaireNaif implements Dictionnaire {
//public class DictionnaireNaif{
    private final char mf=(char)0;//marque de fin NON IMPRIMABLE
    private char[] tab;//tableau de char
    private int Nb_MAX;//nbmax du tableau
    private int lg;//nb mot dans le tabelau
    
    public  DictionnaireNaif(int nb_mot){
    this.Nb_MAX=nb_mot;
    this.tab= new char[40*nb_mot];
    this.lg =0;
    }


    //mot par ligne
    public void afficherfichier(String nomDeFichier) throws FileNotFoundException {
        int i = 0;
        PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(nomDeFichier))));
        while(i < this.lg ){
             pw.println(motIndice(i));
             i++;
        }
        pw.close() ;
   }



    public void afficher2()
    {
        int i = 0;
        System.out.println("Dictionnaire : ");
        while(i < this.lg*40)
        {
             if( (i+1)%40==0 && this.tab[i] != this.mf)
             {
                System.out.println(this.tab[i]+" ");
             }
             else if( (i+1)%40==0 && this.tab[i] == this.mf)
             {
                System.out.println("");
             }
             else if( this.tab[i] != this.mf)
             {
                System.out.print(this.tab[i]+"");
             }
             else
             {
                System.out.print(""); 
             }
             i++;
        }
        System.out.println("");  
        System.out.print(" ");
     }



      public void afficher()
      {
        int i = 0;
        System.out.println("Dictionnaire : ");
        while(i < this.lg*40)
        {
             if( (i+1)%40==0 && this.tab[i] != this.mf)
             {
                System.out.print(this.tab[i]);
             }
             else if( (i+1)%40==0 && this.tab[i] == this.mf)
             {
                System.out.print(" ");
             }
             else if( this.tab[i] != this.mf)
             {
                System.out.print(this.tab[i]);
             }
             else
             {
                System.out.print(""); 
             }
             i++;
        }
        System.out.println("");  
     }
    
    
    /**
     * é.i: qcq
     * é.f.: le dictionnaire est vide.
     */
    public void vider(){this.lg=0;}//nb mot du dictionnaire est vide
    
    
    /**
     * renvoie vrai ssi m est dans le dictionnaire.
     * @param m
     * @return
     */
    public boolean contient(String m)
    {
        int i=0;
        int j=0;
            while(i<this.lg*40 && j!=m.length() && j<40  && m.length()<40   && !(m.isEmpty()) )
            {
               if(this.tab[i]== java.lang.Character.toLowerCase(m.charAt(j))  || this.tab[i]== java.lang.Character.toUpperCase(m.charAt(j) ) )
               {
                  j++;
                  if(j==m.length() && i+1<this.lg*40  && this.tab[i+1]!=this.mf){
                          //System.out.println("j==m.length()  "+(j==m.length()) +" mot  : "+m+"  i  "+i+"     Condition  ");
                          i=i-j;
                          j=0;
                          i=i+40;  
                  }
                  i++;
               }
               else
               {
                  
                  i=i-j;
                  j=0;
                  i=i+40;
               }
           }
       return (j==m.length() &&  i+1<this.lg*40  && this.tab[i+1]==this.mf && ((i-j)%40==0) );
    }
    
     /**
     * é.i.: le dictionnaire contient D0 (un ensemble de this.Nb_MAX mots).
     * é.f.: si m appartient à D0; le dictionnaire est inchangé
     *       sinon, le dictionnaire contient D0 U {m}
     * 
     * @param m
     */
    public void ajouterMot(String m)
    {
           int j = 0;
           
           if(m.length()<=40  &&   (this.lg<this.Nb_MAX) && !(m.isEmpty()))
           {
                   while(j!=40 && j!=m.length() )//tant que je ne suis pas a la fin j'avance dans m
                   {
                      this.tab[this.lg*40+j]= m.charAt(j);
                      j++;
                   } 
                   if(j<40)
                   {
                      this.tab[this.lg*40+j] = this.mf;
                   }
                   this.lg++;
           }
     }
    
 
  
    
        /**
     * é.i: Dictionnaire contient D0
     * é.f.: si m est dans D0, le Dictionnaire contient D0 | {m}, sinon le dictionnaire est inchangé.
     * @param m
     */
    public void supprimer(String m)
    {
       int i=0;//j'initialise i a 0
       int j=(this.lg-1)*40;//je me place a l'indice du dernier mot du dictionnaire
       i=indiceMot(m);
       //si le mot (String m) y  est alors je le supprime
       //Pour cela, je copie le dernier mot a la place de celui a supprimer
       if(i!=-1)
       {
          i=(indiceMot(m)*40);//je me place a l'indice du mot dans le dictionnaire
          //je copie exactement les 40 charactere qui compose le dernier mot
          while(j<this.lg*40-1  && m.length()<40)
          {
              //je supprime le mot avec celui du dernier
              this.tab[i]=this.tab[j];
              i++;
              j++;
          }
          
          this.tab[i]=this.tab[j];//Pour copier aussi la marque de fin
          this.lg--;//je decrement le nombre de mot du dictionnaire
       }
    }

     /**
     * renvoie le nombre de mots de m.
     * @return
     */
    public int nbMots(){return (this.lg);}//this.lg etant le nb mot du dictionnaire alors je le retourne directement)
    
    
     /**
     * vrai ssi il existe m dans D0 tel que il exist u tq m = p.u
     * 
     * (vrai si un mot de D0 commence par p)
     * @param p le préfixe recherché
     * @return
     */
    public boolean contientPrefixe(String p)
    {
      
        int i=0; //je me positionne a l'indice 0 du dictionnaire
        int j=0; //je me positionne a l'indice 0 de String m
        
        //tant que je ne suis pas a la fin du dictionnaire et a la fin du mot (String m)
        //et que le mot m est inferieur a 40
        while(i<this.lg*40 && j!=p.length() && j<40  && p.length()<40 && !(p.isEmpty()) )
        {
           //Si ils ont le meme indice je verifie pour l'indice d'apres
           if(this.tab[i]==p.charAt(j))
           {
              i++;
              j++;
           }
           //si ils n'ont pas le meme caractere je continue de chercher dans le dictionnaire
           else
           {
              
              i=(i-j)+40;
              j=0;
           }
       }
       if(p.isEmpty())
       {
           return false;
       } 
       return (j==p.length() );
        
    }   
    
     /**
     * renvoie la chaîne de caractères s telle que 
     *  s est dans D0 
     *  et m commence par s
     *  et il n'existe pas de chaîne s' ds D plus longue que s tq m commence par s.
     *  
     * @param mot
     * @return
     */
    public String plusLongPrefixeDe(String mot)
    {
      int i=0,j=0,k=0,k_new=0;
      
      char[] s;//je declare un tableau s  de char
      s= new char[40];//je le creer
      
      //je verifie que le mot est inferieur a 40-1 pour rajouter ma marque de fin 
      while(i!=mot.length() && i<40  && mot.length()<=40)
      {
          //je copie le String mot dans ma chaine de caractere
          s[i]=mot.charAt(i);
          i++;
      }
      if(i!=40){
      //je place ma marque de fin 
      s[i]=this.mf;
      }
       i=0;
        //tant que je ne suis pas a la fin du dictionnaire et a la fin du mot (String m)
        //et que le mot m est inferieur a 40
        while(i<this.lg*40 && j!=mot.length() && j<40  && mot.length()<=40)
        {
           //Si ils ont le meme indice je verifie pour l'indice d'apres
           if(this.tab[i]== mot.charAt(j))
           {
              i++;
              j++;
              k_new++;
              if(k<k_new){k=k_new;}
           }
           //si ils n'ont pas le meme caractere je continue de chercher dans le dictionnaire
           else
           {
              if(k<k_new){k=k_new;}
              
              i=(i-j)+40;
              j=0;
              k_new=0;
           }
        }
         //static String copyValueOf(char[] data, int offset  , int count   )
         //-------------------------(tableau  s , int decalage, int compteur)
         //Construit une chaîne de caractères à partir d’une partie de tableau de caractères 
        if(j==mot.length() | k>0)
        {   
             return String.copyValueOf(s,0,k);
        }
        return "";
    }
   
     
     
        /**
     * renvoie le mot associé à l'entier i;
     * @param i l'indice du mot à renvoyer
     * @return
     */
    public String motIndice(int i)
    {
      int j=i*40;
      int k=0;
      char[] s;//je declare un tableau s  de char
      s= new char[40];//je le creer
      String mot="";
     
        while(i<this.lg && j<(i+1)*40 && k!=40 && this.tab[j]!=this.mf )
       {
          //je copie le String mot dans ma chaine de caractere
          s[k]=this.tab[j];
          mot = mot + this.tab[j];
          j++;
          k++;
       }
        /* if(k>0)
        {   
             String s1= String.copyValueOf(s,0,k);
             System.out.println(s1);
             return s1;
        }
        */
        return mot;
    
    }
    
    
       /*
    
     /**
     * renvoie l'entier associé à m;
     * @param m
     * @return
     */
     //renvoie -1 si incorrecte
    public int indiceMot(String m){
        int i=0;
        int j=0;
        
            while(i<this.lg*40  && j<40 && j!=m.length()  && !(m.isEmpty()) ) {
               if(java.lang.Character.toLowerCase(this.tab[i])== java.lang.Character.toLowerCase(m.charAt(j)) )
               {
                  j++;
                  if(j==m.length() && i+1<this.lg*40  && this.tab[i+1]!=this.mf){
                          //System.out.println("j==m.length()  "+(j==m.length()) +" mot  : "+m+"  i  "+i+"     Condition  ");
                          i=i-j+40;
                          j=0; 
                  }
                  i++;
               }
               else
               {
                  
                  i=i-j+40;
                  j=0;
               }
           } 
        if (j==m.length() && !(m.isEmpty())) { 
           return (i-j)/40;
       }
       return -1 ;
    }
    
    
       
            
    
    
    public static void main(String[] arg){
            DictionnaireNaif d= new DictionnaireNaif(10); 
                System.out.println("");
                System.out.println("");
                d.ajouterMot("chouaib");
                d.ajouterMot("regis");
                d.ajouterMot("thang");
                d.ajouterMot("deLES");
                d.ajouterMot("arthur");
                d.ajouterMot("ayoub");
                d.ajouterMot("dele");
                d.ajouterMot("de");
                d.ajouterMot("de ");
                System.out.println("indice du mot deLE est a la position "+(d.indiceMot("deLE")));
                System.out.println("indice du mot deLES est a la position "+(d.indiceMot("deLES")));
                System.out.println("");
                d.afficher();
                System.out.println("DictionnaireNaif Fin");
                System.out.println("");
            }
    
         
}

