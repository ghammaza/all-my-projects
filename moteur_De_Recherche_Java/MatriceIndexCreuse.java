package inf353;
import java.io.*;

public class MatriceIndexCreuse implements MatriceIndex{
//public class MatriceIndexCreuse{
	// attributs :
	private CelluleM[] tab;
	private int nbT;
		
    public MatriceIndexCreuse(int T){
	        if( 0<T ){	  
	           this.nbT = T;
	           this.tab = new CelluleM[this.nbT];		               
       	    }
            else{
		      System.out.println("Erreur MatriceIndexCreuse constructeur, Rien fait ");
            }
    }


   /*public void sauver(String nomDeFichier) throws FileNotFoundException{
	    int i =0;
    	
    	OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(nomDeFichier));
        BufferedWriter bw = new BufferedWriter(writer);
        PrintWriter pw = new PrintWriter(bw);
			
            while (i != this.nbT) {
              CelluleM cc =  tab[i];
              pw.print(" ["+i+"] ");
              while(cc != null){
                             pw.print(" --> (");
                                  if(cc.nbocc<=9)      pw.print(cc.nbocc+"     | ");
                             else if(cc.nbocc<=99)     pw.print(cc.nbocc+"    | ");
                             else if(cc.nbocc<=999)    pw.print(cc.nbocc+"   | ");
                             else if(cc.nbocc<=9999)   pw.print(cc.nbocc+"  | ");
                             else if(cc.nbocc<=99999)  pw.print(cc.nbocc+" | ");
                             
                                  if(cc.nbdoc<=9)      pw.print(cc.nbdoc+"     ) ");
                             else if(cc.nbdoc<=99)     pw.print(cc.nbdoc+"    ) ");
                             else if(cc.nbdoc<=999)    pw.print(cc.nbdoc+"   ) ");
                             else if(cc.nbdoc<=9999)   pw.print(cc.nbdoc+"  ) ");
                             else if(cc.nbdoc<=99999)  pw.print(cc.nbdoc+" ) ");
                             
                             cc = cc.succ;
             }     
   		pw.println("");
   		i++;
        }
        pw.close() ;
   }*/
   
   public void sauver(String nomDeFichier) throws FileNotFoundException{
	    int i =0;
    	
    	OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(nomDeFichier));
        BufferedWriter bw = new BufferedWriter(writer);
        PrintWriter pw = new PrintWriter(bw);
			
            while (i != this.nbT) {
              CelluleM cc =  tab[i];
              pw.print(" ["+i+"] ");
              while(cc != null){
                             pw.print(" -->("+cc.nbocc+" | "+cc.nbdoc+")");
                             
                             cc = cc.succ;
             }     
       		pw.println("");
       		i++;
        }
        pw.close() ;
   }

   public void sauver2(PrintWriter pw, int i){
    	while (i != this.nbT) {
              CelluleM cc =  tab[i];
              //pw.print(" ["+i+"] ");
              while(cc != null){
                             pw.print(" --> (");
                                  if(cc.nbocc<=9)      pw.print(cc.nbocc+"     | ");
                             else if(cc.nbocc<=99)     pw.print(cc.nbocc+"    | ");
                             else if(cc.nbocc<=999)    pw.print(cc.nbocc+"   | ");
                             else if(cc.nbocc<=9999)   pw.print(cc.nbocc+"  | ");
                             else if(cc.nbocc<=99999)  pw.print(cc.nbocc+" | ");
                             
                                  if(cc.nbdoc<=9)      pw.print(cc.nbdoc+"     ) ");
                             else if(cc.nbdoc<=99)     pw.print(cc.nbdoc+"    ) ");
                             else if(cc.nbdoc<=999)    pw.print(cc.nbdoc+"   ) ");
                             else if(cc.nbdoc<=9999)   pw.print(cc.nbdoc+"  ) ");
                             else if(cc.nbdoc<=99999)  pw.print(cc.nbdoc+" ) ");
                             
                             cc = cc.succ;
             }     
       		pw.println("");
       		i++;
        }
   }

   public int val(int ndoc, int nterm){
        if(0<=nterm &&  nterm<this.nbT) {
              CelluleM cc =  tab[nterm];
              while(cc != null && cc.nbdoc<ndoc)   
                    cc = cc.succ;     
       		  if (cc==null)  return 0;//Car il n'existe pas
       		  else if(cc.nbdoc==ndoc)  return cc.nbocc;
       		  else return 0;
        }
       else if(!( 0<=nterm  && nterm < this.nbT)) {
       		 System.out.println("Erreur MatriceIndexCreuse  val, vous avez entré un numéro de terme qui n'existe pas. ");
       		 return 0;
       }
       else return 0;
   }

    
    
   public  void incremente(int ndoc, int nterm){
       if(0<=nterm &&  nterm<this.nbT) {
              CelluleM cc =  tab[nterm];
              CelluleM cp = null;
              while(cc != null && cc.nbdoc<ndoc){
                             cp = cc;
                             cc = cc.succ;
             }     
             CelluleM newc = new CelluleM(ndoc,cc);
       		 if(cc!=null){ 
       		    cc.plusnbocc(cc);
             }else{
       		    if (cp != null)
                     cp.succ = newc;
                else
                     tab[nterm] = newc;
             }
                   
       	}else{
       		 System.out.println("Erreur MatriceIndexCreuse  incremente, vous avez entré un numéro de terme qui n'existe pas. " +  nterm);
        }
   }
    
    
    
   public  void affecte(int ndoc, int nterm, int val){
         if( 0<=nterm &&  nterm<this.nbT){
              CelluleM cc =  tab[nterm];
              CelluleM cp = null;
              while(cc != null && cc.nbdoc<ndoc){
                             cp = cc;
                             cc = cc.succ;
             }    
             CelluleM newc = new CelluleM(val,ndoc,cc);
       		 if( cc==null ) { //nbdoc trouve
       		    if (cp != null){
                    cp.succ = newc;
                }
                else{  //ajout en tete  donc cc==null
                     tab[nterm] = newc;// on remet a jour la tete de la sequence 
                }
             }else{
                tab[nterm] = new CelluleM(val,ndoc,cc);
                
             }
        }
        else
            System.out.println("Erreur MatriceIndexCreuse  affecte, vous avez entré un numéro de terme qui n'existe pas. ");
   }
   
   
   
   
  public static void main (String [] args)  throws FileNotFoundException {
	            MatriceIndex mat = new MatriceIndexCreuse(2);
	            
                mat.affecte(0,0,1);
                mat.affecte(1,0,2);
                
                mat.affecte(0,1,3);
                mat.affecte(1,1,4);
                
                
//affecte(int ndoc, int nterm, int val)
/*[0]  -->(1 | 0) -->(2 | 1)
  [1]  -->(3 | 0) -->(4 | 1)
*/                
                String doc = "matriceCreuse.txt";
                mat.sauver(doc);
                System.out.println("MatriceIndexCreuse, Notre MatriceIndexCreuse est dans "+ doc);
                System.out.println("MatriceIndexCreuse Fin");
                System.out.println("");
    }
    
}
