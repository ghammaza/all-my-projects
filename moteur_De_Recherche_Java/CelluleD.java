package inf353;
public class CelluleD{
		public String mot;
		public CelluleD succ;
		
		public  CelluleD(String m,  CelluleD suc) {
			this.mot   = m;
			this.succ  = suc;
		}
		
		public void CelluleD() {
		   this.mot   = "";
		   this.succ  = null;
		
		}
		
		public void setSucc(CelluleD c){
		    this.succ = c;
		}
		
		
		public void setMot(String m){
		    this.mot = m;
		}
		
		
		public CelluleD getSucc(){
		    return this.succ;
		}
		
		public String getMot(){
		    return this.mot;
		}
	}
