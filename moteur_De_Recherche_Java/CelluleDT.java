package inf353;
/**
 * Classe représentant une cellule dans une liste chaînée.
 * La cellule contient un élément entier et une cellule suivante.
 */
public class CelluleDT {
		public int ndoc;
		public int occ;
		public Cellule succ;
		
		/**
		 * Construit une cellule.
		 * @param e valeur de l'élément.
		 * @param suc Cellule suivante.
		 */
		public CelluleDT(int o,int d, Cellule suc) {
			this.occ = o; 
			this.ndoc = d;
			this.succ=suc;
		}
		
		/**
		 * Construit une Cellule avec une valeur d'élément égale à 0 et une cellule suivante.
		 * @param suc la cellule suivante.
		 */
		public CelluleDT(Cellule suc) {
			this.succ=suc;
		}
		
		/**
		 * Construit une Cellule avec une valeur d'élément égale à 0 et aucune cellule suivante.
		 */
		public void CelluleDT() {}
		
		/**
		 * Modifie la cellule suivante de la cellule courante.
		 * @param c Cellule suivante.
		 */
		public void setSucc(Cellule c){
		    this.succ = c;
		}
		
		/**
		 * Retourne la cellule suivante de la cellule courante.
		 * @return La cellule suivante.
		 */
		public Cellule getSucc(){
		    return this.succ;
		}
		
		/**
		 * Modifie la valeur de l'élément courant
		 * @param e la valeur de l'élément.
		 */
		public void setElement(int o, int d){
		    this.occ = o;
		    this.ndoc = d;
		}
		
		/**
		 * Renvoi la valeur de l'élément.
		 * @return la valeur de l'élément.
		 */
		public int getElement(){
		    return this.occ;
		}
	}
