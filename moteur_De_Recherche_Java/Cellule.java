package inf353;
/**
 * Classe représentant une cellule dans une liste chaînée.
 * La cellule contient un élément entier et une cellule suivante.
 */
public class Cellule {
		public String motH;
		public Cellule succ;
		
		/**
		 * Construit une cellule.
		 * @param e valeur de l'élément.
		 * @param suc Cellule suivante.
		 */
		public Cellule(String e, Cellule suc) {
			this.motH = e; 
			this.succ=suc;
		}
		
		/**
		 * Construit une Cellule avec une valeur d'élément égale à 0 et une cellule suivante.
		 * @param suc la cellule suivante.
		 */
		public Cellule(Cellule suc) {
			this.succ=suc;
		}
		
		/**
		 * Construit une cellule avec une valeur d'élément et aucune cellule suivante.
		 * @param e valeur entière de l'élément.
		 */
		public Cellule(String e) {
			this.motH = e;
		}
		
		/**
		 * Construit une Cellule avec une valeur d'élément égale à 0 et aucune cellule suivante.
		 */
		public Cellule() {}
		
		/**
		 * Modifie la cellule suivante de la cellule courante.
		 * @param c Cellule suivante.
		 */
		public void setSucc(Cellule c){
		    this.succ = c;
		}
		
		/**
		 * Retourne la cellule suivante de la cellule courante.
		 * @return La cellule suivante.
		 */
		public Cellule getSucc(){
		    return this.succ;
		}
		
		/**
		 * Modifie la valeur de l'élément courant
		 * @param e la valeur de l'élément.
		 */
		public void setElement(String e){
		    this.motH = e;
		}
		
		/**
		 * Renvoi la valeur de l'élément.
		 * @return la valeur de l'élément.
		 */
		public String getElement(){
		    return this.motH;
		}
	}
